// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import { LogoutRequest } from 'containers/Logout/actions';
const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/login',
      name: 'login',
      getComponent(nextState, cb) {
        import('containers/Login')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/logout',
      name: 'logout',
      getComponent() {
        LogoutRequest();
      },
    }, {
      path: '/reset-pwd',
      name: 'request reset password',
      getComponent(nextState, cb) {
        import('containers/ResetPwd')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/change-pwd',
      name: 'reset password',
      getComponent(nextState, cb) {
        import('containers/ChangePwd')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/companies',
      name: 'list companies',
      getComponent(nextState, cb) {
        import('containers/ListCompanies')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/add-company',
      name: 'add company',
      getComponent(nextState, cb) {
        import('containers/AddCompany')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/edit-company/:companyId',
      name: 'edit company',
      getComponent(nextState, cb) {
        import('containers/EditCompany')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/rooms',
      name: 'list rooms',
      getComponent(nextState, cb) {
        import('containers/ListRooms')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/add-room',
      name: 'add room',
      getComponent(nextState, cb) {
        import('containers/AddRoom')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/edit-room/:roomId',
      name: 'edit room',
      getComponent(nextState, cb) {
        import('containers/EditRoom')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/gateways',
      name: 'list gateways',
      getComponent(nextState, cb) {
        import('containers/ListGateways')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/add-gateway',
      name: 'add gateway',
      getComponent(nextState, cb) {
        import('containers/AddGateway')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/edit-gateway/:gatewayId',
      name: 'edit gateway',
      getComponent(nextState, cb) {
        import('containers/EditGateway')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/teams',
      name: 'list teams',
      getComponent(nextState, cb) {
        import('containers/ListTeams')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/add-team',
      name: 'add team',
      getComponent(nextState, cb) {
        import('containers/AddTeam')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/edit-team/:teamId',
      name: 'edit team',
      getComponent(nextState, cb) {
        import('containers/EditTeam')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/users',
      name: 'list users',
      getComponent(nextState, cb) {
        import('containers/ListUsers')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/add-user',
      name: 'add user',
      getComponent(nextState, cb) {
        import('containers/AddUser')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/edit-user/:userId',
      name: 'edit user',
      getComponent(nextState, cb) {
        import('containers/EditUser')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/users-batteries',
      name: 'users batteries',
      getComponent(nextState, cb) {
        import('containers/ListUsersSamples')
        .then(loadModule(cb))
        .catch(errorLoading);
      },
    }, {
      path: '/geofencing',
      name: 'users geolocalization',
      getComponent(nextState, cb) {
        import('containers/GeofencingBoard')
        .then(loadModule(cb))
        .catch(errorLoading);
      },
    }, {
      path: '/localization',
      name: 'user localization',
      getComponent(nextState, cb) {
        import('containers/LocalizationBoard')
        .then(loadModule(cb))
        .catch(errorLoading);
      },
    }, {
      path: '/hardness',
      name: 'team hardness',
      getComponent(nextState, cb) {
        import('containers/HardnessBoard')
        .then(loadModule(cb))
        .catch(errorLoading);
      },
    }, {
      path: '/denied',
      name: 'access-denied',
      getComponent(nextState, cb) {
        import('components/AccessDenied')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('components/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
