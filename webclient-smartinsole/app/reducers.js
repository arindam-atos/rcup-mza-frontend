/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';

import languageProviderReducer from 'containers/LanguageProvider/reducer';
import navBarReducer from 'containers/NavBar/reducer';
import teamsReducer from 'containers/ListTeams/reducer';
import teamZonesChartReducer from 'containers/TeamZonesChart/reducer';
import companiesReducer from 'containers/ListCompanies/reducer';
import roomsReducer from 'containers/ListRooms/reducer';
import gatewaysReducer from 'containers/ListGateways/reducer';
import usersReducer from 'containers/ListUsers/reducer';
import teamDashboardReducer from 'containers/TeamDashboard/reducer';
import editCompanyReducer from 'containers/EditCompany/reducer';
import editRoomReducer from 'containers/EditRoom/reducer';
import editGatewayReducer from 'containers/EditGateway/reducer';
import editUserReducer from 'containers/EditUser/reducer';
import editTeamReducer from 'containers/EditTeam/reducer';
import CardPanelReducer from 'containers/CardPanel/reducer';
import listUsersSamplesReducer from 'containers/ListUsersSamples/reducer';
import GeofencingBoardReducer from 'containers/GeofencingBoard/reducer';
import LocalizationBoardReducer from 'containers/LocalizationBoard/reducer';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers) {
  return combineReducers({
    route: routeReducer,
    language: languageProviderReducer,
    navBar: navBarReducer,
    card: CardPanelReducer,
    companies: companiesReducer,
    fetchedCompany: editCompanyReducer,
    rooms: roomsReducer,
    fetchedRoom: editRoomReducer,
    gateways: gatewaysReducer,
    fetchedGateway: editGatewayReducer,
    users: usersReducer,
    fetchedUser: editUserReducer,
    teams: teamsReducer,
    fetchedTeam: editTeamReducer,
    teamUsers: teamDashboardReducer,
    teamUsersSamples: teamZonesChartReducer,
    usersSamples: listUsersSamplesReducer,
    geofencing: GeofencingBoardReducer,
    localization: LocalizationBoardReducer,
    ...asyncReducers,
  });
}
