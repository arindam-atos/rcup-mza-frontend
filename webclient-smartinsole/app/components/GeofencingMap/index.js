import React from 'react';
import { Map, TileLayer, GeoJSON } from 'react-leaflet';
import WhiteBox from 'components/WhiteBox';
import MarkersList from 'components/MarkersList';
import Marker from 'components/Marker';
import GeofencingHeatmap from 'components/GeofencingHeatmap';
import { Input } from 'react-materialize';
import { Wrapper, MarkersDisplayButton } from './styles';

export default class GeofencingMap extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    heatmap: true,
  }
  handleChange = (key, value) => {
    let bool;
    if (value === 'true' || value === true) {
      bool = true;
    } else {
      bool = false;
    }
    this.setState({ heatmap: bool });
  }

  render() {
    return (
      <WhiteBox
        s={12}
        m={12}
        l={8}
        style={{
          position: 'relative',
        }}
        node={[
          <MarkersDisplayButton
            style={{
              textAlign: 'right',
              position: 'absolute',
              right: '12px',
              marginRight: '10px',
              padding: '10px',
              background: '#FFF',
              zIndex: '1000',
              border: '1px solid black',
            }}
            key={'layer'}
          >
            <Input
              name={'layer'}
              type={'switch'}
              onLabel={'On'}
              offLabel={'Off'}
              value={'false'}
              checked={this.state.heatmap}
              onChange={this.handleChange}
            />
          </MarkersDisplayButton>,
          <Wrapper
            key={'map'}
          >
            { this.props.company ?
              <Map
                id={'map'}
                center={[this.props.company.latitude, this.props.company.longitude]}
                zoom={19}
                maxZoom={21}
                minZoom={17}
              >

                <TileLayer
                  key={'tilelayer'}
                  maxZoom={19}
                  url={'https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png'}
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                />
                <GeoJSON
                  data={JSON.parse(this.props.company.map)}
                />

                <GeofencingHeatmap
                  gateways={this.props.gateways}
                  gtw={this.props.gtw}
                  secondInterval={this.props.secondInterval}
                  samples={this.props.samples}
                />

                { this.state.heatmap ?
                  <div>
                    <MarkersList
                      markers={this.props.gateways}
                    />
                    <Marker
                      marker={this.props.gtw}
                    />
                  </div> : ''
              }
              </Map> : <div />
            }
          </Wrapper>,
        ]}
      />
    );
  }
}

GeofencingMap.propTypes = {
  company: React.PropTypes.object,
  gateways: React.PropTypes.array.isRequired,
  gtw: React.PropTypes.object,
  secondInterval: React.PropTypes.number,
  samples: React.PropTypes.array,
};
