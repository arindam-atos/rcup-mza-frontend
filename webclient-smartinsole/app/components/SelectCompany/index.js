import React from 'react';
import { Collapsible, CollapsibleItem } from './styles';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));
const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

export default class SelectCompany extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    if (loggedUser.role === 'admin') {
      this.props.AllCompaniesRequest();
    }
  }

  setCompany = (val) => {
    let companyName = '';
    this.props.companies.forEach((company) => {
      if (company.id === Number(val)) {
        companyName = company.name;
      }
    });
    localStorage.setItem('currentCompany', JSON.stringify({
      companyId: val,
      companyName,
    }));
    window.location.reload();
  }

  render() {
    let content = <div />;

    if (this.props.companies.length > 0) {
      content = (
        <Collapsible>
          <CollapsibleItem header={currentCompany.companyName}>
            <ul>
              { this.props.companies.map((cmp) =>
                /* eslint-disable jsx-a11y/no-static-element-interactions */
                <li key={`li-${cmp.id}`} onClick={() => this.setCompany(cmp.id)}>
                  <span>{ cmp.name }</span>
                </li>
                /* eslint-enable jsx-a11y/no-static-element-interactions */
              )
            }

            </ul>
          </CollapsibleItem>
        </Collapsible>
      );
    }

    return (
      <div>
        { content }
      </div>
    );
  }
}

SelectCompany.propTypes = {
  companies: React.PropTypes.array,
  AllCompaniesRequest: React.PropTypes.func,
};
