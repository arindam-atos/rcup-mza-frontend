import styled from 'styled-components';
import { Collapsible as MCollapsible, CollapsibleItem as MCollapsibleItem } from 'react-materialize';

export const Collapsible = styled(MCollapsible)`
  border: 0;
  margin: 0;

  .active {
    background: transparent;
  }

`;

export const CollapsibleItem = styled(MCollapsibleItem)`
  .collapsible-body {
    padding: 0;
    z-index: 1000;
    position: relative;

    ul {
      position: absolute;
      right: -15px;
      border-radius: 2px;
      -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
      box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
      background-color: #fff;
    }

    li {
      float: none;
      width: 150px;

      span {
        margin-left: 20px;
      }
    }

    li:hover {
      background-color: rgba(0,0,0,0.1);
    }
  }

  .collapsible-header {
    padding: 0;
    border-bottom: none;
    background: transparent;
    line-height: 64px;

    .material-icons {
      margin-right: 0;
    }
  }
`;
