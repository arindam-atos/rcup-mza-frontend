import styled from 'styled-components';
import { Tabs as MTabs, Tab as MTab } from 'react-materialize';

export const Wrapper = styled.div`
  margin: 0 0.8rem !important;

  & > div > div {
    margin: 0 !important;
    padding: 0!important;
  }

`;

export const Tabs = styled(MTabs)`
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
  box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
  background-color: #fff;

  & a {
    color: #1e2736 !important;
  }

  & .indicator {
    background-color: #1e2736 !important;
  }
  width: 99%;

  & .tab {
    width: 49.9%;
  }
`;

export const Tab = styled(MTab)`
`;
