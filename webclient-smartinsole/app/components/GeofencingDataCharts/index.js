import React from "react";
import moment from "moment/moment";
import ChartTabs from "components/ChartTabs";
import { Wrapper, Tabs, Tab } from "./styles";

const COLORS = [
  "#0074D9",
  "#2ECC40",
  "#FFDC00",
  "#FF4136",
  "#B10DC9",
  "#DDDDDD",
  "#001f3f",
  "#F012BE",
  "#85144b"
];

const colorsByGtw = {};
const colorsByRoom = {};

const getColorsByGtw = gateways => {
  gateways.forEach((gtw, index) => {
    colorsByGtw.name = gtw.name;
    colorsByGtw[gtw.name] = COLORS[index];
  });
  colorsByGtw.MOBILITY = COLORS[COLORS.length - 1];
};

const getColorsByRoom = gateways => {
  let index = 0;

  gateways.forEach(gtw => {
    if (!Object.prototype.hasOwnProperty.call(colorsByRoom, gtw.room)) {
      colorsByRoom[gtw.room] = gtw.room;
      colorsByRoom[gtw.room] = COLORS[index];
      index += 1;
    }
  });
  colorsByRoom.MOBILITY = COLORS[COLORS.length - 1];
};

const getSamplesByGtw = samples => {
  const samplesByGtw = {};
  let firstSample = samples[0];
  let prevSample = samples[0];
  const mobility = {
    duration: 0,
    steps: 0
  };

  samples.forEach((sample, index) => {
    if (!Object.prototype.hasOwnProperty.call(samplesByGtw, sample.deviceID)) {
      samplesByGtw[sample.deviceID] = sample.deviceID;
      samplesByGtw[sample.deviceID] = {
        duration: 0,
        steps: 0
      };
    }

    if (prevSample.deviceID !== sample.deviceID) {
      const prevDate = moment(prevSample.timestamp).format("HH:mm:ss");
      const firstDate = moment(firstSample.timestamp).format("HH:mm:ss");
      const currDate = moment(sample.timestamp).format("HH:mm:ss");

      const prevDuration = moment.duration(prevDate, "seconds").asSeconds();
      const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
      const curDuration = moment.duration(currDate, "seconds").asSeconds();

      samplesByGtw[prevSample.deviceID].duration +=
        prevDuration - firstDuration;

      samplesByGtw[prevSample.deviceID].steps +=
        prevSample.Steps - firstSample.Steps;

      mobility.duration += curDuration - prevDuration;

      mobility.steps += sample.steps - prevSample.steps;
      firstSample = sample;
    }

    if (index === samples.length - 1) {
      const firstDate = moment(firstSample.timestamp).format("HH:mm:ss");
      const currDate = moment(sample.timestamp).format("HH:mm:ss");
      const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
      const curDuration = moment.duration(currDate, "seconds").asSeconds();

      samplesByGtw[sample.deviceID].duration += curDuration - firstDuration;
      samplesByGtw[prevSample.deviceID].steps +=
        sample.Steps - firstSample.Steps;
    }
    prevSample = sample;
  });
  samplesByGtw.MOBILITY = mobility;

  return samplesByGtw;
};

const getCrossingGtwData = (samplesByGtw, samples) => {
  const crossingsGtwData = [];

  const firstDate = moment(samples[0].timestamp).format("HH:mm:ss");
  const lastDate = moment(samples[samples.length - 1].timestamp).format(
    "HH:mm:ss"
  );

  const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
  const lastDuration = moment.duration(lastDate, "seconds").asSeconds();
  const duration = lastDuration - firstDuration;

  Object.keys(samplesByGtw).forEach(gtw => {
    crossingsGtwData.push({
      name: gtw,
      value: Math.round((samplesByGtw[gtw].duration / duration) * 100)
    });
  });
  return crossingsGtwData;
};

const getSamplesByZone = (crossingsGtwData, gateways) => {
  const samplesByZone = {};

  crossingsGtwData.forEach(data => {
    if (data.name === "MOBILITY") {
      samplesByZone.MOBILITY = [];
      samplesByZone.MOBILITY.push(data);
    }

    gateways.forEach(gateway => {
      if (data.name === gateway.name) {
        if (
          !Object.prototype.hasOwnProperty.call(samplesByZone, gateway.room)
        ) {
          samplesByZone[gateway.room] += data.value;
          samplesByZone[gateway.room] = [];
        }
        samplesByZone[gateway.room].push(data);
      }
    });
  });
  return samplesByZone;
};

const getCrossingsZoneData = samplesByZone => {
  const crossingsZoneData = [];

  Object.keys(samplesByZone).forEach((sample, index) => {
    let pourcentage = 0;
    samplesByZone[sample].forEach(data => {
      pourcentage += data.value;
    });
    crossingsZoneData.push({
      name: sample,
      value: pourcentage,
      color: COLORS[index]
    });
  });
  return crossingsZoneData;
};

const getSamplesByTime = (samples, from, interval) => {
  const samplesByTime = {};
  const startTime = moment(from, "HH:mm:ss");

  samples.forEach((sample, index) => {
    const sampleTime = moment(
      moment(samples[index].timestamp).format("HH:mm:ss"),
      "HH:mm:ss"
    );
    const duration = moment.duration(moment(sampleTime).diff(startTime));

    let diff = 0;
    if (interval === 1 || interval === 2) {
      diff = duration.asHours();
    } else diff = duration.asMinutes();

    const startTimeString = moment(startTime).format("HH:mm");

    if (!Object.prototype.hasOwnProperty.call(samplesByTime, startTimeString)) {
      samplesByTime[startTimeString] = startTimeString;
      samplesByTime[startTimeString] = [];
    }

    if (diff > interval) {
      if (interval === 1 || interval === 2) startTime.add(interval, "hours");
      else startTime.add(interval, "minutes");
    }
    samplesByTime[startTimeString].push(sample);
  });
  return samplesByTime;
};

const getStepsGtw = samplesByTime => {
  const stepsGtwData = [];

  Object.keys(samplesByTime).forEach(time => {
    const arrayObj = {};
    const firstValues = [];
    const NbStepsArr = {};
    let nbSteps = 0;
    arrayObj.name = time;
    let prevSample = {};
    let mobility = 0;

    samplesByTime[time].forEach(sample => {
      if (!Object.prototype.hasOwnProperty.call(arrayObj, sample.deviceID)) {
        arrayObj[sample.deviceID] = sample.deviceID;
        arrayObj[sample.deviceID] = 0;
        firstValues[sample.deviceID] = sample.deviceID;
        firstValues[sample.seviceID] = sample.steps;
        NbStepsArr[sample.deviceID] = 0;
      }

      if (
        Object.prototype.hasOwnProperty.call(NbStepsArr, sample.deviceID) &&
        prevSample.deviceID !== sample.deviceID &&
        NbStepsArr[sample.deviceID] !== 0
      ) {
        firstValues[sample.deviceID] =
          sample.steps - NbStepsArr[sample.deviceID];
      }

      if (
        Object.keys(prevSample).length !== 0 &&
        prevSample.deviceID !== sample.deviceID
      ) {
        mobility += sample.steps - prevSample.steps;
      }

      nbSteps = Number(sample.steps) - firstValues[sample.deviceID];
      prevSample = sample;
      NbStepsArr[sample.deviceID] = nbSteps;
      arrayObj[sample.deviceID] = nbSteps;
    });
    if (mobility !== 0) {
      arrayObj.MOBILITY = mobility;
    }

    stepsGtwData.push(arrayObj);
  });
  return stepsGtwData;
};

const getStepsByZone = (stepsGtwData, gateways) => {
  const samplesByTimeAndZone = [];

  stepsGtwData.forEach(data => {
    const samplesByZone = {};
    samplesByZone.name = data.name;
    Object.keys(data).forEach(attr => {
      gateways.forEach(gateway => {
        if (attr === gateway.name) {
          if (
            !Object.prototype.hasOwnProperty.call(samplesByZone, gateway.room)
          ) {
            samplesByZone[gateway.room] = gateway.room;
            samplesByZone[gateway.room] = 0;
          }
          samplesByZone[gateway.room] += data[attr];
        } else if (attr === "MOBILITY") {
          samplesByZone.MOBILITY = data[attr];
        }
      });
    });
    samplesByTimeAndZone.push(samplesByZone);
  });
  return samplesByTimeAndZone;
};

const getCrossingAndStepsByGtwData = (samplesByGtw, samples) => {
  const crossingAndStepsByGtwData = [];

  const firstDate = moment(samples[0].imestamp).format("HH:mm:ss");
  const lastDate = moment(samples[samples.length - 1].timestamp).format(
    "HH:mm:ss"
  );

  const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
  const lastDuration = moment.duration(lastDate, "seconds").asSeconds();
  const duration = lastDuration - firstDuration;

  const totalSteps = samples[samples.length - 1].steps - samples[0].steps;

  let obj = {};
  Object.keys(samplesByGtw).forEach(gtw => {
    obj.name = gtw;
    obj.duration = Math.round((samplesByGtw[gtw].duration / duration) * 100);
    obj.steps = Math.round((samplesByGtw[gtw].steps / totalSteps) * 100);
    crossingAndStepsByGtwData.push(obj);
    obj = {};
  });
  return crossingAndStepsByGtwData;
};

const getCrossingAndStepsByZoneData = (crossingAndStepsByGtwData, gateways) => {
  const crossingsAndStepsByZoneData = [];
  const samplesByZone = {};

  crossingAndStepsByGtwData.forEach(data => {
    if (data.name === "MOBILITY") {
      samplesByZone.MOBILITY = data;
    }
    gateways.forEach(gtw => {
      if (gtw.name === data.name) {
        if (!Object.prototype.hasOwnProperty.call(samplesByZone, gtw.room)) {
          samplesByZone[gtw.room] = gtw.room;
          samplesByZone[gtw.room] = {
            duration: 0,
            steps: 0
          };
        }
        samplesByZone[gtw.room].steps += data.steps;
        samplesByZone[gtw.room].duration += data.duration;
      }
    });
  });

  Object.keys(samplesByZone).forEach(sample => {
    crossingsAndStepsByZoneData.push({
      name: sample,
      duration: samplesByZone[sample].duration,
      steps: samplesByZone[sample].steps
    });
  });
  return crossingsAndStepsByZoneData;
};

export default class GeofencingDataCharts extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  state = {
    crossingsGtwData: [{ name: "", value: 100 }],
    crossingsZoneData: [{ name: "", value: 100 }],
    stepsGtwData: [{ name: "" }],
    stepsByZoneData: [{ name: "" }],
    crossingsAndStepsByGtwData: [{ name: "", duration: 0, steps: 0 }],
    crossingsAndStepsByZoneData: [{ name: "", duration: 0, steps: 0 }],
    tab: 0
  };

  componentWillReceiveProps = nextProps => {
    if (this.props.samples !== nextProps.samples) {
      getColorsByGtw(nextProps.gateways);
      getColorsByRoom(nextProps.gateways);

      const samplesByGtw = getSamplesByGtw(nextProps.samples);
      const crossingsGtwData = getCrossingGtwData(
        samplesByGtw,
        nextProps.samples
      );
      const samplesByZone = getSamplesByZone(
        crossingsGtwData,
        nextProps.gateways
      );
      const crossingsZoneData = getCrossingsZoneData(samplesByZone);
      const crossingsAndStepsByGtwData = getCrossingAndStepsByGtwData(
        samplesByGtw,
        nextProps.samples
      );
      const crossingsAndStepsByZoneData = getCrossingAndStepsByZoneData(
        crossingsAndStepsByGtwData,
        nextProps.gateways
      );

      const fromTime = moment(nextProps.from, "HH:mm:ss");
      const toTime = moment(nextProps.to, "HH:mm:ss");
      const duration = moment
        .duration(moment(toTime).diff(fromTime))
        .asMilliseconds();
      let interval = 5;

      if (duration <= 3600000) {
        interval = 5;
      } else if (duration >= 3600001 && duration <= 14400000) {
        interval = 30;
      } else {
        interval = 1;
      }

      const samplesByTime = getSamplesByTime(
        nextProps.samples,
        nextProps.from,
        interval
      );
      const stepsGtwData = getStepsGtw(samplesByTime);
      const stepsByZoneData = getStepsByZone(stepsGtwData, nextProps.gateways);

      this.setState({ crossingsGtwData });
      this.setState({ stepsGtwData });
      this.setState({ crossingsAndStepsByGtwData });

      this.setState({ stepsByZoneData });
      this.setState({ crossingsZoneData });
      this.setState({ crossingsAndStepsByZoneData });
    }
  };

  setTimeInterval = (interval, type) => {
    const samplesByTime = getSamplesByTime(
      this.props.samples,
      this.props.from,
      interval
    );
    const stepsGtwData = getStepsGtw(samplesByTime);

    if (type === "gateway") {
      this.setState({ stepsGtwData });
    } else {
      const stepsByZoneData = getStepsByZone(stepsGtwData, this.props.gateways);

      this.setState({ stepsByZoneData });
    }
  };

  render() {
    return (
      <Wrapper>
        <Tabs
          onChange={e => {
            const value = e.toString();
            const tab = value.substring(value.length - 1, value.length);
            this.setState({ tab: Number(tab) });
          }}
        >
          <Tab title="ROOM" active>
            <ChartTabs
              type="room"
              date={`Between ${this.props.from} and ${this.props.to}`}
              crossingsData={this.state.crossingsZoneData}
              stepsData={this.state.stepsByZoneData}
              crossingsAndStepsData={this.state.crossingsAndStepsByZoneData}
              setTimeInterval={this.setTimeInterval}
              display={this.state.tab === 0 ? "block" : "none"}
              colorsByType={colorsByRoom}
            />
          </Tab>
          <Tab title="GATEWAY" active style={{ padding: 0 }}>
            <ChartTabs
              type="gateway"
              date={`Entre ${this.props.from} et ${this.props.to}`}
              crossingsData={this.state.crossingsGtwData}
              stepsData={this.state.stepsGtwData}
              crossingsAndStepsData={this.state.crossingsAndStepsByGtwData}
              setTimeInterval={this.setTimeInterval}
              display={this.state.tab === 1 ? "block" : "none"}
              colorsByType={colorsByGtw}
            />
          </Tab>
        </Tabs>
      </Wrapper>
    );
  }
}

GeofencingDataCharts.propTypes = {
  samples: React.PropTypes.array,
  gateways: React.PropTypes.array,
  from: React.PropTypes.string,
  to: React.PropTypes.string
};
