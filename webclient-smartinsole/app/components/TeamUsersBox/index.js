import React from 'react';
import Grid from 'components/Grid';

const TeamUsersBox = (props) =>
  <Grid
    columns={props.columns}
    datas={props.users}
    actions={false}
  >
  </Grid>;

TeamUsersBox.propTypes = {
  users: React.PropTypes.array,
  columns: React.PropTypes.array,
};

export default TeamUsersBox;
