import styled from 'styled-components';

export const Wrapper = styled.div`
  .leaflet-draw-section a > span {
    display: none;
  }
`;
