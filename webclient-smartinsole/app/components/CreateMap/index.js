import React from 'react';
import { Map, TileLayer, FeatureGroup } from 'react-leaflet';
import L from 'leaflet';
import { EditControl } from 'react-leaflet-draw';
import { Wrapper } from './styles';

// work around broken icons when using webpack, see https://github.com/PaulLeCam/react-leaflet/issues/255

class CreateMap extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      zoom: props.map ? 19 : 6,
      latitude: props.latitude ? props.latitude : 46.309,
      longitude: props.longitude ? props.longitude : 1.989,
    };

    this.onCreated = this.onCreated.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onEdited = this.onEdited.bind(this);
    this.onDeleted = this.onDeleted.bind(this);
    this.onFeatureGroupReady = this.onFeatureGroupReady.bind(this);
  }

  onEdited = () => {
    this.onChange();
  }

  onCreated() {
    this.onChange();
  }

  onDeleted = () => {
    this.onChange();
  }

  onChange = () => {
    // this.editableFG contains the edited geometry, which can be manipulated through the leaflet API

    if (!this.editableFG) {
      return;
    }

    const geojsonData = this.editableFG.leafletElement.toGeoJSON();

    this.props.setMap(geojsonData);
  }

  onFeatureGroupReady = (reactFGref) => {
    // populate the leaflet FeatureGroup with the geoJson layers
    let mapJson = getGeoJson();
    if (reactFGref) {
      if (this.props.map && this.counter === 0) {
        const mapFeatures = JSON.parse(this.props.map).features;
        mapJson = {
          type: 'FeatureCollection',
          features: [...mapJson.features, ...mapFeatures],
        };
      }

      const leafletFG = reactFGref.leafletElement;

      const leafletGeoJSON = new L.GeoJSON(mapJson);

      leafletGeoJSON.eachLayer((layer) => {
        leafletFG.addLayer(layer);
      });
      // store the ref for future access to content
      this.editableFG = reactFGref;
      this.counter += 1;
    }
  }
  counter = 0;
  editableFG = null

  render() {
    return (
      <Wrapper className="col s12">

        <Map
          style={{ height: 400 }}
          center={[this.state.latitude, this.state.longitude]}
          zoom={this.state.zoom}
          zoomControl
          className="map"
          maxZoom={19}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
          <FeatureGroup ref={(reactFGref) => { this.onFeatureGroupReady(reactFGref); }}>
            <EditControl
              position="topright"
              onEdited={this.onEdited}
              onCreated={this.onCreated}
              onDeleted={this.onDeleted}
              draw={{
                circlemarker: false,
                marker: false,
              }}
            />
          </FeatureGroup>
        </Map>
      </Wrapper>
    );
  }
}

function getGeoJson() {
  return {
    type: 'FeatureCollection',
    features: [
      {
      },
    ],
  };
}

CreateMap.propTypes = {
  setMap: React.PropTypes.func,
  map: React.PropTypes.string,
  latitude: React.PropTypes.number,
  longitude: React.PropTypes.number,
};

export default CreateMap;
