import React from 'react';
import Row from 'components/Row';
import { Col, Icon, Wrapper, H5 } from './styles';

export default class IconInfo extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  state = {
    data: this.props.data,
    date: '',
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.data !== nextProps.data) {
      this.setState({ data: nextProps.data });
    }

    if (this.props.date !== nextProps.date) {
      this.setState({ date: nextProps.date });
    }
  }

  render() {
    let info = <div></div>;

    if (!this.props.node) {
      info = (
        <Row className="no-margin">
          <Col l={3} m={3} s={6} className="icon">
            <Icon medium>{this.props.icon}</Icon>
          </Col>
          <Col l={9} m={9} s={6}>
            <div className={'data-value'}>
              {this.state.data}
              <span className="unit"> {this.props.unit}</span>
            </div>
          </Col>
        </Row>);
    } else {
      info = (
        <Row className="no-margin">
          <Col l={12} m={12} s={12} className="icon">
            { this.props.node }
          </Col>

        </Row>
      );
    }

    return (
      <Wrapper>
        <Row className={`no-margin ${this.props.className}`}>
          <H5>
            {this.props.label}
          </H5>
          { this.props.date ?
            <div className="right-align date">
              {this.props.date}
            </div> : ''
          }
        </Row>
        <Row className="no-margin">
          { info }
        </Row>
      </Wrapper>);
  }
}

IconInfo.propTypes = {
  label: React.PropTypes.string,
  className: React.PropTypes.string,
  icon: React.PropTypes.string,
  data: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
    React.PropTypes.array,
  ]),
  unit: React.PropTypes.string,
  date: React.PropTypes.string,
  node: React.PropTypes.node,
};
