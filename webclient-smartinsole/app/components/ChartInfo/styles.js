import { Col as MCol, Icon as MIcon } from 'react-materialize';
import styled from 'styled-components';

export const Wrapper = styled.div`
  .right-align {
    font-size: 1.1rem;
    position: absolute;
    text-align: right;
    right: 0;
    top: 30px;

  }

  @media (max-width: 992px) {
    top: 10px;
    font-size: 1.2rem;

    @media (max-width: 600px) {
      top: 40px;
    }

  }

  .row {
    position: relative;
  }

  .unit {
    font-size: 1.2rem;
    position: absolute;
    right: 0;
    bottom: -10px;
  }

  @media (min-width: 1225px) and (max-width: 1350px) {
    .iconinfo-title {
      padding-right: 50px;
    }
  }

`;

export const H5 = styled.h5`
  margin-bottom: 15px;
`;

export const Col = styled(MCol)`
  padding: 0 !important;

  & .data-value {
    font-size: 3rem;
    text-align: right;
  }

  & .white-box {
    background: #FFF;
    padding: 15px;
  }

  .icon {
    padding-left: 0 !important;
  }

  @media (max-width: 992px) {
    & {
      padding: 0;
    }
  }
`;

export const Icon = styled(MIcon)`
  border-radius: 100%;
  vertical-align: top;

`;
