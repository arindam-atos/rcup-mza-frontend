import React from 'react';
import Row from 'components/Row';
import { Col, Icon, Wrapper } from './styles';

export default class IconInfo extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  state = {
    data: this.props.data,
    date: '',
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.data !== nextProps.data) {
      this.setState({ data: nextProps.data });
    }

    if (this.props.date !== nextProps.date) {
      this.setState({ date: nextProps.date });
    }
  }

  render() {
    return (
      <Wrapper>
        <Row>
          <Col
            s={3}
          >
            <Icon
              color={this.props.color}
            >
              {this.props.icon}
            </Icon>
          </Col>
          <Col
            s={9}
          >
            <div>
              <p className="info-label">{this.props.label}</p>
              <p className="data">{this.props.data} <span>{this.props.unit}</span></p>
            </div>
          </Col>
        </Row>
        { this.props.date ?
          <Row className="datetime">
            <div>
              <Icon>event</Icon>
              <span>{this.props.date}</span>
            </div>
          </Row> : ''
        }
      </Wrapper>);
  }
}

IconInfo.propTypes = {
  label: React.PropTypes.string,
  color: React.PropTypes.string,
  icon: React.PropTypes.string,
  data: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
    React.PropTypes.array,
  ]),
  unit: React.PropTypes.string,
  date: React.PropTypes.string,
};
