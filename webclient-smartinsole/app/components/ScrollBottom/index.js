import React from 'react';

export default class ScrollBottom extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    this.scrollView.scrollTop = this.scrollView.scrollHeight;
    this.scrollView.scrollIntoView({ behavior: 'smooth', block: 'end' });
  }

  render() {
    return (
      <div
        style={{ maxHeight: '300px', overflowY: 'auto' }}
        ref={(el) => { this.scrollView = el; }}
      >
        { this.props.node }
      </div>
    );
  }
}

ScrollBottom.propTypes = {
  node: React.PropTypes.node,
};
