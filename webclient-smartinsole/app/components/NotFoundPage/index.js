/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  margin: auto;
  margin-top: 20%;
  width: 50%;
  text-align: center;

  h1 {
    font-size: 45px;
    @media (max-width: 600px) {
      font-size: 30px;
    }
  }
`;

export default class NotFound extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper>
        <h1>
          404 Not Found
        </h1>
        <p>{'Sorry but the page does not exist'}</p>
      </Wrapper>
    );
  }
}
