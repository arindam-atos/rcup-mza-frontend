import { Col as MCol, Icon as MIcon } from 'react-materialize';
import styled from 'styled-components';

export const Col = styled(MCol)`
  padding: 0.8rem !important;

  & .white-box {
    padding: 15px;

    border-radius: 4px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
    box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
    background-color: #fff;
  }

  @media (max-width: 992px) {
    & {
      padding: 0;
      margin: 0;
    }
  }
`;

export const Icon = styled(MIcon)`
  border-radius: 100%;
  vertical-align: top;
`;
