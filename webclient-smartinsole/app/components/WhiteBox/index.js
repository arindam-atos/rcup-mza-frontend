import React from 'react';
import { Col } from './styles';

const WhiteBox = (props) =>
  <Col s={props.s || 12} m={props.m || 3} l={props.l || 3} style={props.style} className={props.className || ''}>
    <div className={'white-box'}>
      {props.node}
    </div>
  </Col>;

WhiteBox.propTypes = {
  node: React.PropTypes.node.isRequired,
  s: React.PropTypes.number,
  m: React.PropTypes.number,
  l: React.PropTypes.number,
  style: React.PropTypes.object,
  className: React.PropTypes.string,
};

export default WhiteBox;
