import styled from 'styled-components';

export const Wrapper = styled.div`
  .input-mac-address .col{
    margin-top: 0;
    @media (min-width: 992px) {
      width: 12.333333%;
    }

    @media (max-width: 600px) {
      width: 15.333333%
    }

    input {
      text-align: center;
      height: 2.6rem;
    }
  }

`;

export const Colon = styled.span`
  float: left;
  margin-top: 10px;
`;
