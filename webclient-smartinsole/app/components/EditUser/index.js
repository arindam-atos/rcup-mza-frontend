/*
 *  @Project    WebClient WorkerSole
 *
 *  @File       EditUser.js
 *
 *  @summary    View to edit user
 *
 *  @author     PHONG Stéphane
 *
 */

import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import Row from 'components/Row';
import Col from 'components/Col';
import { Button, Icon } from 'react-materialize';
import { Wrapper, Colon } from './styles';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));
const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

class EditUser extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    id: '',
    email: '',
    macAddress: '',
    cur_password: '',
    new_password: '',
    role: '',
    firstname: '',
    lastname: '',
    height: 0,
    weight: 0,
    shoeSize: 0,
    age: 0,
  };

  componentWillMount() {
    const { userId } = this.props.params;

    this.props.GetUserRequest(currentCompany.companyId, userId);
    this.props.setPageTitle('Edit user');
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.user !== nextProps.user) {
      const { id, email, macAddress, macAdd1, macAdd2, macAdd3, macAdd4, macAdd5, macAdd6, role, firstname, lastname, height, weight, shoeSize, age } = nextProps.user;

      this.setState({
        id,
        email,
        macAddress,
        macAdd1,
        macAdd2,
        macAdd3,
        macAdd4,
        macAdd5,
        macAdd6,
        role,
        firstname,
        lastname,
        height,
        weight,
        shoeSize,
        age,
      });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { macAdd1, macAdd2, macAdd3, macAdd4, macAdd5, macAdd6 } = this.state;

    this.state.macAddress = `${macAdd1}:${macAdd2}:${macAdd3}:${macAdd4}:${macAdd5}:${macAdd6}`;

    this.props.EditUserRequest(currentCompany.companyId, this.state);
  }

  render() {
    const { email, macAdd1, macAdd2, macAdd3, macAdd4, macAdd5, macAdd6, firstname, lastname,
    height, weight, shoeSize, age } = this.props.user;

    const options = [
      <option key="1" value="worker">Ouvrier</option>,
    ];

    if (loggedUser.role === 'director') {
      options.push(
        <option key="2" value="manager">Manageur</option>
      );
    }

    if (loggedUser.role === 'admin') {
      options.push(
        <option key="2" value="manager">Manageur</option>,
        <option key="3" value="director">Directeur</option>,
        <option key="4" value="admin">Administrateur</option>,
      );
    }

    return (
      <Wrapper>
        <Form
          className="stayActive"
          node={[
            <Row
              key="user-add"
            >
              <Input
                key="email"
                label="Email"
                name="email"
                type="email"
                cb={this.setField}
                defaultValue={email}
                value={email}
                s={12}
                m={6}
                l={6}
                className={'validate'}
              />
              <Col
                s={12}
                m={6}
                l={6}
                className="input-mac-address"
              >
                <label className="active" htmlFor="add">
                  Adresse Mac
                </label>
                <div id="add">
                  <Input
                    key={'macAdd1'}
                    name={'macAdd1'}
                    value={macAdd1}
                    cb={this.setField}
                    maxLength={2}
                    s={2}
                    m={2}
                    l={2}
                  />
                </div>
                <Colon>:</Colon>
                <Input
                  key={'macAdd2'}
                  name={'macAdd2'}
                  value={macAdd2}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd3'}
                  name={'macAdd3'}
                  value={macAdd3}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd4'}
                  name={'macAdd4'}
                  value={macAdd4}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd5'}
                  name={'macAdd5'}
                  value={macAdd5}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd6'}
                  name={'macAdd6'}
                  value={macAdd6}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
              </Col>
            </Row>,
            <Row
              key="user-pwd"
            >
              <Input
                key="cur_password"
                label="Current Password"
                name="cur_password"
                type="password"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
                placeholder="**********"
              />
              <Input
                key="new_password"
                label="New Password"
                name="new_password"
                type="password"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
                placeholder="**********"
              />
            </Row>,
            <Row
              key="user-role"
            >
              <Select
                label="Select user's role"
                key="role"
                name="role"
                multiple={false}
                defaultVal={this.state.role}
                options={options}
                cb={this.setField}
              />
            </Row>,
            <Row
              key="user-name"
            >
              <Input
                key="firstname"
                label="Firstname"
                name="firstname"
                cb={this.setField}
                value={firstname}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="lastname"
                label="Lastname"
                name="lastname"
                cb={this.setField}
                value={lastname}
                s={12}
                m={6}
                l={6}
              />
            </Row>,
            <Row
              key="user-properties"
            >
              <Input
                key="height"
                label="Height in cm"
                name="height"
                cb={this.setField}
                value={height}
                s={6}
                m={6}
                l={6}
              />
              <Input
                key="weight"
                label="Weight in kg"
                name="weight"
                cb={this.setField}
                value={weight}
                s={6}
                m={6}
                l={6}
              />
              <Input
                key="shoeSize"
                label="Shoes size"
                name="shoeSize"
                cb={this.setField}
                value={shoeSize}
                s={6}
                m={6}
                l={6}
              />
              <Input
                key="age"
                label="Age"
                name="age"
                cb={this.setField}
                value={age}
                s={6}
                m={6}
                l={6}
              />
            </Row>,
            <Row
              key="submit"
            >
              <Button
                key="submit"
                id="submit"
                waves="light"
                className="blue"
                onClick={this.handleSubmit}
              >
                Edit
                <Icon left>mode_edit</Icon>
              </Button>
            </Row>,
          ]}
        />
      </Wrapper>
    );
  }
}

EditUser.propTypes = {
  setPageTitle: React.PropTypes.func,
  params: React.PropTypes.object.isRequired,
  GetUserRequest: React.PropTypes.func.isRequired,
  EditUserRequest: React.PropTypes.func.isRequired,
  user: React.PropTypes.object,
};

export default EditUser;
