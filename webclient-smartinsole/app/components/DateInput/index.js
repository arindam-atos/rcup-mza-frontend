import React from 'react';
import Input from 'components/Input';

const options = {
  selectMonths: true, // Creates a dropdown to control month
  selectYears: false, // Creates a dropdown of 15 years to control year,
  monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mars', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
  weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
  weekdaysLetter: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
  format: 'yyyy-mm-dd',
  close: 'Fermer',
  today: 'AUJOURD\'HUI',
  clear: false,
};

const DateInput = (props) =>
  <Input
    label={props.label}
    name={props.name}
    type={'date'}
    s={props.s || 6}
    m={props.m || 3}
    l={props.l || 3}
    value={props.value}
    options={options}
    cb={props.cb}
  />;

DateInput.propTypes = {
  label: React.PropTypes.string,
  name: React.PropTypes.string,
  cb: React.PropTypes.func,
  value: React.PropTypes.string,
  s: React.PropTypes.number,
  m: React.PropTypes.number,
  l: React.PropTypes.number,
};

export default DateInput;
