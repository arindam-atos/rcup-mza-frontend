import styled from 'styled-components';

export const Wrapper = styled.div`
  .datetime {
    color: #777;
    border-top: 1px solid #777;

    padding-top: 20px;
    i {
      float: left;
    }

    span {
      float: right;
    }
  }

  .col {
    text-align: center;
    padding: 10px;
    .room-info {
      font-size: 17px;
      width: 100%;
      border: 1px solid #777;
      float: left;
      display: inline-block;
      vertical-align: middle;
      padding: 20px;
    }
    .room-name {
      float: left;
      text-align: left;
    }
    .room-persons {
      float: right;
      text-align: right;
      display: inline-flex;
      vertical-align: middle;

      i {
        line-height: 25px;
      }
    }
  }
`;
