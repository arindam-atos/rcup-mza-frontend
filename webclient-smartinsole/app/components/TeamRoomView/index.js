import React from "react";
import {Col, Icon, Row} from "react-materialize";
import moment from "moment/moment";
import {Wrapper} from "./styles";

const currentCompany = JSON.parse(localStorage.getItem("currentCompany"));

class TeamRoomView extends React.Component {
    //  eslint-disable-line react/prefer-stateless-function

    state = {
        rooms: [
            {
                name: "",
                nbPerson: 0
            }
        ],
        addresses: [],
        lastSamples: [],
        timeElapsed: 0
    };

    componentWillMount() {
        this.props.AllRoomsRequest(currentCompany.companyId);
    }

    componentDidMount = () => {
        this.interval = setInterval(() => this.tick(), 1000);
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.rooms !== nextProps.rooms) {
            const rooms = [];
            nextProps.rooms.forEach(room => {
                rooms.push({
                    name: room.name,
                    gateways: room.gateways,
                    nbPerson: 0
                });
            });
            this.setState({rooms});
        }
        if (this.props.teamUsers !== nextProps.teamUsers) {
            const addresses = [];
            nextProps.teamUsers.forEach(user => {
                addresses.push(user.macAddress);
            });
            if (addresses.length > 0) {
                this.props.getSamples(
                    currentCompany.companyId,
                    addresses,
                    moment().format("YYYY-MM-DD"),
                    moment().format("YYYY-MM-DD")
                );
            }
            for (let i = 0; i < this.state.rooms.length; i += 1) {
                this.state.rooms[i].nbPerson = 0;
            }
            this.setState({
                addresses,
                timeElapsed: 0
            });
        }
        if (this.props.lastSamples !== nextProps.lastSamples) {
            const nowDate = moment();
            Object.keys(nextProps.lastSamples).forEach(userSample => {
                if (userSample && nextProps.lastSamples[userSample] && nextProps.lastSamples[userSample].length > 0 && nowDate.diff(nextProps.lastSamples[userSample][0].Timestamp, "minutes") < 1) {
                    this.state.rooms.forEach((room, i) => {
                        room.gateways.forEach(gtw => {
                            if (nextProps.lastSamples[userSample][0].deviceID === gtw.name) {
                                this.state.rooms[i].nbPerson += 1;
                            }
                        });
                    });
                }
            });
            this.setState({lastSamples: nextProps.lastSamples});
        }
    }

    componentWillUnmount = () => {
        clearInterval(this.interval);
    };

    tick = () => {
        if (this.state.timeElapsed === 60) {
            this.props.getSamples(
                currentCompany.companyId,
                this.state.addresses,
        moment().format("YYYY-MM-DD"),
        moment().format("YYYY-MM-DD")
            );
            for (let i = 0; i < this.state.rooms.length; i += 1) {
                this.state.rooms[i].nbPerson = 0;
            }
            this.setState({
                timeElapsed: 0
            });
        }
        this.setState({
            timeElapsed: this.state.timeElapsed + 1
        });
    };

    render() {
        return (
            <Wrapper>
                <Row>
                    {this.state.rooms.map(room => (
                        <Col s={12} m={3} l={3} key={`room-${room.name}`}>
                            <div className="room-info">
                                <div className="room-name">{room.name}:</div>
                                <div className="room-persons">
                                    {room.nbPerson}
                                    <Icon tiny>person</Icon>
                                </div>
                            </div>
                        </Col>
                    ))}
                </Row>
                <Row className="datetime">
                    <div>
                        <Icon>event</Icon>
                        <span>{`Updated ${this.state.timeElapsed}`} seconds ago</span>
                    </div>
                </Row>
            </Wrapper>
        );
    }
}

TeamRoomView.propTypes = {
    rooms: React.PropTypes.array,
    AllRoomsRequest: React.PropTypes.func,
    teamUsers: React.PropTypes.array,
    lastSamples: React.PropTypes.object,
    getSamples: React.PropTypes.func
};

export default TeamRoomView;
