import React from 'react';
import {browserHistory} from 'react-router';
import Logo from 'public/logo/logo.png';
import Input from 'components/Input';
import CardPanel from 'containers/CardPanel';
import {Button, Col, Row, Wrapper} from './styles';

export default class ResetPwd extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

    state = {
        username: '',
    }

    setField = (key, value) => {
        this.setState({[key]: value});
    }

    handleSubmit = () => {
        this.props.ResetPwdRequest(this.state.username);
    }

    render() {
        return (
            <Wrapper>
                <div className="form">
                    <div className="logo">
                        <img src={Logo} alt="logo"/>
                    </div>
                    <CardPanel/>
                    <Row
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        <p>Veuillez saisir votre adresse e-mail</p>
                    </Row>
                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                        >
                            <Input
                                label="Identifiant"
                                cb={this.setField}
                                name="username"
                                type="email"
                                className={'validate'}
                                icon="account_circle"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <Button
                                waves="light"
                                type="submit"
                                onClick={this.handleSubmit}
                                className="submit"
                            >
                                Envoyer
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <Button
                                onClick={() => browserHistory.push('/login')}
                                href="/login"
                                className="forgotten-pwd"
                            >
                                Retour
                            </Button>
                        </Col>
                    </Row>
                </div>
            </Wrapper>
        );
    }
}

ResetPwd.propTypes = {
    ResetPwdRequest: React.PropTypes.func.isRequired,
};
