import styled from 'styled-components';
import bg from 'public/login-bg.jpg';

import {
  Row as MRow,
  Col as MCol,
  Button as MButton,
} from 'react-materialize';

export const Wrapper = styled.div`
  background: url(${bg});
  width: 100%;
  height: 100%;
  display: flex;
  background-size: cover;
  background-position: 50%;
  .form {
    padding: 40px 0px;
    border-radius: 4px;
    background-color: rgba(0, 0, 0, 0.4)
    margin: auto;
    width: 30%;
    color: #FFF !important;
  }

  @media (min-width: 992px) {
    height: 100vh;
  }

  @media (max-width: 1200px) {
    .form {
      width: 50%;
    }
  }


  @media (max-width: 992px) {
    min-height: 100vh;
    .form {
      width: 60%;
      height: 100%;
    }
  }

  @media (max-width: 600px) {
    min-height: 100vh;
    .form {
      width: 80%;
      height: 100%;
    }
  }

  label, .active {
    color: #FFF !important;
  }

  input:focus {
    border-bottom: 1px solid #FFF !important;
    box-shadow: 0 1px 0 0 #FFF !important;
  }

  .logo {
    text-align: center;
    vertical-align: middle;
    margin-bottom: 20px;
    @media (max-width: 992px) {
      margin-left: 0;
    }

  }

  .logo img {
    width: 80px;
    height: 90px;
  }

  i {
    line-height: 50px;
  }

`;

export const Row = styled(MRow)`
  &.align-center {
    text-align: center;
  }
`;

export const Col = styled(MCol)`
  & a {
    line-height: 36px;
    color: #FFF;
  }

  & a:hover {
    color: #039be5;
  }

  &.pwd {
    text-align: left;
    @media (max-width: 600px) {
      text-align: center;
    }
  }

  &.submit {
    text-align: right;
    @media (max-width: 600px) {
      margin-top: 30px;
      text-align: center;
    }
  }

`;

export const Button = styled(MButton)`
  &.submit {
    background-color: #eb4842
    width: 60%;
  }
  &.forgotten-pwd {
    background-color: transparent;
    box-shadow: none;
    text-transform: none;
  }
  &.forgotten-pwd:hover {
    color: #bdbdbd;
  }

`;
