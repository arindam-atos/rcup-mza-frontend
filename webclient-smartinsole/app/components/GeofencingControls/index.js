import React from 'react';
import moment from 'moment/moment';
import { Button, Icon } from 'react-materialize';
import DateInput from 'components/DateInput';
import Input from 'components/Input';
import Select from 'components/Select';
import WhiteBox from 'components/WhiteBox';
import Row from 'components/Row';
import Col from 'components/Col';
import { timeInterval } from './constants';
import { Wrapper } from './styles';

export default class GeofencingControls extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    userOptions: [],
    userId: '',
    timeOptions: [],
    day: moment().format('YYYY-MM-DD'),
    from: {
      value: '12:00',
      index: 24,
    },
    to: {
      value: '12:30',
      index: 26,
    },
    switchInterval: false,
  };

  componentWillMount = () => {
    const timeOptions = [];

    timeInterval.forEach((time, index) => {
      timeOptions.push(<option key={`time-${time}`} value={index}>{time}</option>);
    });

    this.setState({ timeOptions });
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.users !== nextProps.users) {
      const usersOption = [];

      nextProps.users.forEach((user) => {
        usersOption.push(
          <option key={`user-${user.id}`} value={user.id}>{`${user.firstname} ${user.lastname}`}</option>
        );
      });
      this.setState({ usersOption });
      this.setState({ userId: nextProps.users[0].id });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  setStringTimeInterval = (key, value) => {
    let index = 0;

    // use of for loop to use break
    // This loop is used to convert input time to select input time
    for (let i = 0; i < timeInterval.length; i += 1) {
      if (value[0] === timeInterval[i][0] && value[1] === timeInterval[i][1]) {
        index = i;
        break;
      }
    }

    this.setState({
      [key]: {
        index,
        value,
      },
    });
  }

  setSwitchInterval = () => {
    if (this.state.switchInterval) {
      this.setState({ switchInterval: false });
    } else {
      this.setState({ switchInterval: true });
    }
  }

  checkTimeInterval = (key, value) => {
    const val = Number(value);

    if (key === 'from' && val >= this.state.to.index) {
      this.setState({
        to: {
          index: val + 1,
          value: timeInterval[val + 1],
        },
      });
    } else if (key === 'to' && val <= this.state.from.index) {
      this.setState({
        from: {
          index: val - 1,
          value: timeInterval[val - 1],
        },
      });
    }

    this.setState({
      [key]: {
        index: value,
        value: timeInterval[value],
      },
    });
  }

  handleSubmit = () => {
    const payload = {
      from: this.state.from.value,
      to: this.state.to.value,
      day: this.state.day,
    };

    this.props.handleSubmit(payload);
  }

  render() {
    return (
      <Wrapper>
        <WhiteBox
          s={12}
          m={12}
          l={4}
          className={'whitebox-2'}
          node={[
            <Row
              key={'data-time'}
            >
              <Col s={12}>
                <Select
                  label={'Users'}
                  key={'userId'}
                  name={'userId'}
                  multiple={false}
                  defaultVal={this.props.userId}
                  options={this.state.usersOption}
                  cb={this.props.setField}
                  s={12}
                  m={12}
                  l={12}
                />
              </Col>
              <Col s={12}>
                <DateInput
                  label={'Day'}
                  name={'day'}
                  value={moment().format('YYYY-MM-DD')}
                  s={12}
                  m={12}
                  l={12}
                  cb={this.setField}
                />
              </Col>

              {!this.state.switchInterval ?
                <Col s={5} m={5} l={5}>
                  <Select
                    label="From"
                    name={'from'}
                    multiple={false}
                    defaultVal={this.state.from.index}
                    options={this.state.timeOptions}
                    cb={this.checkTimeInterval}
                    s={12}
                    m={12}
                    l={12}
                  />
                </Col> :
                <Col s={5} m={5} l={5}>
                  <Input
                    label={'From'}
                    name={'from'}
                    key={'from'}
                    labelClassName="active"
                    value={this.props.from}
                    placeholder={'12:00'}
                    s={12}
                    m={12}
                    l={12}
                    cb={this.setStringTimeInterval}
                  />
                </Col>
              }
              {!this.state.switchInterval ?
                <Col s={5} m={5} l={5}>
                  <Select
                    label="To"
                    name={'to'}
                    multiple={false}
                    defaultVal={this.state.to.index}
                    options={this.state.timeOptions}
                    cb={this.checkTimeInterval}
                    s={12}
                    m={12}
                    l={12}
                  />
                </Col> :
                <Col s={5} m={5} l={5}>
                  <Input
                    label={'To'}
                    name={'to'}
                    key={'to'}
                    labelClassName="active"
                    placeholder={'13:00'}
                    value={this.props.to}
                    s={12}
                    m={12}
                    l={12}
                    cb={this.setStringTimeInterval}
                  />
                </Col>
              }
              <Col s={2} m={2} l={2}>
                <Button
                  key="switch-input"
                  id="switch-input"
                  onClick={this.setSwitchInterval}
                >
                  <Icon>swap_horiz</Icon>
                </Button>
              </Col>

            </Row>,
            <Row
              key={'speed'}
            >
              <Col
                s={5}
                m={5}
                l={5}
                style={{ textAlign: 'right' }}
              >
                <Button
                  key="speeddown"
                  id="speeddown"
                  onClick={this.props.speeddown}
                  disabled={this.props.speed <= -1 * this.props.speedMax}
                >
                  <Icon>fast_rewind</Icon>
                </Button>
              </Col>
              <Col
                s={2}
                m={2}
                l={2}
                style={{
                  fontSize: '22px',
                  textAlign: 'center',
                  lineHeight: '36px',
                  height: '36px',
                }}
              >
                x{this.props.speed}
              </Col>
              <Col
                s={4}
                m={4}
                l={4}
              >
                <Button
                  key="speedup"
                  id="speedup"
                  disabled={this.props.speed >= this.props.speedMax}
                  onClick={this.props.speedup}
                >
                  <Icon>fast_forward</Icon>
                </Button>
              </Col>
            </Row>,
            <Row
              key="submit"
              style={{
                marginTop: '20px',
              }}
            >
              <Col
                s={12}
                m={12}
                l={12}
                style={{ textAlign: 'center' }}
              >
                <Button
                  key="submit"
                  id="submit"
                  waves="light"
                  onClick={this.handleSubmit}
                >
                  START
                  <Icon left>play_arrow</Icon>
                </Button>
              </Col>
            </Row>,
          ]}
        />
      </Wrapper>
    );
  }
}

GeofencingControls.propTypes = {
  userId: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
  ]),
  users: React.PropTypes.array,
  speed: React.PropTypes.number,
  speedMax: React.PropTypes.number,
  speedup: React.PropTypes.func.isRequired,
  speeddown: React.PropTypes.func.isRequired,
  from: React.PropTypes.string,
  to: React.PropTypes.string,
  handleSubmit: React.PropTypes.func.isRequired,
  setField: React.PropTypes.func.isRequired,
};
