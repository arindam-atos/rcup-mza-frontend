import styled from 'styled-components';

export const Wrapper = styled.div`
  #switch-input {
    width: 50px;
    padding: 0 1rem;
    margin-top: 1.5rem;
  }
`;
