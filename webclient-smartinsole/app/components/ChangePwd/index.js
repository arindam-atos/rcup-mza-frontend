import React from 'react';
import {browserHistory} from 'react-router';
import Logo from 'public/logo/logo.png';
import Input from 'components/Input';
import CardPanel from 'containers/CardPanel';
import {Button, Col, Row, Wrapper} from './styles';

class ChangePwd extends React.Component {// eslint-disable-line react/prefer-stateless-function

    state = {
        username: this.props.location.query.username,
        pwd: '',
        checkPwd: '',
    }

    componentWillMount = () => {
        if (this.props.location.query.access_token === 'undefined' || this.props.location.query.username === 'undefined') {
            browserHistory.push('/login');
        } else {
            // localStorage.setItem('loggedUser', JSON.stringify({
            //   accessToken: this.props.location.query.access_token,
            // }));
        }
    }

    setField = (key, value) => {
        this.setState({[key]: value});
    }

    handleSubmit = () => {
        if (this.state.pwd !== this.state.checkPwd) {
            this.props.setCard('Les mots de passes ne sont pas identiques', 'error');
        } else {
            this.props.ChangePwdRequest(this.state.username, this.state.pwd);
        }
    }

    render() {
        return (
            <Wrapper>
                <div className="form">
                    <div className="logo">
                        <img src={Logo} alt="logo"/>
                    </div>
                    <CardPanel/>

                    <Row className={'disabled'}>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                        >
                            <Input
                                value={this.props.location.query.username}
                                label="Email"
                                cb={this.setField}
                                name="email"
                                type="email"
                                className="validate"
                                icon="account_circle"
                                disabled
                            />
                        </Col>
                    </Row>

                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                        >
                            <Input
                                label="Password"
                                cb={this.setField}
                                name="pwd"
                                type="password"
                                className={'validate'}
                                icon="vpn_key"
                            />
                        </Col>
                    </Row>


                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                        >
                            <Input
                                label="Type again your password"
                                cb={this.setField}
                                name="checkPwd"
                                type="password"
                                className={'validate'}
                                icon="vpn_key"
                            />
                        </Col>
                    </Row>


                    <Row
                        style={{
                            marginTop: '30px',
                        }}
                    >
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <Button
                                waves="light"
                                type="submit"
                                onClick={this.handleSubmit}
                                className="submit"
                            >
                                Save
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <Button
                                onClick={() => browserHistory.push('/login')}
                                href="/login"
                                className="forgotten-pwd"
                            >
                                Retour
                            </Button>
                        </Col>
                    </Row>
                </div>
            </Wrapper>
        );
    }
}

ChangePwd.propTypes = {
    ChangePwdRequest: React.PropTypes.func.isRequired,
    setCard: React.PropTypes.func,
    location: React.PropTypes.object.isRequired,
};

export default ChangePwd;
