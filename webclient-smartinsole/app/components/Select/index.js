import React from 'react';

class Select extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  state = {
    defaultVal: this.props.defaultVal,
  }

  componentDidMount() {
    $(`select#${this.props.name}`).material_select();
    $(`select#${this.props.name}`).on('change', this.handleChange.bind(this));
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.defaultVal !== nextProps.defaultVal) {
      this.setState({ defaultVal: nextProps.defaultVal });
    }
  }

  componentDidUpdate() {
    $(`select#${this.props.name}`).material_select();
  }

  handleChange = () => {
    const val = $(`select#${this.props.name}`).val();
    this.props.cb(this.props.name, val);
  }

  render() {
    return (
      <div className={`input-field col s${this.props.s || 12} m${this.props.m || 12} l${this.props.l || 12}`} style={this.props.style}>
        <select key={this.props.name} id={this.props.name} multiple={this.props.multiple} value={this.state.defaultVal} onChange={this.handleChange}>
          {this.props.options}
        </select>
        <label htmlFor={this.props.name} id={`id-${this.props.name}`}>{this.props.label}</label>
      </div>
    );
  }
}

Select.propTypes = {
  label: React.PropTypes.string,
  defaultVal: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
    React.PropTypes.array,
  ]),
  name: React.PropTypes.string.isRequired,
  options: React.PropTypes.array,
  multiple: React.PropTypes.bool,
  cb: React.PropTypes.func,
  s: React.PropTypes.number,
  m: React.PropTypes.number,
  l: React.PropTypes.number,
  style: React.PropTypes.object,
};

export default Select;
