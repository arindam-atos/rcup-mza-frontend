import React from 'react';
import Grid from 'components/Grid';
import { Row } from 'react-materialize';
import WhiteBox from 'components/WhiteBox';

export class ListCompanies extends React.Component {//  eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      companies: this.props.companies,
    };
  }

  componentWillMount() {
    this.props.AllCompaniesRequest();
    this.props.setPageTitle('Companies list');
  }

  render() {
    const columns = ['id', 'Name', 'Created', 'Last Updated'];

    if (this.props.companies) {
      this.state.companies = this.props.companies;
    }

    return (
      <Row>
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Grid
              key={'list-companies'}
              columns={columns}
              datas={this.state.companies}
              deleteRequest={this.props.DeleteCompanyRequest}
              addUrl="add-company"
              editUrl="edit-company"
              actions
            />,
          ]}
        />
      </Row>
    );
  }
}

ListCompanies.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllCompaniesRequest: React.PropTypes.func,
  DeleteCompanyRequest: React.PropTypes.func,
  companies: React.PropTypes.array,
};

export default ListCompanies;
