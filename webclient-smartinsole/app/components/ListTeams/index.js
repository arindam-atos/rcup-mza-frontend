import React from 'react';
import Grid from 'components/Grid';
import { Row } from 'react-materialize';
import WhiteBox from 'components/WhiteBox';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

export class ListTeams extends React.PureComponent {//  eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      teams: this.props.teams,
    };
  }

  componentWillMount() {
    this.props.AllTeamsRequest(currentCompany.companyId);
    this.props.setPageTitle('Teams list');
  }

  render() {
    const columns = ['id', 'Name', 'Created', 'Last updated'];

    if (this.props.teams) {
      this.state.teams = this.props.teams;
    }

    return (
      <Row>
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Grid
              key={'list-teams'}
              columns={columns}
              datas={this.state.teams}
              actions
              addUrl="/add-team"
              editUrl="/edit-team"
              deleteRequest={this.props.DeleteTeamRequest}
            >
            </Grid>,
          ]}
        />
      </Row>
    );
  }
}

ListTeams.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllTeamsRequest: React.PropTypes.func,
  DeleteTeamRequest: React.PropTypes.func,
  teams: React.PropTypes.array,
};

export default ListTeams;
