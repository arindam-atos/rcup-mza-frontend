import styled from 'styled-components';

export const Wrapper = styled.div`
  .collection-item {
    height: 80px;
  }

  .collection {
    overflow: visible;
  }

  .collection h4 {
    font-size: 1.28rem;
  }

  .label {
    line-height: 60px;
    font-size: 20px;
  }

`;
