import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import { Button, Icon, Collection, CollectionItem } from 'react-materialize';
import Row from 'components/Row';
import Col from 'components/Col';
import { Wrapper } from './styles';

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class AddRoom extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    name: '',
    listHardnesses: [],
    hardnesses: {},
    hardnessesOptions: [],
  };

  componentWillMount = () => {
    const hardnessesOptions = [
      <option key={0} value={0}>0</option>,
      <option key={1} value={1}>1</option>,
      <option key={2} value={2}>2</option>,
      <option key={3} value={3}>3</option>,
      <option key={4} value={4}>4</option>,
      <option key={5} value={5}>5</option>,
    ];

    this.props.setPageTitle('Add room');
    this.props.GetCompanyRequest(currentCompany.companyId);
    this.setState({ hardnessesOptions });
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.company !== nextProps.company) {
      const hardnesses = {};
      const listHardnesses =
      nextProps.company.hardnesses.split(';');

      listHardnesses.forEach((hardness) => {
        hardnesses[hardness] = 0;
      });

      this.setState({ listHardnesses, hardnesses });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  setSelect = (key, value) => {
    this.setState((prevState) => ({
      hardnesses: {
        ...prevState.hardnesses,
        [key]: parseInt(value, 10),
      },
    }));
  }

  createInput = () =>
    <Row key="hardnesses-input" id="hardness-input">
      <Collection header="List of hardnesses" key="inputs-collection">
        {
        Object.keys(this.state.hardnesses).map((el, i) =>
          <CollectionItem key={`collection-item-${i}`}>
            <Col s={4} m={4} l={5}>
              <span className="label">{el}</span>
            </Col>
            <Col s={6} m={4} l={4}>
              <Select
                label="Select hardness value"
                key={el}
                name={el}
                defaultValue={0}
                options={this.state.hardnessesOptions}
                cb={this.setSelect}
              />
            </Col>
          </CollectionItem>
          )
        }
      </Collection>
    </Row>


  handleSubmit = (e) => {
    e.preventDefault();
    const room = this.state;

    this.props.AddRoomRequest(currentCompany.companyId, room);
  }

  render() {
    return (
      <Wrapper>
        <Form
          node={[
            <Row
              key="form-inputs"
            >
              <Input
                key="name"
                label="Name"
                name="name"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
            </Row>,
            this.createInput(),
            <Row
              key="form-submit"
            >
              <Button
                key="submit"
                id="submit"
                waves="light"
                onClick={this.handleSubmit}
                className="blue"
              >
                Add
                <Icon left>create</Icon>
              </Button>
            </Row>,
          ]}
        />
      </Wrapper>
    );
  }
}

AddRoom.propTypes = {
  setPageTitle: React.PropTypes.func,
  AddRoomRequest: React.PropTypes.func.isRequired,
  company: React.PropTypes.object,
  GetCompanyRequest: React.PropTypes.func,
};

export default AddRoom;
