import styled from 'styled-components';

const H1 = styled.h1`
  font-size: 2.5em;
  margin-top: 0;
  margin-bottom: 0.5em;
  margin-left: 10px;
  @media (max-width: 992px) {
    margin-top: 10px;
  }
`;

export default H1;
