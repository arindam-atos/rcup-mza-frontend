import React from 'react';
import { Icon } from 'react-materialize';
import { Input as MInput } from './styles';

class Input extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  handleChange = (event) => {
    if (this.props.type === 'file') {
      this.props.cb(this.props.name, event.target.files[0]);
    } else {
      this.props.cb(this.props.name, event.target.value);
    }
  }

  render() {
    const options = {
      options: this.props.options || '',
    };

    let content = '';

    if (this.props.icon) {
      content = (<MInput
        placeholder={this.props.placeholder}
        label={this.props.label}
        type={this.props.type || ''}
        className={this.props.className}
        labelClassName={this.props.labelClassName}
        s={this.props.s || 12}
        m={this.props.m || 12}
        l={this.props.l || 12}
        onChange={this.handleChange}
        defaultValue={this.props.value}
        key={this.props.value}
        disabled={this.props.disabled}
        checked={this.props.checked}
        maxLength={this.props.maxLength}
        {...options.options !== '' ? options : ''}
      >
        <Icon>{this.props.icon}</Icon>
      </MInput>);
    } else {
      content = (<MInput
        placeholder={this.props.placeholder}
        label={this.props.label}
        type={this.props.type || ''}
        className={this.props.className}
        labelClassName={this.props.labelClassName}
        s={this.props.s || 12}
        m={this.props.m || 12}
        l={this.props.l || 12}
        onChange={this.handleChange}
        defaultValue={this.props.value}
        key={this.props.value}
        disabled={this.props.disabled}
        checked={this.props.checked}
        maxLength={this.props.maxLength}
        {...options.options !== '' ? options : ''}
      />);
    }

    return (
      <div>
        { content }
      </div>
    );
  }
}

Input.propTypes = {
  placeholder: React.PropTypes.string,
  label: React.PropTypes.string,
  name: React.PropTypes.string.isRequired,
  type: React.PropTypes.string,
  cb: React.PropTypes.func,
  value: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
    React.PropTypes.object,
  ]),
  className: React.PropTypes.string,
  labelClassName: React.PropTypes.string,
  s: React.PropTypes.number,
  m: React.PropTypes.number,
  l: React.PropTypes.number,
  options: React.PropTypes.object,
  disabled: React.PropTypes.bool,
  checked: React.PropTypes.bool,
  maxLength: React.PropTypes.number,
  icon: React.PropTypes.string,
};

export default Input;
