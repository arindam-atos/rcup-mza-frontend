import styled from 'styled-components';
import { Row as MRow } from 'react-materialize';

export const Row = styled(MRow)`
  max-width: 60%;
  background: #FFF;
  border-radius: 10px;
  padding-bottom: 20px;
  padding-left: 5px;
  padding-right: 5px;
  margin-top: 30px;

  @media (max-width: 1450px) {
    max-width: 80%;
  }

  & button#submit {
    display: block;
    margin: 0 auto;
  }

  &.stayActive input ~ label {
    -ms-transform: translateY(-140%);
    -webkit-transform: translateY(-140%)
    transform: translateY(-140%);
  }
`;
