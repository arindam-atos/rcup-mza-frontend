import React from 'react';
import { Row } from './styles';

export default class Form extends React.Component {// eslint-disable-line react/prefer-stateless-function
  render() {
    const { node } = this.props;

    return (
      <Row
        className={this.props.className}
      >
        <form>
          {node}
        </form>
      </Row>
    );
  }

}

Form.propTypes = {
  node: React.PropTypes.node,
  className: React.PropTypes.string,
};
