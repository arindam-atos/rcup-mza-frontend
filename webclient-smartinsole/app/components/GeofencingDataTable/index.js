import React from "react";
import { Table } from "react-materialize";
import WhiteBox from "components/WhiteBox";
import Row from "components/Row";
import moment from "moment/moment";

export default class GeofencingDataTable extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  state = {
    samplesBody: []
  };

  componentWillReceiveProps = nextProps => {
    if (this.props.samples !== nextProps.samples) {
      const samplesBody = [];
      nextProps.samples.forEach(sample => {
        this.props.gateways.forEach(gtw => {
          if (sample.DeviceID === gtw.name) {
            samplesBody.push(
              <tr
                key={`sample-data-${sample.timestamp}`}
                id={`sample-data-${moment(sample.timestamp)
                  .subtract(1, "hours")
                  .format("HH:mm:ss")}`}
              >
                <td>{gtw.room}</td>
                <td>{gtw.name}</td>
                <td>{sample.steps}</td>
                <td>
                  {moment(sample.timestamp)
                    .subtract(1, "hours")
                    .format("HH:mm:ss")}
                </td>
              </tr>
            );
          }
        });
      });
      this.setState({ samplesBody });
    }

    if (this.props.gtw !== nextProps.gtw && nextProps.gtw) {
      const tr = document.getElementById(
        `sample-data-${nextProps.gtw.timestamp}`
      );
      this.interval = setInterval(() => {
        tr.style.background = "#FFF";
        tr.style.color = "#000";
      }, nextProps.interval);
      tr.style.background = "#27364d";
      tr.style.color = "#FFF";
    }
  };

  render() {
    let grid = <div />;

    if (this.state.samplesBody.length > 0) {
      grid = (
        <Table responsive centered>
          <thead>
            <tr>
              <th>Salle</th>
              <th>Scanner</th>
              <th>Nombre de pas</th>
              <th>Timestamp</th>
            </tr>
          </thead>
          <tbody>{this.state.samplesBody}</tbody>
        </Table>
      );
    } else {
      grid = <div style={{ textAlign: "center" }}>Pas de données</div>;
    }

    return (
      <WhiteBox
        s={12}
        m={12}
        l={4}
        node={[
          <Row key={"data"}>
            <div style={{ height: "165px", overflowY: "auto" }}>{grid}</div>
          </Row>
        ]}
      />
    );
  }
}

GeofencingDataTable.propTypes = {
  samples: React.PropTypes.array,
  gtw: React.PropTypes.object,
  gateways: React.PropTypes.array
};
