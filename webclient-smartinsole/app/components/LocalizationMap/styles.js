import styled from 'styled-components';

export const MarkersDisplayButton = styled.div`
  textAlign: 'right',
  position: 'absolute',
  right: '12px',
  marginRight: '10px',
  padding: '10px',
  background: '#FFF',
  zIndex: '1000',
  border: '1px solid black',
  ,
`;

export const Wrapper = styled.div`
  .leaflet-container {
    height: 500px;
  }

  @media (max-width: 600px) {
    .leaflet-container {
      height: 420px;
    }
  }

  .blinking {
    animation: fade 0.5s infinite alternate;
  }
`;
