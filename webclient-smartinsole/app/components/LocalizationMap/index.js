import React from 'react';
import { Map, TileLayer } from 'react-leaflet';
import WhiteBox from 'components/WhiteBox';
import Marker from 'components/Marker';
import { Wrapper } from './styles';

export default class LocalizationMap extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    heatmap: true,
  }
  handleChange = (key, value) => {
    let bool;
    if (value === 'true' || value === true) {
      bool = true;
    } else {
      bool = false;
    }
    this.setState({ heatmap: bool });
  }

  render() {
    return (
      <WhiteBox
        s={12}
        m={12}
        l={8}
        style={{
          position: 'relative',
        }}
        node={[
          <Wrapper
            key={'map'}
          >
            <Map
              id={'map'}
              center={[48.86799126699168, 2.3465922474861145]}
              zoom={9}
              maxZoom={15}
              minZoom={5}
            >

              <TileLayer
                key={'tilelayer'}
                maxZoom={21}
                url={'https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png'}
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />

              <Marker
                marker={this.props.gtw}
              />
            </Map>
          </Wrapper>,
        ]}
      />
    );
  }
}

LocalizationMap.propTypes = {
  gtw: React.PropTypes.object,
};
