import React from "react";
import styled from "styled-components";
import moment from "moment/moment";
import Row from "components/Row";
import GeofencingMap from "components/GeofencingMap";
import GeofencingControls from "components/GeofencingControls";
import GeofencingDataCharts from "components/GeofencingDataCharts";
import GeofencingSamplesDL from "components/GeofencingSamplesDL";

const currentCompany = JSON.parse(localStorage.getItem("currentCompany"));

const Wrapper = styled.div``;

export default class GeofencingBoard extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  state = {
    company: null,
    gateways: [],
    samples: [],
    gtw: null,
    index: 0,
    from: "",
    to: "",
    day: moment().format("YYYY-MM-DD"),
    userId: "",
    userAdd: "",
    interval: 1000,
    speed: 1,
    maxSpeed: 32,
    users: [],
    secondInterval: 0,
    rooms: []
  };

  componentWillMount() {
    this.props.setPageTitle("Geofencing");
    this.props.AllGatewaysRequest(currentCompany.companyId);
    this.props.AllUsersRequest(currentCompany.companyId);
    this.props.GetCompanyRequest(currentCompany.companyId);
  }

  componentWillReceiveProps = nextProps => {
    if (this.props.company !== nextProps.company) {
      this.setState({ company: nextProps.company });
    }
    if (nextProps.gateways && this.props.gateways !== nextProps.gateways) {
      this.setState({ gateways: nextProps.gateways });
    }
    if (this.props.userSamples !== nextProps.userSamples) {
      this.setState({ samples: nextProps.userSamples });
    }
    if (this.props.users !== nextProps.users) {
      const users = [];

      nextProps.users.forEach(user => {
        if (user.role === "worker" || user.role === "manager") users.push(user);
      });
      this.setState({ userAdd: users[0].macAddress });
      this.setState({ userId: users[0].id });
      this.setState({ users });
    }
  };

  componentWillUnmount = () => {
    clearInterval(this.interval);
  };

  setField = (key, value) => {
    switch (key) {
      case "day":
        this.setState({ [key]: moment(value).format("YYYY-MM-DD") });
        break;
      case "userId":
        this.props.users.forEach(user => {
          if (value === user.id.toString()) {
            this.setState({ userAdd: user.macAddress });
          }
        });
        break;
      default:
        this.setState({ [key]: value });
    }
  };

  handleSubmit = payload => {
    clearInterval(this.interval);

    const fromDate = moment(payload.from, "HH:mm:ss");
    const toDate = moment(payload.to, "HH:mm:ss");

    const duration = Number(moment.duration(toDate.diff(fromDate)).asSeconds());

    this.setState({ secondInterval: duration });

    if (fromDate.isValid() && toDate.isValid()) {
      this.props.getSamplesByHours(
        currentCompany.companyId,
        this.state.userAdd,
        payload.day,
        fromDate.format("HH:mm:ss"),
        toDate.format("HH:mm:ss")
      );
      this.interval = setInterval(this.updateMarker, this.state.interval);

      this.setState({ from: payload.from });
      this.setState({ to: payload.to });
      this.setState({ day: payload.day });

    }

    this.setState({ index: 0 });
    this.setState({ samplesBody: [] });
  };

  updateMarker = () => {
    if (this.state.samples.length > this.state.index) {
      this.state.gateways.forEach(gtw => {
        if (this.state.samples[this.state.index].deviceID === gtw.name) {
          const data = {
            name: gtw.name,
            room: gtw.room,
            longitude: gtw.longitude,
            latitude: gtw.latitude,
            steps: this.props.userSamples[this.state.index].steps,
            timestamp: moment(
              this.props.userSamples[this.state.index].timestamp
            ).format("HH:mm:ss")
          };
          this.setState({ gtw: data });
        }
      });
      this.setState({ index: this.state.index + 1 });
    } else {
      clearInterval(this.interval);
      this.setState({ gtw: null });
    }
  };

  speedup = () => {
    if (this.state.speed < this.state.maxSpeed) {
      const interval = this.state.interval / 2;
      let speed = this.state.speed;
      if (speed >= 1) {
        speed *= 2;
      } else if (speed === -2) {
        speed /= -2;
      } else {
        speed /= 2;
      }
      this.setState({ speed });
      this.setState({ interval });
      clearInterval(this.interval);
      this.interval = setInterval(this.updateMarker, interval);
    }
  };

  speeddown = () => {
    if (this.state.speed > -1 * this.state.maxSpeed) {
      const interval = this.state.interval * 2;
      let speed = this.state.speed;
      if (speed > 1) {
        speed /= 2;
      } else if (speed < 1) {
        speed *= 2;
      } else {
        speed *= -2;
      }
      this.setState({ interval });
      this.setState({ speed });
      clearInterval(this.interval);
      this.interval = setInterval(this.updateMarker, interval);
    }
  };

  render() {
    return (
      <Wrapper>
        <Row className={"flex"}>
          <GeofencingMap
            map={this.state.map}
            company={this.state.company}
            gateways={this.state.gateways}
            gtw={this.state.gtw}
            secondInterval={this.state.secondInterval}
            samples={this.state.samples}
          />

          <GeofencingControls
            userId={this.state.userId}
            users={this.state.users}
            from={this.state.from}
            to={this.state.to}
            speed={this.state.speed}
            speedMax={this.state.speedMax}
            speedup={this.speedup}
            speeddown={this.speeddown}
            handleSubmit={this.handleSubmit}
            setField={this.setField}
          />

          <GeofencingSamplesDL
            samples={this.state.samples}
            from={this.state.from}
            to={this.state.to}
            day={this.state.day}
            macaddr={this.state.userAdd}
          />
        </Row>
        <Row>
          <GeofencingDataCharts
            samples={this.state.samples}
            from={this.state.from}
            to={this.state.to}
            gateways={this.state.gateways}
          />
        </Row>
      </Wrapper>
    );
  }
}

GeofencingBoard.propTypes = {
  setPageTitle: React.PropTypes.func,
  company: React.PropTypes.object,
  gateways: React.PropTypes.array,
  AllGatewaysRequest: React.PropTypes.func,
  userSamples: React.PropTypes.array,
  users: React.PropTypes.array,
  AllUsersRequest: React.PropTypes.func,
  getSamplesByHours: React.PropTypes.func,
  GetCompanyRequest: React.PropTypes.func
};
