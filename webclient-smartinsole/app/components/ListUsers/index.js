/*
 *  @Project    WebClient WorkerSole
 *
 *  @File       ListUsers.js
 *
 *  @summary    View with list of users from same company
 *
 *  @author     PHONG Stéphane
 *
 */

import React from 'react';
import Grid from 'components/Grid';
import { Row } from 'react-materialize';
import WhiteBox from 'components/WhiteBox';

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));
const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

class ListUsers extends React.Component {//  eslint-disable-line react/prefer-stateless-function

  state = {
    users: [],
  };

  componentWillMount() {
    this.props.AllUsersRequest(currentCompany.companyId);
    this.props.setPageTitle('Users list');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.users) {
      const users = [];

      // Only add admin user in the list if the loggedUser is also admin
      nextProps.users.forEach((user) => {
        if (user.role !== 'admin') {
          users.push(user);
        } else if (loggedUser.role === 'admin' && user.role === 'admin') {
          users.push(user);
        }
      });

      this.setState({
        users,
      });
    }
  }

  render() {
    const columns = ['id', 'Email', 'Mac Address', 'Role', 'Firstname', 'Lastname', 'Created', 'Last updated'];

    let actions = false;

    if (loggedUser.role === 'admin' || loggedUser.role === 'director') {
      actions = true;
    }

    return (
      <Row>
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Grid
              key={'list-users'}
              columns={columns}
              datas={this.state.users}
              addUrl="/add-user"
              editUrl="/edit-user"
              deleteRequest={this.props.DeleteUserRequest}
              actions={actions}
            >
            </Grid>,
          ]}
        />
      </Row>
    );
  }
}

ListUsers.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllUsersRequest: React.PropTypes.func,
  DeleteUserRequest: React.PropTypes.func,
};

export default ListUsers;
