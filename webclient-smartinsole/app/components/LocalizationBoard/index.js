import React from "react";
import styled from "styled-components";
import moment from "moment/moment";
import Row from "components/Row";
import LocalizationMap from "components/LocalizationMap";
import GeofencingControls from "components/GeofencingControls";

const currentCompany = JSON.parse(localStorage.getItem("currentCompany"));

const Wrapper = styled.div``;

export default class GeofencingBoard extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  state = {
    gateways: [],
    samples: [],
    gtw: null,
    index: 0,
    from: "",
    to: "",
    day: moment().format("YYYY-MM-DD"),
    userId: "",
    userAdd: "",
    interval: 1000,
    speed: 1,
    maxSpeed: 32,
    users: [],
    secondInterval: 0
  };

  componentWillMount() {
    this.props.setPageTitle("PTI");
    this.props.AllUsersRequest(currentCompany.companyId);
  }

  componentWillReceiveProps = nextProps => {
    if (this.props.userSamples !== nextProps.userSamples) {
      this.setState({ samples: nextProps.userSamples });
    }
    if (this.props.users !== nextProps.users) {
      const users = [];

      nextProps.users.forEach(user => {
        if (user.role === "worker" || user.role === "manager") users.push(user);
      });
      this.setState({ userAdd: users[0].macAddress });
      this.setState({ userId: users[0].id });
      this.setState({ users });
    }
  };

  setField = (key, value) => {
    switch (key) {
      case "day":
        this.setState({ [key]: moment(value).format("YYYY-MM-DD") });
        break;
      case "userId":
        this.props.users.forEach(user => {
          if (value === user.id.toString()) {
            this.setState({ userAdd: user.macAddress });
          }
        });
        break;
      default:
        this.setState({ [key]: value });
    }
  };

  handleSubmit = payload => {
    clearInterval(this.interval);

    const fromDate = moment(payload.from, "HH:mm:ss");
    const toDate = moment(payload.to, "HH:mm:ss");

    const duration = Number(moment.duration(toDate.diff(fromDate)).asSeconds());

    this.setState({ secondInterval: duration });

    if (fromDate.isValid() && toDate.isValid()) {
      this.props.getSamplesByHours(
        currentCompany.companyId,
        this.state.userAdd,
        payload.day,
        fromDate.format("HH:mm:ss"),
        toDate.format("HH:mm:ss")
      );
      this.interval = setInterval(this.updateMarker, this.state.interval);

      this.setState({ from: payload.from });
      this.setState({ to: payload.to });
      this.setState({ day: payload.day });
    }

    this.setState({ index: 0 });
    this.setState({ samplesBody: [] });
  };

  updateMarker = () => {
    if (this.state.samples.length > this.state.index) {
      if (
        this.state.samples[this.state.index].deviceID.charAt(0) === "A" &&
        this.state.samples[this.state.index].latitude !== undefined &&
        this.state.samples[this.state.index].longitude
      ) {
        const data = {
          name: this.state.samples[this.state.index].macAddress,
          steps: this.state.samples[this.state.index].steps,
          longitude: this.state.samples[this.state.index].latitude,
          latitude: this.state.samples[this.state.index].longitude,
          timestamp: moment(
            this.state.samples[this.state.index].timestamp
          ).format("HH:mm:ss")
        };
        this.setState({ gtw: data });
      }
      this.setState({ index: this.state.index + 1 });
    } else {
      clearInterval(this.interval);
      this.setState({ gtw: null });
    }
  };

  speedup = () => {
    if (this.state.speed < this.state.maxSpeed) {
      const interval = this.state.interval / 2;
      let speed = this.state.speed;
      if (speed >= 1) {
        speed *= 2;
      } else if (speed === -2) {
        speed /= -2;
      } else {
        speed /= 2;
      }
      this.setState({ speed });
      this.setState({ interval });
      clearInterval(this.interval);
      this.interval = setInterval(this.updateMarker, interval);
    }
  };

  speeddown = () => {
    if (this.state.speed > -1 * this.state.maxSpeed) {
      const interval = this.state.interval * 2;
      let speed = this.state.speed;
      if (speed > 1) {
        speed /= 2;
      } else if (speed < 1) {
        speed *= 2;
      } else {
        speed *= -2;
      }
      this.setState({ interval });
      this.setState({ speed });
      clearInterval(this.interval);
      this.interval = setInterval(this.updateMarker, interval);
    }
  };

  render() {
    return (
      <Wrapper>
        <Row className={"flex"}>
          <LocalizationMap
            gtw={this.state.gtw}
            secondInterval={this.state.secondInterval}
            samples={this.state.samples}
          />

          <GeofencingControls
            userId={this.state.userId}
            users={this.state.users}
            from={this.state.from}
            to={this.state.to}
            speed={this.state.speed}
            speedMax={this.state.speedMax}
            speedup={this.speedup}
            speeddown={this.speeddown}
            handleSubmit={this.handleSubmit}
            setField={this.setField}
          />
        </Row>
      </Wrapper>
    );
  }
}

GeofencingBoard.propTypes = {
  setPageTitle: React.PropTypes.func,
  userSamples: React.PropTypes.array,
  users: React.PropTypes.array,
  AllUsersRequest: React.PropTypes.func,
  getSamplesByHours: React.PropTypes.func
};
