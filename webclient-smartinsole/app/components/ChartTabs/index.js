import React from 'react';
import styled from 'styled-components';
import WhiteBox from 'components/WhiteBox';
import ChartInfo from 'components/ChartInfo';
import PieChart from 'components/PieChart';
import StackedBarChart from 'components/StackedBarChart';
import MultipleBarChart from 'components/MultipleBarChart';
import { Button } from 'react-materialize';

const ButtonList = styled.div`
  @media (max-width: 600px) {
    margin-top: 20px;
  }
`;

export default class ChartTabs extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <div style={{ display: this.props.display }}>
        <WhiteBox
          s={12}
          m={12}
          l={5}
          node={[
            <ChartInfo
              label="Time spent (%)"
              date={this.props.date}
              key={`crossings-info-${this.props.type}`}
              data={this.props.crossingsData}
              node={(
                <PieChart
                  key={`crossing-chart-${this.props.type}`}
                  data={this.props.crossingsData}
                  colorsByType={this.props.colorsByType}
                />
              )}
            />,
          ]}
        />
        <WhiteBox
          s={12}
          m={12}
          l={7}
          node={[
            <ChartInfo
              label="Number of steps"
              key={`steps-info-${this.props.type}`}
              date={this.props.date}
              data={this.props.stepsData}
              node={(
                <div>
                  <ButtonList>
                    <Button
                      waves={'light'} onClick={() => this.props.setTimeInterval(5, this.props.type)}
                      style={{ textTransform: 'lowercase' }}
                    >{'5m'}</Button>
                    <Button
                      style={{ textTransform: 'lowercase' }}
                      waves={'light'} onClick={() => this.props.setTimeInterval(30, this.props.type)}
                    >
                      {'30m'}
                    </Button>
                    <Button
                      waves={'light'}
                      style={{ textTransform: 'lowercase' }}
                      onClick={() => this.props.setTimeInterval(1, this.props.type)}
                    >
                      {'1h'}
                    </Button>
                    <Button
                      waves={'light'}
                      onClick={() => this.props.setTimeInterval(2, this.props.type)}
                      style={{ textTransform: 'lowercase' }}
                    >
                      {'2h'}
                    </Button>

                  </ButtonList>
                  <StackedBarChart
                    key={`steps-chart-${this.props.type}`}
                    data={this.props.stepsData}
                    colorsByType={this.props.colorsByType}
                  />
                </div>
              )}
            />,
          ]}
        />
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <ChartInfo
              label="Time Spent (%) VS Number of steps (%)"
              date={this.props.date}
              key={`crossings-steps-info-${this.props.type}`}
              node={(
                <MultipleBarChart
                  key={`crossings-steps-chart-${this.props.type}`}
                  data={this.props.crossingsAndStepsData}
                />
              )}
            />,
          ]}
        />
      </div>
    );
  }
}

ChartTabs.propTypes = {
  type: React.PropTypes.string,
  date: React.PropTypes.string,
  crossingsData: React.PropTypes.array,
  stepsData: React.PropTypes.array,
  crossingsAndStepsData: React.PropTypes.array,
  setTimeInterval: React.PropTypes.func,
  display: React.PropTypes.string,
  colorsByType: React.PropTypes.object,
};
