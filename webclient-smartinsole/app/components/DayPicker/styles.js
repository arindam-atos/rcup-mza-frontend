import styled from 'styled-components';

import { DayPicker as MDayPicker } from 'react-day-picker';

export const Wrapper = styled.div`
  text-align: center;

  .row {
    margin-top: 20px;
  }

  .Range .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
   background-color: #f0f8ff !important;
   color: #4a90e2;
  }

  .Range .DayPicker-Month {
  }

  .Range .DayPicker-NavButton {
    top: 0;
    right: 46px;
  }

  .Range .DayPicker-Day {
    border-radius: 0 !important;
    background: #FFF;
  }
`;

export const DayPicker = styled(MDayPicker)`
`;
