import React from 'react';
import 'react-day-picker/lib/style.css';
import { DateUtils } from 'react-day-picker';
import { Row, Col, Button } from 'react-materialize';
import { Wrapper, DayPicker as MDayPicker } from './styles';


export default class DayPicker extends React.Component {
  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.handleDayMouseEnter = this.handleDayMouseEnter.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.state = this.resetState();
  }
  resetState() {
    return {
      from: null,
      to: null,
      enteredTo: null, // Keep track of the last day for mouseEnter.
    };
  }
  isSelectingFirstDay(from, to, day) {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }
  handleDayClick(day) {
    const { from, to } = this.state;
    if (from && to && day >= from && day <= to) {
      this.handleResetClick();
      return;
    }
    if (this.isSelectingFirstDay(from, to, day)) {
      this.setState({
        from: day,
        to: null,
        enteredTo: null,
      });
    } else {
      this.setState({
        to: day,
        enteredTo: day,
      });

      this.props.setDate(from, day);
    }
  }
  handleDayMouseEnter(day) {
    const { from, to } = this.state;
    if (!this.isSelectingFirstDay(from, to, day)) {
      this.setState({
        enteredTo: day,
      });
    }
  }
  handleResetClick() {
    this.setState(this.resetState());
  }
  render() {
    const { from, to, enteredTo } = this.state;
    const modifiers = { start: from, end: enteredTo };
    const disabledDays = { before: this.state.from };
    const selectedDays = [from, { from, to: enteredTo }];
    return (
      <Wrapper>
        <MDayPicker
          className="Range"
          numberOfMonths={2}
          fromMonth={from}
          selectedDays={selectedDays}
          disabledDays={disabledDays}
          modifiers={modifiers}
          onDayClick={this.handleDayClick}
          onDayMouseEnter={this.handleDayMouseEnter}
        />
        <Row>
          {!from && !to && 'Please select the first day.'}
          {from && !to && 'Please select the last day.'}
          {from &&
            to && (
              <Col
                s={6}
                m={6}
                l={6}
                style={{ lineHeight: '37px' }}
              >
              Selected from {from.toLocaleDateString()} to {to.toLocaleDateString()}
              </Col>
            )}
          {from &&
            to && (
              <Col
                s={6}
                m={6}
                l={6}
              >
                <Button
                  className="link"
                  style={{ background: '#4a90e2' }} onClick={this.handleResetClick}
                >
                  Reset
                </Button>
              </Col>
            )}
        </Row>
      </Wrapper>
    );
  }
}

DayPicker.propTypes = {
  setDate: React.PropTypes.func,
};
