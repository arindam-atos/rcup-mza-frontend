/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {browserHistory} from 'react-router';
import BgImg from 'public/background.jpg';
import Logo from 'public/logo/logo.png';

import {Wrapper, SideNav, SideNavItem, NavBar} from './styles';

const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

export default class SideBar extends React.Component { // eslint-disable-line react/prefer-stateless-function

    state = {
        company: null,
    }

    componentWillMount = () => {
        this.props.GetCompanyRequest(currentCompany.companyId);
    }

    componentWillReceiveProps = (nextProps) => {
        if (this.props.company !== nextProps.company) {
            // const arrayBufferView = new Uint8Array(nextProps.company.file.data);
            // const blob = new Blob([arrayBufferView], { type: 'image/png' });
            //
            // const fileReaderInstance = new FileReader();
            // fileReaderInstance.readAsDataURL(blob);
            // fileReaderInstance.onload = () => {
            //   const base64data = fileReaderInstance.result;
            //   this.setState({ file: base64data });
            // };
            this.setState({company: nextProps.company});
        }
    }

    render() {
        return (
            <Wrapper>
                <NavBar className="hide-on-large-only">
                    <div>{this.props.pageTitle}</div>
                </NavBar>
                <SideNav
                    trigger={
                        <div className="hide-on-large-only button-collapse">
                            <i className="material-icons">menu</i>
                        </div>
                    }
                    options={{
                        menuWidth: 300,
                    }}
                    className={'side-nav fixed show-on-large-only sidebar-background'}
                    style={{backgroundImage: `url('${BgImg}')`}}
                >
                    {this.state.company ?

                        <SideNavItem
                            className="company-img"
                            onClick={(e) => {
                                e.preventDefault();
                                browserHistory.push('/');
                            }}
                        >
                            <img alt="logo" src={Logo}/>
                        </SideNavItem> : <SideNavItem/>
                    }

                    <SideNavItem
                        userView
                        user={{
                            name: `${loggedUser.firstname.toUpperCase()}${loggedUser.firstname.slice(1)} ${loggedUser.lastname.toUpperCase()}${loggedUser.lastname.slice(1)}`,
                            email: loggedUser.email,
                        }}
                    />

                    <SideNavItem
                        href="/"
                        className="item"
                        icon="apps"
                        onClick={(e) => {
                            e.preventDefault();
                            browserHistory.push('/');
                        }}
                    >
                        Dashboard
                    </SideNavItem>

                    <SideNavItem
                        className="item"
                        icon="gps_fixed"
                        href="/geofencing"
                        onClick={(e) => {
                            e.preventDefault();
                            browserHistory.push('/geofencing');
                        }}
                    >
                        Geofencing
                    </SideNavItem>

                    <SideNavItem
                        className="item disabled"
                        icon="business"
                    >
                        Hardness Index
                    </SideNavItem>


                    <SideNavItem
                        className="item disabled"
                        icon="gps_fixed"
                    >
                        Isolated Worker Protection
                    </SideNavItem>

                    <SideNavItem
                        className="item disabled"
                        icon="timeline"
                    >
                        Carried Weight Monitoring
                    </SideNavItem>

                    {
                        loggedUser.role === 'admin' ?
                            <SideNavItem
                                className="item disabled"
                                icon="power"
                            >
                                Insoles Management
                            </SideNavItem> : ''
                    }

                    <div className="hide-on-large-only">
                        <SideNavItem className="divider"/>
                        {
                            loggedUser.role === 'admin' ?
                                <SideNavItem
                                    className="item"
                                    icon="business"
                                    href="/companies"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        browserHistory.push('/companies');
                                    }}
                                >
                                    Companies
                                </SideNavItem> : ''
                        }
                        {
                            loggedUser.role === 'admin' ?
                                <SideNavItem
                                    className="item"
                                    icon="rss_feed"
                                    href="/gateways"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        browserHistory.push('/gateways');
                                    }}
                                >
                                    Gateways
                                </SideNavItem> : ''
                        }
                        {
                            loggedUser.role === 'admin' ?
                                <SideNavItem
                                    className="item"
                                    icon="contacts"
                                    href="/teams"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        browserHistory.push('/teams');
                                    }}
                                >
                                    Teams
                                </SideNavItem> : ''
                        }
                        {
                            loggedUser.role === 'admin' ?
                                <SideNavItem
                                    className="item"
                                    icon="accessibility"
                                    href="/users"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        browserHistory.push('/users');
                                    }}
                                >
                                    Users
                                </SideNavItem> : ''
                        }
                        <SideNavItem className="divider"/>
                        <SideNavItem
                            className="item"
                            icon="power_settings_new"
                            href="/logout"
                        >
                            Disconnect
                        </SideNavItem>
                    </div>
                </SideNav>
            </Wrapper>
        );
    }
}

SideBar.propTypes = {
    pageTitle: React.PropTypes.string,
    company: React.PropTypes.object,
    GetCompanyRequest: React.PropTypes.func,
};
