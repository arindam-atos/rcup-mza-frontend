import styled from 'styled-components';
import { SideNav as MSideNav, SideNavItem as MSideNavItem } from 'react-materialize';

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  display: block;
  z-index: 9999;
  color: #fff;
  font-weight: 200;
  background-size: cover;
  background-position: 50%;

  .button-collapse {
    position: fixed;
    background: #FFF;
    color: #777;
    padding-left: 20px;
    i {
      line-height: 35px;
    }
  }

  .sidebar-background {
    position: absolute;
    z-index: 99;
    height: 100%;
    width: 100%;
    display: block;
    top: 0;
    left: 0;
    background-size: cover;
    background-position: 50%;
  }

  @media (max-width: 992px) {
    color: #777;
    .row {
      position: relative;
      margin-bottom: 0;
      width: 1000px;
      background: #FFF;

      .col i, .col span {
        line-height: 35px;
      }
    }

  }
`;

export const NavBar = styled.div`
  position: fixed;
  width: 100%;
  background: #FFF;
  div {
    z-index: 0;
    line-height: 35px;
    width: 100%;
    height: 36px;
    text-align: right;
    padding-right: 20px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
    box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
  }
`;

export const Ul = styled.ul`
  margin-top: -30px;
  margin-bottom: -30px;
`;

export const SideNav = styled(MSideNav)`
  width: 300px !important;
  overflow-y: auto;
  overflow-x: hidden;
  z-index: 2;

  & .company-img {
    width: 300px;
    height: 100px;
    margin: 2rem 0;

    & a {
      display : flex;
      justify-content: center;
      pointer: none;
      margin: 15px 10px 0px 10px;
      height: 100%;
      &:hover {
        opacity: 1;
      }
    }


    & img {
      max-width: 100%;
      max-height: 100%;
    }
  }
`;

export const SideNavItem = styled(MSideNavItem)`
  font-family: Open-Sans !important;
  padding: 10px;

  & > .userView {
    border-bottom: 1px solid #d0d0d0;
    padding-top: 0px;
    padding-left: 15px;
  }
  & > .userView > .background > img {
    position: absolute;
    right: 0;
  }

  &.item > a {
    color: #FFF ;
    padding: 0 15px;
  }

  &.item > a:hover {
    opacity: 0.5;
    background: #FFF;
    color: #000 !important;

    i {
      color: #000 !important;
    }

    &.divider {
      height: 0;
      border-bottom: 1px solid #FFF;
      margin: 0 10px;
    }
  }

  &.divider {
    background: transparent;
    border-bottom: 1px solid #d0d0d0;
    margin: 10px !important;
  }

  &.disabled {
    color: grey !important;
    a:hover {
      cursor: default;
    }
  }

  i {
    margin: 0 20px 0px 10px !important
    color: #FFF !important;
  }

`;
