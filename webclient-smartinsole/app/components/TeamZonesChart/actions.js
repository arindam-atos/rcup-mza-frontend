import moment from "moment/moment";

// Filter list of team's users samples by creating an array of object indexed by time interval
// format of the result:
// samplesByTime = [{
//    key: "2018-05-18 12:00:00",
//    data: [{ list of samples
//             in interval 12:00 - 12:30 }]
// }, {
//    key: "2018-05-18 12:30:00",
//    data: [{ list of samples
//             in interval 12:30 - 13:00 }]
// }, ... ]
export function indexSamplesByTime(listUsersSamples, day, from, to) {
  const samplesByTime = [];
  const fromTime = moment(`${day} ${from}`);
  const toTime = moment(`${day} ${to}`);

  while (toTime.diff(fromTime, "minute") >= 30) {
    const timeIndex = moment(fromTime).format("YYYY-MM-DDTHH:mm");
    samplesByTime.push({
      key: timeIndex,
      data: []
    });
    fromTime.add(30, "minute");
  }

  listUsersSamples.forEach(userSample => {
    userSample.forEach(sample => {
      const sampleDate = moment(sample.timestamp);
      for (let j = 0; j < samplesByTime.length; j += 1) {
        const currDate = moment(samplesByTime[j].key);
        if (samplesByTime[j + 1] !== undefined) {
          const nextDate = moment(samplesByTime[j + 1].key);

          if (sampleDate.isAfter(currDate) && sampleDate.isBefore(nextDate)) {
            samplesByTime[j].data.push(sample);
          }
        } else if (sampleDate.isAfter(currDate)) {
          samplesByTime[samplesByTime.length - 1].data.push(sample);
        }
      }
    });
  });
  return samplesByTime;
}

// This function is called for each index of filtered array of users samples indexed by time. It returns the time spent near by gateway
// format of the result:
// samplesByGtw = {
//  MOBILITY: {duration: -2899, steps: 170}
//  RB30002: {duration: 1795, steps: 0}
//  RB30007: {duration: 1553, steps: 0}
//  RB30008: {duration: 1178, steps: 0}
//  RB30010: {duration: 170, steps: 0}
// }
export function getSamplesByGtw(samples, time) {
  const samplesByGtw = {};
  let firstSample = samples[0];
  let prevSample = samples[0];
  const mobility = {
    duration: 0,
    steps: 0
  };

  // Calculate the mobility duration for the first sample of the list of samples indexed by time and then proceed to calcul the rest of duration of all samples
  const curTime = moment(time).format("HH:mm:ss");
  const curTimeDuration = moment.duration(curTime, "seconds").asSeconds();
  const firstSampleDate = moment(firstSample.timestamp).format("HH:mm:ss");
  const firstSampleDateDuration = moment
    .duration(firstSampleDate, "seconds")
    .asSeconds();
  mobility.duration += firstSampleDateDuration - curTimeDuration;

  samples.forEach((sample, index) => {
    if (!Object.prototype.hasOwnProperty.call(samplesByGtw, sample.deviceID)) {
      samplesByGtw[sample.deviceID] = {
        duration: 0,
        steps: 0
      };
    }

    if (prevSample.deviceID !== sample.deviceID) {
      const prevDate = moment(prevSample.timestamp).format("HH:mm:ss");
      const firstDate = moment(firstSample.timestamp).format("HH:mm:ss");
      const currDate = moment(sample.timestamp).format("HH:mm:ss");

      const prevDuration = moment.duration(prevDate, "seconds").asSeconds();
      const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
      const curDuration = moment.duration(currDate, "seconds").asSeconds();

      if (firstDuration <= prevDuration) {
        samplesByGtw[prevSample.DeviceID].duration +=
          prevDuration - firstDuration;
      }

      samplesByGtw[prevSample.deviceID].steps +=
        prevSample.steps - firstSample.steps;

      if (prevSample.macAddress === sample.macAddress) {
        mobility.duration += curDuration - prevDuration;
        mobility.steps += sample.steps - prevSample.steps;
      }
      firstSample = sample;
    }

    if (index === samples.length - 1) {
      const firstDate = moment(firstSample.timestamp).format("HH:mm:ss");
      const currDate = moment(sample.timestamp).format("HH:mm:ss");
      const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
      const curDuration = moment.duration(currDate, "seconds").asSeconds();

      samplesByGtw[sample.deviceID].duration += curDuration - firstDuration;

      samplesByGtw[prevSample.deviceID].steps +=
        sample.steps - firstSample.steps;
    }
    prevSample = sample;
  });
  samplesByGtw.MOBILITY = mobility;
  return samplesByGtw;
}

export const getSamplesByZone = (crossingsGtwData, gateways, rooms) => {
  const samplesByZone = {};
  samplesByZone.Mobility = crossingsGtwData.MOBILITY;

  rooms.forEach(room => {
    samplesByZone[room.name] = {
      duration: 0,
      steps: 0
    };
  });

  Object.keys(crossingsGtwData).forEach(data => {
    gateways.forEach(gateway => {
      if (data === gateway.name) {
        samplesByZone[gateway.room].duration += crossingsGtwData[data].duration;
        samplesByZone[gateway.room].steps += crossingsGtwData[data].steps;
      }
    });
  });
  return samplesByZone;
};

export const getSamplesByZoneTimeSpentPourcentage = samplesByZone => {
  let totalDuration = 0;
  const SamplesByZoneTimeSpentPourcentage = {};

  Object.keys(samplesByZone).forEach(zone => {
    totalDuration += samplesByZone[zone].duration;
    SamplesByZoneTimeSpentPourcentage[zone] = {
      duration: 0,
      steps: 0
    };
  });

  Object.keys(samplesByZone).forEach(zone => {
    SamplesByZoneTimeSpentPourcentage[zone].steps = samplesByZone[zone].steps;
    SamplesByZoneTimeSpentPourcentage[zone].duration = Math.round(
      (samplesByZone[zone].duration / totalDuration) * 100
    );
  });

  return SamplesByZoneTimeSpentPourcentage;
};
