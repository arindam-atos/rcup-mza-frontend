import React from 'react';
import 'hammerjs';
import vis from 'vis';
import moment from 'moment/moment';
import DateInput from 'components/DateInput';
import Select from 'components/Select';
import Row from 'components/Row';
import Col from 'components/Col';
import {Button, Col as ReactCol, Icon, Row as ReactRow} from 'react-materialize';
import {timeInterval} from './constants';
import {getSamplesByGtw, getSamplesByZone, getSamplesByZoneTimeSpentPourcentage, indexSamplesByTime} from './actions';
import {Wrapper} from './styles';

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

// Called when the Visualization API is loaded.
function drawVisualization(samplesByZone, startTime) {
    let yC = 0;
    const xLabels = {};
    const yLabels = {};
    const firstTime = moment(startTime);
    const data = new vis.DataSet();

    for (let index = 0; index < samplesByZone.length; index += 1) {
        /* eslint-disable no-loop-func */
        Object.keys(samplesByZone[index]).forEach((zone, i) => {
            data.add({
                id: `${zone}-${index}`,
                x: i * 20,
                y: yC,
                z: samplesByZone[index][zone].duration,
            });
            if (yC === 0) {
                xLabels[i * 20] = zone;
            }
        });
        yLabels[yC] = firstTime.format('HH:mm');
        firstTime.add(30, 'minute');
        yC += 10;
    }
    /* eslint-enable no-loop-func */

    // specify options
    const options = {
        width: '100',
        height: '500px',
        style: 'bar',
        xCenter: '55%',
        animationPreload: true,
        showPerspective: true,
        showGrid: false,
        showShadow: false,
        keepAspectRatio: true,
        verticalRatio: 0.5,
        zMax: 100,
        yBarWidth: 10,
        xBarWidth: 10,
        showLegend: true,
        xLabel: 'Zones',
        yLabel: 'Timestamp',
        zLabel: 'Time Spent',
        yValueLabel: (value) => value % 10 === 0 ? yLabels[value] : '',
        xValueLabel: (value) => xLabels[value],
        tooltip: (tip) =>
            `value: <b>${tip.z} %</b><br/>
      time: <b>${yLabels[tip.y]}</b>`,
        filterLabel: 'LABEL',
        legendLabel: '% Time spent',
    };

    const container = document.getElementById('mygraph');
    /* eslint-disable no-unused-vars */
    if (data.length > 0) {
        const graph3d = new vis.Graph3d(container, data, options);
        /* eslint-enable no-unused-vars */
    } else {
        $(container).children().remove();
    }
}

class TeamZonesChart extends React.Component {//  eslint-disable-line react/prefer-stateless-function

    state = {
        day: moment().format('YYYY-MM-DD'),
        from: {
            value: '09:00:00',
            index: 2,
        },
        to: {
            // value: '10:30:00',
            value: '11:00:00',
            index: 6,
        },
        interval: 30,
        gateways: [],
        samplesByZone: [],
        loopIndex: 0,
        timeOptions: [],
        intervalErr: '',
        nodata: true,
        roomsData: [],
    };

    componentWillMount() {
        const timeOptions = [];

        this.props.AllGatewaysRequest(currentCompany.companyId);
        this.props.AllRoomsRequest(currentCompany.companyId);

        timeInterval.forEach((time, index) => {
            timeOptions.push(<option key={`time-${time}`} value={index}>{time}</option>);
        });

        this.setState({timeOptions});
    }

    componentWillReceiveProps(nextProps) {
        // Call multiple times getSamplesByHours by interval 30 minutes
        if (this.props.teamUsers !== nextProps.teamUsers) {
            const addresses = [];
            const firstTime = moment(`${this.state.day} ${this.state.from.value}`);
            const lastTime = moment(`${this.state.day} ${this.state.to.value}`);

            nextProps.teamUsers.forEach((add) => {
                addresses.push(add.macAddress);
            });

            this.props.getSamplesByHours(currentCompany.companyId, addresses, this.state.day, moment(firstTime).format('HH:mm:ss'), moment(lastTime).format('HH:mm:ss'));
        }

        if (this.props.teamUsersSamples !== nextProps.teamUsersSamples) {
            let nodata = false;
            const samplesByTime = indexSamplesByTime(nextProps.teamUsersSamples, this.state.day, this.state.from.value, this.state.to.value);
            const timeSpentByUserByGtw = [];

            // get the time spent by gateway for each user
            samplesByTime.forEach((sample) => {
                if (sample.data.length > 0) {
                    nodata = false;
                    timeSpentByUserByGtw.push(getSamplesByGtw(sample.data, sample.key));
                } else {
                    nodata = true;
                }
            });

            if (!nodata) {
                this.setState({nodata: true});

                const timeSpentByZone = [];
                timeSpentByUserByGtw.forEach((sample) => {
                    timeSpentByZone.push(getSamplesByZone(sample, nextProps.gateways, nextProps.rooms));
                });

                const samplesByTimeZone = [];
                timeSpentByZone.forEach((samplesByZone) => {
                    samplesByTimeZone.push(getSamplesByZoneTimeSpentPourcentage(samplesByZone));
                });

                //drawVisualization(samplesByTimeZone, `${this.state.day} ${this.state.from.value}`);
            } else {
                this.setState({nodata: false});
                //drawVisualization([], `${this.state.day} ${this.state.from.value}`);
            }


            // count by zones
            const rooms = this.props.rooms;
            const roomsData = {};
            rooms.forEach(room => {
                roomsData[room.name] = {
                    steps: 0,
                    data: [],
                };
            });

            if (nextProps.teamUsersSamples && nextProps.teamUsersSamples.length > 0 && nextProps.teamUsersSamples[0] && nextProps.teamUsersSamples[0].length > 0) {
                Object.keys(nextProps.teamUsersSamples[0][0]).map(macAddress => {
                    nextProps.teamUsersSamples[0][0][macAddress].map(data => {
                        rooms.forEach(room => {
                            room.gateways.forEach(gtw => {
                                if (data.deviceID === gtw.name) {
                                    roomsData[room.name].steps += data.steps;
                                    roomsData[room.name].data.push({
                                        deviceID: data.deviceID,
                                        macAddress: macAddress,
                                        timestamp: data.timestamp,
                                        battery: data.battery,
                                        temperature: data.temperature,
                                        rawSteps: data.rawSteps,
                                        steps: data.steps,
                                    });
                                }
                            });
                        });
                    });
                });
            }

            const d = Object.keys(roomsData).map(room => roomsData[room].steps);
            if (d.length === 0) {
                Object.keys(roomsData).map(room => roomsData[room].ratio = 0);
            } else {
                const max = Object.keys(roomsData).map(room => roomsData[room].steps).reduce((a, b) => a + b);
                if (max === 0) {
                    Object.keys(roomsData).map(room => roomsData[room].ratio = 0);
                } else {
                    Object.keys(roomsData).map(room => roomsData[room].ratio = Math.round(roomsData[room].steps / max * 100));
                }
            }
            this.setState({roomsData});
        }
    }

    setField = (key, value) => {
        this.setState({[key]: value});
    }

    checkTimeInterval = (key, value) => {
        const val = Number(value);

        if (key === 'from' && val >= this.state.to.index) {
            this.setState({
                to: {
                    index: val + 1,
                    value: timeInterval[val + 1],
                },
            });
        } else if (key === 'to' && val <= this.state.from.index) {
            this.setState({
                from: {
                    index: val - 1,
                    value: timeInterval[val - 1],
                },
            });
        }

        this.setState({
            [key]: {
                index: value,
                value: timeInterval[value],
            },
        });
    }

    handleSubmit = () => {
        const addresses = [];
        const firstTime = moment(`${this.state.day} ${this.state.from.value}`);
        const lastTime = moment(`${this.state.day} ${this.state.to.value}`);
        const diff = lastTime.diff(firstTime, 'minute');
        // if (lastTime > moment()) {
        //     this.setState({intervalErr: 'Incorrect date range'});
        // } else if (diff < 120) {
        //     this.setState({intervalErr: 'Interval too short, minimum of 1h30 interval required.'});
        // } else {
        this.setState({intervalErr: ''});
        this.props.teamUsers.forEach((add) => {
            addresses.push(add.macAddress);
        });
        this.props.getSamplesByHours(currentCompany.companyId, addresses, this.state.day, moment(firstTime).format('HH:mm:ss'), moment(lastTime).format('HH:mm:ss'));
        // }
    }


    render() {
        return (
            <Wrapper>
                <Row>
                    <Col s={12} m={12} l={3}>
                        <DateInput
                            label={'Day'}
                            name={'day'}
                            value={moment().format('YYYY-MM-DD')}
                            s={12}
                            m={12}
                            l={12}
                            cb={this.setField}
                        />

                    </Col>
                    <Col s={6} m={6} l={3}>
                        <Select
                            label="From"
                            name={'from'}
                            multiple={false}
                            defaultVal={this.state.from.index}
                            options={this.state.timeOptions}
                            cb={this.checkTimeInterval}
                            s={12}
                            m={12}
                            l={12}
                        />
                    </Col>
                    <Col s={6} m={6} l={3}>
                        <Select
                            label="To"
                            name={'to'}
                            multiple={false}
                            defaultVal={this.state.to.index}
                            options={this.state.timeOptions}
                            cb={this.checkTimeInterval}
                            s={12}
                            m={12}
                            l={12}
                        />
                    </Col>
                    <Col
                        s={12}
                        m={12}
                        l={3}
                        style={{textAlign: 'center'}}
                    >
                        <Button
                            key="submit"
                            id="submit"
                            waves="light"
                            onClick={this.handleSubmit}
                            style={{
                                backgroundColor: '#4caf50',
                                marginTop: '10px',
                            }}
                        >
                            RUN
                            <Icon left>play_arrow</Icon>
                        </Button>
                    </Col>
                </Row>
                {
                    this.state.intervalErr !== '' ?
                        <p
                            style={{textAlign: 'center'}}
                        >
                            {this.state.intervalErr}
                        </p> : ''
                }

                {<ReactRow>
                    {Object.keys(this.state.roomsData).map(room => (
                        <ReactCol s={12} m={3} l={3} key={`room-${room}`}>
                            <div className="room-info">
                                <div className="room-name">{room}:</div>
                                <div
                                    className="room-persons">{this.state.roomsData[room].steps.toLocaleString()} steps&nbsp;({this.state.roomsData[room].ratio}%)
                                </div>
                            </div>
                        </ReactCol>
                    ))}
                </ReactRow>}

                <ul>
                    {Object.keys(this.state.roomsData).map((room, key) => <li
                        key={key}>
                        <h1>{room}</h1>
                        <table>
                            <thead>
                            <tr>
                                <th>DeviceID</th>
                                <th>MACAddress</th>
                                <th>Timestamp</th>
                                <th>Battery</th>
                                <th>Temperature</th>
                                <th>RawSteps</th>
                                <th>Steps</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.roomsData[room].data.map((data, idx) => <tr key={idx}>
                                    <td>{data.deviceID}</td>
                                    <td>{data.macAddress}</td>
                                    <td>{moment(data.timestamp).format('HH:mm:ss')}</td>
                                    <td>{data.battery}</td>
                                    <td>{data.temperature}</td>
                                    <td>{data.rawSteps.toLocaleString()}</td>
                                    <td>{data.steps.toLocaleString()}</td>
                                </tr>)
                            }
                            </tbody>
                        </table>
                    </li>)}
                </ul>
            </Wrapper>
        );
    }
}

TeamZonesChart.propTypes = {
    teamUsers: React.PropTypes.array,
    teamUsersSamples: React.PropTypes.array,
    AllGatewaysRequest: React.PropTypes.func,
    AllRoomsRequest: React.PropTypes.func,
    getSamplesByHours: React.PropTypes.func,
};

export default TeamZonesChart;
