import React from 'react';
import L from 'leaflet';
import { Marker, Tooltip } from 'react-leaflet';
// import iconImg from './marker-icon-red.png';

export default class Marker2 extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    marker: null,
    blinkCss: 'blinking',
    opacity: 1.0,
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.marker) {
      this.setState({ opacity: 1.0 });
      this.setState({ blinkCss: 'blinking' });
      this.setState({ marker: nextProps.marker });
    } else {
      this.setState({ blinkCss: '' });
    }
  }

  render() {
    const icon = new L.Icon({
      iconUrl: 'marker-icon-red.png',
      iconAnchor: new L.Point(12, 42),
      popupAnchor: null,
      shadowUrl: null,
      shadowSize: null,
      shadowAnchor: null,
      iconSize: new L.Point(25, 43),
      className: this.state.blinkCss,
    });

    let marker = <div></div>;

    if (this.state.marker !== null) {
      marker = (
        <Marker
          key={this.state.marker.name}
          position={[this.state.marker.latitude, this.state.marker.longitude]}
          opacity={this.state.opacity}
          icon={icon}
        >
          <Tooltip>
            <div>
              <div>{this.state.marker.name}</div>
              <div>{this.state.marker.room}</div>
              <div>{this.state.marker.steps}</div>
              <div>{this.state.marker.timestamp}</div>
            </div>
          </Tooltip>
        </Marker>
      );
    }

    return (
      <div>
        { marker }
      </div>
    );
  }
}

Marker2.propTypes = {
  marker: React.PropTypes.object,
};
