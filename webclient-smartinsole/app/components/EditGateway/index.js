import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import Row from 'components/Row';
import { Button, Icon } from 'react-materialize';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class EditGateway extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    id: '',
    name: '',
    roomId: '',
    latitude: '',
    longitude: '',
    roomsOptions: [],
  };

  componentWillMount() {
    const { gatewayId } = this.props.params;

    this.props.GetGatewayRequest(currentCompany.companyId, gatewayId);
    this.props.setPageTitle('Edit gateway');
    this.props.AllRoomsRequest(currentCompany.companyId);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.rooms !== nextProps.rooms) {
      const roomsOptions = [];

      nextProps.rooms.forEach((room) => {
        roomsOptions.push(
          <option
            key={`room-${room.id}`}
            value={room.id}
          >{room.name}</option>
        );
      });
      this.setState({
        roomId: nextProps.rooms[0].id,
        roomsOptions,
      });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { id, name, roomId, latitude, longitude } = this.props.gateway;

    const gateway = {};
    gateway.id = id;
    gateway.name = this.state.name || name;
    gateway.roomId = this.state.roomId || roomId;
    gateway.latitude = this.state.latitude || latitude;
    gateway.longitude = this.state.longitude || longitude;

    this.props.EditGatewayRequest(currentCompany.companyId, gateway);
  }

  render() {
    const { name, roomId, longitude, latitude } = this.props.gateway;

    return (
      <div>
        <Form
          className="stayActive"
          node={[
            <Row
              key="form-inputs"
            >
              <Input
                key="name"
                label="Name"
                name="name"
                cb={this.setField}
                value={name || ''}
                s={12}
                m={6}
                l={6}
              />
              <Select
                label="Select the room"
                key="select-room"
                name="roomId"
                defaultValue={roomId || ''}
                options={this.state.roomsOptions || []}
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="latitude"
                label="Latitude"
                name="latitude"
                cb={this.setField}
                value={latitude || ''}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="longitude"
                label="Longitude"
                name="longitude"
                cb={this.setField}
                value={longitude || ''}
                s={12}
                m={6}
                l={6}
              />
            </Row>,
            <Row
              key="form-submit"
            >
              <Button
                key="submit"
                id="submit"
                waves="light"
                onClick={this.handleSubmit}
                className="blue"
              >
                Edit
                <Icon left>mode_edit</Icon>
              </Button>
            </Row>,
          ]}
        />
      </div>
    );
  }
}

EditGateway.propTypes = {
  setPageTitle: React.PropTypes.func,
  params: React.PropTypes.object.isRequired,
  GetGatewayRequest: React.PropTypes.func.isRequired,
  EditGatewayRequest: React.PropTypes.func.isRequired,
  gateway: React.PropTypes.object,
  AllRoomsRequest: React.PropTypes.func,
  rooms: React.PropTypes.array,
};

export default EditGateway;
