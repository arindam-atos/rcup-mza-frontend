import { Col as MCol } from 'react-materialize';
import styled from 'styled-components';

const Col = styled(MCol)`
padding: 0 !important;

@media (max-width: 992px) {
  & {
    margin: 0;
    }
  }
`;

export default Col;
