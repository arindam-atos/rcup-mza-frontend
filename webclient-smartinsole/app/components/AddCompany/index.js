import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import { Row, Col, Button, Icon, Collection, CollectionItem } from 'react-materialize';
import CreateMap from 'components/CreateMap';
import { Wrapper } from './styles';

class AddCompany extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    name: '',
    address: '',
    city: '',
    zipCode: '',
    country: '',
    latitude: '',
    longitude: '',
    map: '',
    listHardnesses: [],
  };

  componentWillMount = () => {
    this.props.setPageTitle('Add company');
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  setMap = (map) => {
    this.setState({ map: JSON.stringify(map) });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let hardnesses = '';

    this.state.listHardnesses.forEach((hardness, i) => {
      const tmp = hardness.replace(/^\s+/, '').replace(/\s+$/, '');
      // Remove empty input
      if (tmp !== '' && i === 0) {
        hardnesses = `${hardness}`;
      } else if (tmp !== '') {
        hardnesses = `${hardnesses};${hardness}`;
      } else {
        this.state.listHardnesses.splice(i, 1);
      }
    });


    const company = this.state;
    company.hardnesses = hardnesses;
    this.props.AddCompanyRequest(company);
  }

  addInput = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
      listHardnesses: [...prevState.listHardnesses, ''],
    }));
  }


  handleChange = (event, index) => {
    const values = [...this.state.listHardnesses];
    values[index] = event.target.value;
    this.setState({ listHardnesses: values });
  }

  removeInput = (e, el) => {
    e.preventDefault();
    const index = this.state.listHardnesses.indexOf(el);

    const listHardnesses = [...this.state.listHardnesses];
    listHardnesses.splice(index, 1);

    this.setState({ listHardnesses });
  }

  createInput = () =>
    <Row key="hardnesses-input" id="hardness-input">
      <Collection header="List of hardnesses" key="inputs-collection">
        {
        this.state.listHardnesses.map((el, i) =>
          <CollectionItem key={`collection-item-${i}`}>
            <Col s={6} m={8} l={6}>
              <input
                key={el}
                defaultValue={el}
                onBlur={(e) => this.handleChange(e, i)}
              />
            </Col>
            <Col s={2} m={4} l={2}>
              <Button
                onClick={(e) => this.removeInput(e, el)}
              >Remove</Button>
            </Col>
          </CollectionItem>
          )
        }
      </Collection>
      <Button
        onClick={this.addInput}
      >Add</Button>
    </Row>

  render() {
    return (
      <Wrapper>
        <Form
          node={[
            <Input
              key="name"
              label="Name"
              name="name"
              cb={this.setField}
            />,
            <Input
              key="address"
              label="Address"
              name="address"
              cb={this.setField}
            />,
            <Input
              key="city"
              label="City"
              name="city"
              cb={this.setField}
              s={6}
              m={6}
              l={6}
            />,
            <Input
              key="zipCode"
              label="Zipcode"
              name="zipCode"
              cb={this.setField}
              s={6}
              m={6}
              l={6}
            />,
            <Input
              key="country"
              label="Country"
              name="country"
              cb={this.setField}
            />,
            <Input
              key="file"
              label="file"
              name="file"
              cb={this.setField}
              type="file"
            />,
            <Input
              key="latitude"
              label="Latitude"
              name="latitude"
              cb={this.setField}
              s={6}
              m={6}
              l={6}
            />,
            <Input
              key="longitude"
              label="Longitude"
              name="longitude"
              cb={this.setField}
              s={6}
              m={6}
              l={6}
            />,
            <CreateMap
              key="map"
              setMap={this.setMap}
            />,
            this.createInput(),
            <Button
              key="submit"
              id="submit"
              waves="light"
              onClick={this.handleSubmit}
              className="blue"
            >
              Add
              <Icon left>create</Icon>
            </Button>,
          ]}
        />
      </Wrapper>
    );
  }
}

AddCompany.propTypes = {
  setPageTitle: React.PropTypes.func,
  AddCompanyRequest: React.PropTypes.func.isRequired,
};

export default AddCompany;
