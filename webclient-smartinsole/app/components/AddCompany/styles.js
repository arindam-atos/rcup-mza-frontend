import styled from 'styled-components';

export const Wrapper = styled.div`
  .collection-item {
    height: 80px;
  }

  .collection h4 {
    font-size: 1.28rem;
  }
`;
