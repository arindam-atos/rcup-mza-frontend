import { Row as MRow } from 'react-materialize';
import styled from 'styled-components';

const Row = styled(MRow)`

margin-bottom: 0.3rem;

@media (max-width: 992px) {
  & {
    margin: 0;
    }
  }

  .no-margin {
    margin-bottom: 0;
  }
`;

export default Row;
