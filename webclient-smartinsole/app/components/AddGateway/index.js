import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import { Button, Icon } from 'react-materialize';
import Row from 'components/Row';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class AddGateway extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    name: '',
    roomsOptions: [],
    latitude: '',
    longitude: '',
    roomId: 0,
  };

  componentWillMount = () => {
    this.props.setPageTitle('Add gateway');
    this.props.AllRoomsRequest(currentCompany.companyId);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.rooms !== nextProps.rooms) {
      const roomsOptions = [];

      nextProps.rooms.forEach((room) => {
        roomsOptions.push(
          <option
            key={`room-${room.id}`}
            value={room.id}
          >{room.name}</option>
        );
      });
      this.setState({
        roomId: nextProps.rooms[0].id,
        roomsOptions,
      });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const gateway = this.state;

    this.props.AddGatewayRequest(currentCompany.companyId, gateway);
  }

  render() {
    return (
      <div>
        <Form
          node={[
            <Row
              key="form-inputs"
            >
              <Input
                key="name"
                label="Name"
                name="name"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Select
                label="Select the room"
                key="select-room"
                name="roomId"
                defaultValue=""
                options={this.state.roomsOptions || []}
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="latitude"
                label="Latitude"
                name="latitude"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="longitude"
                label="Longitude"
                name="longitude"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
            </Row>,
            <Row
              key="form-submit"
            >
              <Button
                key="submit"
                id="submit"
                waves="light"
                onClick={this.handleSubmit}
                className="blue"
              >
                Add
                <Icon left>create</Icon>
              </Button>
            </Row>,
          ]}
        />
      </div>
    );
  }
}

AddGateway.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllRoomsRequest: React.PropTypes.func,
  rooms: React.PropTypes.array,
  AddGatewayRequest: React.PropTypes.func.isRequired,
};

export default AddGateway;
