import { Table as MTable, Button, Icon as MIcon } from 'react-materialize';
import styled from 'styled-components';

export const Wrapper = styled.div`
  input {
    margin-bottom: 0;
  }
`;

export const Table = styled(MTable)`
  & .fixed-action-btn  {
    position: relative;
    right: 0;
    bottom: 0;
  }

  & .fixed-action-btn.horizontal ul {
    top: 15%;
    right: 42px;
  }
`;

export const Th = styled.th`
  text-align: center;
`;

export const Td = styled.td`
  text-align: right;
`;

export const Tr = styled.tr`
  &:hover {
    background: #d0d0d0;
  }
`;

export const ButtonAdd = styled(Button)`
  float: right;
`;

export const Icon = styled(MIcon)`
  .search {
    line-height: 2em;
  }
`;
