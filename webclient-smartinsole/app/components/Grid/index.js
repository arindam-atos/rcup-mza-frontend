import React from 'react';
import Pagination from 'components/Pagination';
import Popup from 'components/Popup';
import { browserHistory } from 'react-router';
import { Row, Input, Button } from 'react-materialize';
import { Wrapper, Table, Th, Td, Tr, ButtonAdd, Icon } from './styles';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class Grid extends React.PureComponent {//  eslint-disable-line react/prefer-stateless-function

  state = {
    show: false,
    columns: this.props.columns,
    datas: this.props.datas,
    dataId: null,
  };

  componentWillReceiveProps = (nextProps) => {
    const datas = [];
    const size = (nextProps.datas.length < 10 ? nextProps.datas.length : 10);

    for (let i = 0; i < size; i += 1) {
      datas.push(nextProps.datas[i]);
    }

    this.setState({ datas });
  }

  // Method to get 10 first items for pagination
  initFirstItems = () => {
    const datas = [];
    const size = this.props.datas.length < 10 ? this.props.datas.length : 10;

    for (let i = 0; i < size; i += 1) {
      datas.push(this.props.datas[i]);
    }

    this.setState({ datas });
  }

  // Filter Input
  filterItem = (event) => {
    const items = [];

    this.props.datas.forEach((data) => {
      Object.keys(data).some((item) => {
        if (`${data[item]}`.indexOf(event.target.value) !== -1) {
          items.push(data);
          return true;
        }
        return false;
      });
    });

    if (event.target.value === '') {
      this.initFirstItems();
    } else {
      this.setState({ datas: items });
    }
  }

  changePage = (event) => {
    const paginatedItems = [];
    const datas = this.props.datas;

    for (let i = (event * 10) - 10; i < event * 10; i += 1) {
      if (datas[i]) {
        paginatedItems.push(datas[i]);
      }
    }

    this.setState({ datas: paginatedItems });
  }

  confirmDelete = () => {
    this.setState({ show: false });
    this.props.deleteRequest(currentCompany.companyId, this.state.data.id);
  }

  cancelDelete = () => {
    this.setState({ show: false });
  }

  render() {
    return (
      <Wrapper>
        <Row>
          <Input s={6} m={6} l={6} label="Filter" onChange={this.filterItem} className="search">
            <Icon>search</Icon>
          </Input>
          {
            this.props.addUrl ?
              <ButtonAdd
                floating
                className="green"
                waves="light"
                icon="add"
                node="a"
                onClick={(e) => {
                  e.preventDefault();
                  browserHistory.push(this.props.addUrl);
                }}
                href={this.props.addUrl}
              /> : ''
          }
        </Row>

        <Table
          bordered
          responsive
        >
          <thead>
            <tr>
              {
                this.state.columns.map((column, index) =>
                  <th
                    data-field={column}
                    key={`column-${index}`}
                  >
                    {column}
                  </th>
                )
              }
              {
                this.props.actions ? <Th>Actions</Th> : <Th />
              }
            </tr>
          </thead>

          <tbody>
            {
              this.state.datas.map((data, index) =>
                <Tr key={`row-${index}`}>
                  {
                    Object.keys(data).map((val, keyVal) =>
                      <td
                        key={`td-${keyVal}`}
                      >
                        { data[val] }
                      </td>)
                    }
                  { this.props.actions ?
                    <Td>
                      <Button floating fab="horizontal" icon="fast_rewind" className="blue" waves="light" >
                        <Button
                          floating
                          icon="mode_edit" className="green"
                          node="a" href={`${this.props.editUrl}/${data.id}`}
                          onClick={(e) => {
                            e.preventDefault();
                            browserHistory.push(`${this.props.editUrl}/${data.id}`);
                          }}
                        />
                        <Button floating icon="not_interested" className="red darken-1" onClick={() => this.setState({ data, show: true })} />
                      </Button>
                    </Td> : <Td />
                  }
                </Tr>
              )
            }

          </tbody>
        </Table>
        <Popup
          show={this.state.show}
          onConfirm={this.confirmDelete}
          onCancel={this.cancelDelete}
        />
        <Pagination
          datas={this.props.datas}
          cb={this.changePage}
        />
      </Wrapper>
    );
  }
}

Grid.propTypes = {
  columns: React.PropTypes.array,
  datas: React.PropTypes.array,
  actions: React.PropTypes.bool,
  addUrl: React.PropTypes.string,
  editUrl: React.PropTypes.string,
  deleteRequest: React.PropTypes.func,
};

export default Grid;
