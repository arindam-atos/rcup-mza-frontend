import React from 'react';
import { browserHistory } from 'react-router';
import SelectCompany from 'containers/SelectCompany';
import { Icon } from 'react-materialize';
import { Navbar, NavItem, Collapsible, CollapsibleItem } from './styles';

const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

class NavBar extends React.PureComponent {//  eslint-disable-line react/prefer-stateless-function

  state = {
    pageTitle: '',
    ressourceItem: false,
    companyItem: false,
  }

  componentDidMount() {
    // Add listener to close or keep opened collapsible item from navbar
    window.addEventListener('click', (e) => {
      const ressourceItems = document.getElementById('ressource-items');
      if (ressourceItems.contains(e.target)) {
        ressourceItems.getElementsByClassName('collapsible-body')[0].style.display = 'block';
      } else {
        ressourceItems.getElementsByClassName('collapsible-body')[0].style.display = 'none';
      }

      if (loggedUser.role === 'admin') {
        const listCompDiv = document.getElementById('list-companies');
        if (listCompDiv.contains(e.target)) {
          document.getElementById('list-companies').getElementsByClassName('collapsible-body')[0].style.display = 'block';
        } else {
          document.getElementById('list-companies').getElementsByClassName('collapsible-body')[0].style.display = 'none';
        }
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.pageTitle !== nextProps.pageTitle) {
      this.setState({ pageTitle: nextProps.pageTitle });
    }
  }

  render() {
    return (
      <Navbar left>
        <NavItem className="page-title">
          {this.state.pageTitle}
        </NavItem>


        <NavItem href="/logout">
          <Icon>power_settings_new</Icon>
        </NavItem>

        <NavItem
          onClick={(e) => {
            browserHistory.push(`/edit-user/${loggedUser.id}`);
            e.preventDefault();
          }}
        >
          <Icon>account_circle</Icon>
        </NavItem>

        <NavItem
          href={'#'}
          id="ressource-items"
          onClick={(e) => e.preventDefault()}
        >
          <Collapsible>
            <CollapsibleItem header="" icon="settings">

              <ul>
                { /* eslint-disable jsx-a11y/no-static-element-interactions */ }
                {
                  loggedUser.role === 'admin' ?
                    <li
                      key="companies"
                      onClick={() => {
                        browserHistory.push('/companies');
                      }}
                    >
                      Companies
                    </li> : ''
                }

                <li
                  key="rooms"
                  onClick={() => {
                    browserHistory.push('/rooms');
                  }}
                >
                  Rooms
                </li>
                <li
                  key="gateways"
                  onClick={() => {
                    browserHistory.push('/gateways');
                  }}
                >
                  Gateways
                </li>
                <li
                  key="teams"
                  onClick={() => {
                    browserHistory.push('/teams');
                  }}
                >
                  Teams
                </li>
                <li
                  key="users"
                  onClick={() => {
                    browserHistory.push('/users');
                  }}
                >
                  Users
                </li>
                { /* eslint-enable jsx-a11y/no-static-element-interactions */ }
              </ul>
            </CollapsibleItem>

          </Collapsible>
        </NavItem>


        <NavItem href="#!">
          <Icon>
            notifications_none
          </Icon>
        </NavItem>

        <NavItem
          id="list-companies"
          onClick={(e) => e.preventDefault()}
        >
          <SelectCompany />
        </NavItem>

      </Navbar>
    );
  }
}

NavBar.propTypes = {
  pageTitle: React.PropTypes.string,
};

export default NavBar;
