import styled from 'styled-components';
import { Navbar as MNavbar, NavItem as MNavItem, Collapsible as MCollapsible, CollapsibleItem as MCollapsibleItem } from 'react-materialize';

export const Navbar = styled(MNavbar)`
  background: #FFF;
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
  box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
  background-color: #fff;

  .page-title {
    color: #777;
    pointer-events: none;
    cursor: default;
    float: left;
  }

  @media (max-width: 992px) {
    & {
      display: none;
      }
    }

`;

export const NavItem = styled(MNavItem)`
  float: right;
  a {
    color: #777;
  }

  &:hover {
    background-color: rgba(0,0,0,0.1);
  }

`;

export const Collapsible = styled(MCollapsible)`
  border: 0;
  margin: 0;

  .active {
    background: transparent;
  }
`;

export const CollapsibleItem = styled(MCollapsibleItem)`
  .collapsible-body {
    padding: 0;
    z-index: 1000;
    position: absolute;
    right: 109px;
    top: 65px;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
    box-shadow: 0 1px 2px rgba(0,0,0,.05), 0 0 0 1px rgba(63,63,68,.1);
    background-color: #fff;

    li {
      float: none;
      padding: 0 1rem;
      width: 150px;
      text-align: left;
    }

    li:hover {
      background-color: rgba(0,0,0,0.1);
    }
  }

  .collapsible-header {
    padding: 0;
    border-bottom: none;
    background: transparent;

    .material-icons {
      margin-right: 0;
    }
  }
`;
