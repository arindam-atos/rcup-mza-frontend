/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { browserHistory } from 'react-router';
import SideBar from 'containers/SideBar';
import NavBar from 'containers/NavBar';
import Login from 'containers/Login';
import styled from 'styled-components';
const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

const Wrapper = styled.div`
  min-height: 100vh;
`;

const Container = styled.div`
  background: hsla(240,7%,81%,.15);
  position: relative;
  float: right;
  width: calc(100% - 300px);
  margin-left: 300px;
  min-height: 100vh;
  @media (max-width: 992px) {
    margin-top: 40px;
    margin-left: 0px;
    padding: 0;
    width: 100%;
    float: none'
  }
`;


export default class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: React.PropTypes.node,
  };

  componentWillMount() {
    // localStorage.clear();

    if (window.location.pathname === '/reset-pwd') {
      browserHistory.push('/reset-pwd');
    } else if (window.location.pathname === '/change-pwd') {
      browserHistory.push(`/change-pwd?access_token=${this.props.location.query.access_token}&username=${this.props.location.query.username}`);
    } else if (!loggedUser) {
      browserHistory.push('/login');
    } else if (loggedUser &&  window.location.pathname === '/login') {
      browserHistory.push('/');
    }
    // } else if (loggedUser) {
    //   browserHistory.push('/login');
    // }
  }

  componentWillUnmount() {
    localStorage.clear();
  }

  render() {
    let logged = false;

    if (loggedUser) {
      logged = true;
    }

    let content = '';

    if (window.location.pathname === '/reset-pwd' || window.location.pathname === '/change-pwd') {
      content = this.props.children;
    } else if (logged) {
      content = (
        <div>
          <SideBar />
          <Container>
            <NavBar />
            {this.props.children}
          </Container>
        </div>);
    } else {
      content = (
        <Login />
      );
    }

    return (
      <Wrapper>
        { content }
      </Wrapper>
    );
  }
}

App.propTypes = {
  location: React.PropTypes.object,
};
