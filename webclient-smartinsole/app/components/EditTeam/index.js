import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import { Button, Icon } from 'react-materialize';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class EditTeam extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    name: '',
    managersId: this.props.team.managersId,
    workersId: this.props.team.workersId,
    managersOptions: [],
    workersOptions: [],
  };

  componentWillMount() {
    this.props.setPageTitle('Edit team');
    this.props.AllUsersRequest(currentCompany.companyId);
    this.props.GetTeamRequest(currentCompany.companyId, this.props.params.teamId);
  }

  componentWillReceiveProps(nextProps) {
    const managersOptions = [
      <option key={0} disabled value="">{'Select'}</option>,
    ];
    const workersOptions = [
      <option key={0} disabled value="">{'Select'}</option>,
    ];
    const { users } = nextProps;

    let i = 1;

    if (users) {
      users.forEach((user) => {
        if (user.role === 'manager') {
          managersOptions.push(
            <option
              key={i}
              value={user.id}
            >
              {`${user.firstname} ${user.lastname}`}
            </option>
          );
        } else if (user.role === 'worker') {
          workersOptions.push(
            <option
              key={i}
              value={user.id}
            >
              {`${user.firstname} ${user.lastname}`}
            </option>);
        }
        i += 1;
      });

      this.setState({
        managersOptions,
        workersOptions,
      });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { id, name, managersId, workersId } = this.props.team;
    const team = {};

    team.id = id;
    team.name = this.state.name || name;
    team.managersId = this.state.managersId || managersId;
    team.workersId = this.state.workersId || workersId;

    this.props.EditTeamRequest(currentCompany.companyId, team);
  }

  render() {
    const { name, managersId, workersId } = this.props.team;

    return (
      <div>
        <Form
          node={[
            <Input
              key="name"
              label="Name"
              name="name"
              cb={this.setField}
              value={name || ''}
            />,
            <Select
              label="Select managers"
              key="managersId"
              name="managersId"
              multiple
              defaultVal={managersId || []}
              options={this.state.managersOptions}
              cb={this.setField}
            />,
            <Select
              label="Select workers"
              key="workersId"
              name="workersId"
              multiple
              defaultVal={workersId || []}
              options={this.state.workersOptions}
              cb={this.setField}
            />,
            <Button
              key="submit"
              id="submit"
              waves="light"
              className="blue"
              onClick={this.handleSubmit}
            >
              Edit
              <Icon left>mode_edit</Icon>
            </Button>,
          ]}
        />
      </div>
    );
  }
}

EditTeam.propTypes = {
  setPageTitle: React.PropTypes.func,
  params: React.PropTypes.object.isRequired,
  GetTeamRequest: React.PropTypes.func.isRequired,
  EditTeamRequest: React.PropTypes.func.isRequired,
  team: React.PropTypes.object,
  AllUsersRequest: React.PropTypes.func.isRequired,
  users: React.PropTypes.array,
};

export default EditTeam;
