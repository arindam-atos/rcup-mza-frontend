import React from 'react';
import Grid from 'components/Grid';
import { Row } from 'react-materialize';
import WhiteBox from 'components/WhiteBox';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class ListGateways extends React.Component {//  eslint-disable-line react/prefer-stateless-function

  state = {
    gateways: [],
  };

  componentWillMount() {
    this.props.AllGatewaysRequest(currentCompany.companyId);
    this.props.setPageTitle('Gateways list');
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.gateways !== nextProps.gateways) {
      const listGtw = [];
      nextProps.gateways.forEach((gtw) => {
        listGtw.push({
          id: gtw.id,
          name: gtw.name,
          room: gtw.room,
          latitude: gtw.latitude,
          longitude: gtw.longitude,
          createdAt: gtw.createdAt,
          updatedAt: gtw.updatedAt,
        });
        this.setState({ gateways: listGtw });
      });
    }
  }

  render() {
    const columns = ['id', 'Name', 'Room', 'Latitude', 'Longitude', 'Created', 'Last updated'];

    return (
      <Row>
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Grid
              key={'list-gateways'}
              columns={columns}
              datas={this.state.gateways}
              addUrl="/add-gateway"
              editUrl="/edit-gateway"
              deleteRequest={this.props.DeleteGatewayRequest}
              actions
            >
            </Grid>,
          ]}
        />
      </Row>
    );
  }
}

ListGateways.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllGatewaysRequest: React.PropTypes.func,
  DeleteGatewayRequest: React.PropTypes.func,
  gateways: React.PropTypes.array,
};

export default ListGateways;
