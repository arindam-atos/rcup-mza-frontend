import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import { Button, Icon, Row, Col, Collection, CollectionItem } from 'react-materialize';
import CreateMap from 'components/CreateMap';
import { Wrapper } from './styles';

class EditCompany extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    name: '',
    address: '',
    zipCode: '',
    city: '',
    country: '',
    latitude: 0,
    longitude: 0,
    map: '',
    listHardnesses: [],
    hardnesses: '',
    gateways: [],
    logo: null,
  };

  componentWillMount() {
    this.props.GetCompanyRequest(this.props.params.companyId);
    this.props.setPageTitle('Edit company');
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.company !== nextProps.company && nextProps.company !== null) {
      let listHardnesses = [];
      if (nextProps.company.hardnesses !== null) {
        listHardnesses = nextProps.company.hardnesses.split(';');
      }
      this.setState({
        name: nextProps.company.name,
        address: nextProps.company.address,
        zipCode: nextProps.company.zipCode,
        city: nextProps.company.city,
        country: nextProps.company.country,
        latitude: nextProps.company.latitude,
        longitude: nextProps.company.longitude,
        map: nextProps.company.map,
        hardnesses: nextProps.company.hardnesses,
        listHardnesses,
        file: nextProps.company.file,
        logo: nextProps.company.logo,
      });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  setMap = (map) => {
    this.setState({ map: JSON.stringify(map) });
  }

  addInput = (e) => {
    e.preventDefault();
    this.setState((prevState) => ({
      listHardnesses: [...prevState.listHardnesses, ''],
    }));
  }


  handleChange = (event, index) => {
    const values = [...this.state.listHardnesses];
    values[index] = event.target.value;
    this.setState({ listHardnesses: values });
  }

  removeInput = (e, el) => {
    e.preventDefault();
    const index = this.state.listHardnesses.indexOf(el);

    const listHardnesses = [...this.state.listHardnesses];
    listHardnesses.splice(index, 1);

    this.setState({ listHardnesses });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { name, address, city, zipCode, country, latitude, longitude, map, file, logo } = this.props.company;

    let hardnesses = '';
    this.state.listHardnesses.forEach((hardness, i) => {
      const tmp = hardness.replace(/^\s+/, '').replace(/\s+$/, '');
      // Remove empty input
      if (tmp !== '' && i === 0) {
        hardnesses = `${hardness}`;
      } else if (tmp !== '') {
        hardnesses = `${hardnesses};${hardness}`;
      } else {
        this.state.listHardnesses.splice(i, 1);
      }
    });

    const company = {};
    company.name = this.state.name ? this.state.name : name;
    company.address = this.state.address ? this.state.address : address;
    company.city = this.state.city ? this.state.city : city;
    company.zipCode = this.state.zipCode ? this.state.zipCode : zipCode;
    company.country = this.state.country ? this.state.country : country;
    company.latitude = this.state.latitude ? this.state.latitude : latitude;
    company.longitude = this.state.longitude ? this.state.longitude : longitude;
    company.map = this.state.map ? this.state.map : map;
    company.hardnesses = this.state.hardnesses === hardnesses ? this.state.hardnesses : hardnesses;
    company.file = this.state.file === file ? null : this.state.file;
    company.logo = this.state.logo === logo ? this.state.logo : logo;

    this.props.EditCompanyRequest(this.props.params.companyId, company);
  }

  createInput = () =>
    <Row key="hardnesses-input" id="hardness-input">
      <Collection header="List of hardnesses" key="inputs-collection">
        {
        this.state.listHardnesses.map((el, i) =>
          <CollectionItem key={`collection-item-${i}`}>
            <Col s={6} m={8} l={6}>
              <input
                key={el}
                defaultValue={el}
                onBlur={(e) => this.handleChange(e, i)}
              />
            </Col>
            <Col s={2} m={4} l={2}>
              <Button
                onClick={(e) => this.removeInput(e, el)}
              >Remove</Button>
            </Col>
          </CollectionItem>
          )
        }
      </Collection>
      <Button
        onClick={this.addInput}
      >Add</Button>
    </Row>

  render() {
    return (
      <Wrapper>
        <Form
          className="stayActive"
          node={[
            <Input
              key="name"
              label="Name"
              name="name"
              cb={this.setField}
              value={this.state.name || ''}
            />,
            <Input
              key="address"
              label="Address"
              name="address"
              cb={this.setField}
              value={this.state.address || ''}
            />,
            <Input
              key="city"
              label="City"
              name="city"
              cb={this.setField}
              value={this.state.city || ''}
              s={6}
              m={6}
              l={6}
            />,
            <Input
              key="zipCode"
              label="Zipcode"
              name="zipCode"
              cb={this.setField}
              value={this.state.zipCode || ''}
              s={6}
              m={6}
              l={6}
            />,
            <Input
              key="country"
              label="Country"
              name="country"
              cb={this.setField}
              value={this.state.country || ''}
            />,
            <Input
              key="file"
              label="File"
              name="file"
              cb={this.setField}
              type="file"
            />,
            <Input
              key="latitude"
              label="Latitude"
              name="latitude"
              cb={this.setField}
              value={this.state.latitude || ''}
              s={6}
              m={6}
              l={6}
            />,
            <Input
              key="longitude"
              label="Longitude"
              name="longitude"
              cb={this.setField}
              value={this.state.longitude || ''}
              s={6}
              m={6}
              l={6}
            />,

            (this.state.map !== '') ?
              <CreateMap
                key="map"
                setMap={this.setMap}
                map={this.state.map}
                latitude={this.state.latitude}
                longitude={this.state.longitude}
              /> : <div key="map" />,
            this.createInput(),
            <Button
              key="submit"
              id="submit"
              waves="light"
              onClick={this.handleSubmit}
              className="blue"
            >
              Edit
              <Icon left>mode_edit</Icon>
            </Button>,
          ]}
        />
      </Wrapper>
    );
  }
}

EditCompany.propTypes = {
  setPageTitle: React.PropTypes.func,
  params: React.PropTypes.object.isRequired,
  GetCompanyRequest: React.PropTypes.func.isRequired,
  EditCompanyRequest: React.PropTypes.func.isRequired,
  company: React.PropTypes.object,
};

export default EditCompany;
