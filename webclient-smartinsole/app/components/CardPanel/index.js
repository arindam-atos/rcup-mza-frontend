import React from 'react';
import Row from 'components/Row';
import Col from 'components/Col';
import { CardPanel as MCardPanel } from 'react-materialize';

class CardPanel extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  componentWillUnmount() {
    this.props.resetCard();
  }

  render() {
    const { card } = this.props;

    return (
      <div>
        { card.message ?
          <Row>
            <Col s={1} m={1} l={1} />
            <Col s={this.props.s || 10} m={this.props.m || 10} l={this.props.l || 10}>
              <MCardPanel
                className={`${card.type === 'error' ? 'red' : 'blue'} lighten-4 black-text`}
              >
                <span>{card.message}</span>
              </MCardPanel>
            </Col>
          </Row> : ''
        }
      </div>
    );
  }
}

CardPanel.propTypes = {
  s: React.PropTypes.number,
  m: React.PropTypes.number,
  l: React.PropTypes.number,
  card: React.PropTypes.object.isRequired,
  resetCard: React.PropTypes.func.isRequired,
};

export default CardPanel;
