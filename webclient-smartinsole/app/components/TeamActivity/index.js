import React from 'react';
import moment from 'moment/moment';
import ReactTooltip from 'react-tooltip';
import { Wrapper } from './styles';
require('./flapper');
import axios from "axios";

const flapperLabelOptions = {
  width: 3,             // number of digits
  format: null,         // options for jquery.numberformatter, if loaded
  align: 'right',       // aligns values to the left or right of display
  // padding: '&nbsp;',    // value to use for padding
  chars: [' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],          // array of characters that Flapper can display
  // chars: [' ','0','1','2','3','4','5','6','7','8','9'],          // array of characters that Flapper can display

  chars_preset: 'alphanum',  // 'num', 'hexnum', 'alpha' or 'alphanum'
  timing: 250,          // the maximum timing for digit animation
  min_timing: 10,       // the minimum timing for digit animation
  threshhold: 100,      // the point at which Flapper will switch from
  // simple to detailed animations
  transform: true,       // Flapper automatically detects the jquery.transform
  // plugin. Set this to false if you want to force
  // transform to off
  on_anim_start: null,   // Callback for start of animation
  on_anim_end: null,     // Callback for end of animation
};

const flapperValuesOptions = {
  width: 6,             // number of digits
  format: null,         // options for jquery.numberformatter, if loaded
  align: 'right',       // aligns values to the left or right of display
  // padding: '&nbsp;',    // value to use for padding
  chars: [' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],          // array of characters that Flapper can display
  // chars: [' ','0','1','2','3','4','5','6','7','8','9'],          // array of characters that Flapper can display

  chars_preset: 'alphanum',  // 'num', 'hexnum', 'alpha' or 'alphanum'
  timing: 250,          // the maximum timing for digit animation
  min_timing: 10,       // the minimum timing for digit animation
  threshhold: 100,      // the point at which Flapper will switch from
  // simple to detailed animations
  transform: true,       // Flapper automatically detects the jquery.transform
  // plugin. Set this to false if you want to force
  // transform to off
  on_anim_start: null,   // Callback for start of animation
  on_anim_end: null,     // Callback for end of animation
};

class TeamActivity extends React.Component {//  eslint-disable-line react/prefer-stateless-function

  state = {
    flappersSize: '',
    yesterdayAvg: 0,
    dayAvg: 0,
    weekAvg: 0,
    minAvg: 0,
    maxAvg: 0,
  }

  componentDidMount = () => {
    $('#flapper-display-label-dayAvg').flapper(flapperLabelOptions).val($('#flapper-display-label-dayAvg').val()).change();

    $('#flapper-display-label-yesterdayAvg').flapper(flapperLabelOptions).val($('#flapper-display-label-yesterdayAvg').val()).change();

    $('#flapper-display-label-weekAvg').flapper(flapperLabelOptions).val($('#flapper-display-label-weekAvg').val()).change();

    $('#flapper-display-label-minAvg').flapper(flapperLabelOptions).val($('#flapper-display-label-minAvg').val()).change();

    $('#flapper-display-label-maxAvg').flapper(flapperLabelOptions).val($('#flapper-display-label-maxAvg').val()).change();


    $('#flapper-display-val-dayAvg').flapper(flapperValuesOptions).val(0).change();

    $('#flapper-display-val-yesterdayAvg').flapper(flapperValuesOptions).val(0).change();

    $('#flapper-display-val-weekAvg').flapper(flapperValuesOptions).val(0).change();

    $('#flapper-display-val-minAvg').flapper(flapperValuesOptions).val(0).change();

    $('#flapper-display-val-maxAvg').flapper(flapperValuesOptions).val(0).change();

    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions.bind(this));
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.usersSteps !== nextProps.usersSteps) {
      const daysAvg = {};
      const yesterdayDate = moment(moment().subtract(1, 'days').format('YYYY-MM-DD'));
      const todayDate = moment();
      let nbWorkers = 0;

      // First get number of steps by day excluding manager or director samples
      nextProps.usersSteps.forEach((user) => {
        // Only want workers' samples
        if (user.user.role === 'worker') {
          user.arrayStepsByUser.forEach((userSample) => {
            const sampleDate = moment(userSample.timestamp).format('YYYY-MM-DD');
            if (!Object.prototype.hasOwnProperty.call(daysAvg, sampleDate)) {
              daysAvg[sampleDate] = 0;
            }
            daysAvg[sampleDate] += userSample.step;
          });
          nbWorkers += 1;
        }
      });
      // Get average steps by day
      Object.keys(daysAvg).forEach((stepsByDay) => {
        daysAvg[stepsByDay] = Math.round(daysAvg[stepsByDay] / nbWorkers);
      });

      let nbDay = 0;
      let totalStepWeek = 0;
      let minAvg = {
        day: '',
        value: 0,
      };
      let maxAvg = {
        day: '',
        value: 0,
      };
      let dayAvg = 0;
      let yesterdayAvg = 0;
      Object.keys(daysAvg).forEach((avgByDay) => {
        const avgByDayDate = moment(avgByDay);
        if (nbDay === 0) {
          minAvg = {
            day: avgByDay,
            value: daysAvg[avgByDay],
          };
        }

        if (avgByDayDate.diff(todayDate, 'days') === 0) {
          dayAvg = daysAvg[avgByDay];
        }
        if (avgByDayDate.diff(yesterdayDate, 'days') === 0) {
          yesterdayAvg = daysAvg[avgByDay];
        }
        if (daysAvg[avgByDay] < minAvg.value) {
          minAvg = {
            day: avgByDay,
            value: daysAvg[avgByDay],
          };
        }
        if (daysAvg[avgByDay] > maxAvg.value) {
          maxAvg = {
            day: avgByDay,
            value: daysAvg[avgByDay],
          };
        }
        totalStepWeek += daysAvg[avgByDay];
        nbDay += 1;
      });

      const weekAvg = Math.round(totalStepWeek / nbDay);

      const urlArray = process.env.NODE_ENV.split("#");
      const historyUrl = "http://" + urlArray[1];
      let url = historyUrl + "/dayStats";
      axios
        .get(url)
        .then(response => {
          $('#flapper-display-val-dayAvg').val(response.data.dayAvg).change();
          $('#flapper-display-val-yesterdayAvg').val(response.data.yesterdayAvg).change();
          $('#flapper-display-val-weekAvg').val(response.data.weekAvg).change();
          $('#flapper-display-val-minAvg').val(response.data.minAvg).change();
          $('#flapper-display-val-maxAvg').val(response.data.maxAvg).change();
          this.setState({
            dayAvg,
            yesterdayAvg,
            weekAvg: Math.round(totalStepWeek / nbDay),
            minAvg,
            maxAvg,
          });
        })
        .catch(error => {
          console.log(error);
        });



    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions.bind(this));
  }

  updateDimensions = () => {
    if (window.innerWidth <= 340) {
      this.setState({ flappersSize: 'XS' });
    } else if (window.innerWidth > 340 && window.innerWidth <= 600) {
      this.setState({ flappersSize: 'S' });
    }
  }


  render() {
    return (
      <Wrapper>
        <div className="flapper-line">
          <div className="center-flapper" data-tip data-for="dayAvg">
            <input defaultValue="J" id="flapper-display-label-dayAvg" className={`flapper-label ${this.state.flappersSize}`} />
            <input id="flapper-display-val-dayAvg" className={this.state.flappersSize} />
          </div>
          <ReactTooltip id="dayAvg" type="info">
            <span>{'Today\'s average number of steps'}</span>
          </ReactTooltip>
        </div>

        <div className="flapper-line">
          <div className="center-flapper" data-tip data-for="yesterdayAvg">
            <input defaultValue="J-1" id="flapper-display-label-yesterdayAvg" className={`flapper-label ${this.state.flappersSize}`} />
            <input id="flapper-display-val-yesterdayAvg" className={this.state.flappersSize} />
          </div>
          <ReactTooltip id="yesterdayAvg" type="info">
            <span>{'Yesterday\'s average number of steps'}</span>
          </ReactTooltip>
        </div>

        <div className="flapper-line">
          <div className="center-flapper" data-tip data-for="weekAvg">
            <input defaultValue="W" id="flapper-display-label-weekAvg" className={`flapper-label ${this.state.flappersSize}`} />
            <input id="flapper-display-val-weekAvg" className={this.state.flappersSize} />
          </div>
          <ReactTooltip id="weekAvg" type="info">
            <span>{'This week\'s average number of steps'}</span>
          </ReactTooltip>
        </div>

        <div className="flapper-line">
          <div className="center-flapper" data-tip data-for="minAvg">
            <input defaultValue="MIN" id="flapper-display-label-minAvg" className={`flapper-label ${this.state.flappersSize}`} />
            <input id="flapper-display-val-minAvg" className={this.state.flappersSize} />
          </div>
          <ReactTooltip id="minAvg" type="info">
            <span>{`This week's day minimum average number of steps, ${this.state.minAvg.day}`}</span>
          </ReactTooltip>
        </div>

        <div className="flapper-line">
          <div className="center-flapper" data-tip data-for="maxAvg">
            <input defaultValue="MAX" id="flapper-display-label-maxAvg" className={`flapper-label ${this.state.flappersSize}`} />
            <input id="flapper-display-val-maxAvg" className={this.state.flappersSize} />
          </div>
          <ReactTooltip id="maxAvg" type="info">
            <span>{`This week's day maximum average number of steps, ${this.state.maxAvg.day}`}</span>
          </ReactTooltip>
        </div>
      </Wrapper>
    );
  }
}

TeamActivity.propTypes = {
  usersSteps: React.PropTypes.array,
};

export default TeamActivity;