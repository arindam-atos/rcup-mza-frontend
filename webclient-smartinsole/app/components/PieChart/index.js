import React from 'react';
import { ResponsiveContainer, PieChart as Chart, Pie, Legend, Cell } from 'recharts';

const PieChart = (props) => {
  let verticalAlign = 'middle';
  let align = 'right';
  let layout = 'vertical';
  let cx = 175;

  if (window.innerWidth <= 600) {
    cx = (window.innerWidth / 2) - 25;
  }

  if ((window.innerWidth <= 600) || (window.innerWidth > 992 && window.innerWidth < 1450)) {
    verticalAlign = 'bottom';
    align = 'center';
    layout = 'horizontal';
  }

  return (
    <ResponsiveContainer witdh="100%" height={385}>
      <Chart
        width={400}
        height={400}
        onMouseHover={this.onPieEnter}
        nameKey={'name'}
      >
        <Pie
          dataKey={'value'}
          data={props.data}
          cx={cx}
          cy={175}
          innerRadius={80}
          outerRadius={100}
          fill="#8884d8"
          paddingAngle={5}
          label={(data) => `${data.payload.value}%`}
        >
          {
            props.data.map((entry, index) =>
              <Cell key={`cell-${index}`} fill={props.colorsByType[entry.name]} />
            )
          }
        </Pie>
        <Legend
          align={align}
          verticalAlign={verticalAlign}
          layout={layout}
          iconType={'circle'}
        />
      </Chart>
    </ResponsiveContainer>
  );
};


PieChart.propTypes = {
  data: React.PropTypes.array,
  colorsByType: React.PropTypes.object,
};

export default PieChart;
