import React from 'react';
import { Wrapper } from './styles';

const Footer = () =>
  <Wrapper>
    <div>
      <p> © 2017 <a href="https://rcup.io">RCUP</a></p>
    </div>
  </Wrapper>;

export default Footer;
