import styled from 'styled-components';

export const Wrapper = styled.footer`
  position: relative;
  float: right;
  margin-left: 300px;
  width: calc(100% - 300px);
  background: #FFF;
  color: #9a9a9a;
  border-top: 1px solid #e7e7e7;
  display: block;

  & p {
    position: absolute;
    right: 20px;
    margin-top: 0px;
    padding-top: 10px;
    padding-bottom: 10px;
  }

  @media (max-width: 992px) {
    margin-left: 0px;
  }

  & > div {
    height:20px;
  }
`;
