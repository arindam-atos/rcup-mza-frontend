import React from 'react';
import { ResponsiveContainer, BarChart, Bar, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import moment from 'moment/moment';

class TeamChart extends React.PureComponent {//  eslint-disable-line react/prefer-stateless-function
  state = {
    data: [],
  }

  componentWillReceiveProps(nextProps) {
    const data = [];
    if (nextProps.userWithSteps) {
      const averageSteps = [];

      let cpt = 0;
      nextProps.userWithSteps.forEach((user) => {
        user.arrayStepsByUser.forEach((nbStep) => {
          const date = moment(nbStep.timestamp).format('DD/MM/YYYY');
          if (cpt === 0) {
            averageSteps[date] = nbStep.rawStep;
          } else {
            averageSteps[date] += nbStep.rawStep;
          }
        });
        cpt += 1;
      });
      Object.keys(averageSteps).forEach((prop) => {
        const obj = {};
        obj.timestamp = prop;
        obj.rawSteps = averageSteps[prop] / nextProps.userWithSteps.length;
        data.push(obj);
      });
      this.setState({ data });
    }
  }

  render() {
    const chart = (
      <ResponsiveContainer witdh="100%" height={400}>

        <BarChart
          height={300}
          data={this.state.data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis dataKey="timestamp" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Scatter data={this.state.data} line={<BarChart />} />

          <Bar dataKey="rawSteps" fill="#1e2736" />

        </BarChart>

      </ResponsiveContainer>
    );

    return (

      <div>
        {chart}
      </div>
    );
  }
}


TeamChart.propTypes = {
  userWithSteps: React.PropTypes.array,
  cb: React.PropTypes.func,
};

export default TeamChart;
