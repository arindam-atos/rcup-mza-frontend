import React from 'react';
import {Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, Scatter, Tooltip, XAxis, YAxis} from 'recharts';
import moment from 'moment/moment';
import {Wrapper} from './styles';

class TeamChart extends React.PureComponent {//  eslint-disable-line react/prefer-stateless-function
    state = {
        data: [],
    }

    componentWillReceiveProps(nextProps) {
        const data = [];
        if (this.props.userWithSteps !== nextProps.userWithSteps) {
            const averageSteps = [];
            nextProps.userWithSteps.forEach((user) => {
                if (user.user.role === 'worker') {
                    user.arrayStepsByUser.forEach((nbStep) => {
                        const date = moment(nbStep.timestamp).format('YYYY-MM-DD');
                        if (!averageSteps[date]) {
                            averageSteps[date] = 0
                        }
                        averageSteps[date] += nbStep.step;
                    });
                }
            });

            // fill empty days
            for (let i = 0; i < 7; i++) {
                let day = moment().startOf('isoWeek').isoWeekday(1).add(i, 'day').format('YYYY-MM-DD');
                if (!averageSteps[day]) {
                    averageSteps[day] = 0
                }
            }

            const dates = Object.keys(averageSteps);
            dates.sort();
            dates.forEach((prop) => {
                data.push({
                    timestamp: moment(prop).format('DD/MM/YYYY'),
                    steps: averageSteps[prop],
                });
            });

            this.setState({data});
        }

        if (this.props.userWithSteps !== nextProps.userWithSteps) {
            this.props.cb(data);
        }
    }

    render() {
        const chart = (
            <ResponsiveContainer witdh="100%" height={400}>
                <BarChart
                    height={300}
                    data={this.state.data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}
                >
                    <XAxis dataKey="timestamp"/>
                    <YAxis tickFormatter={tick => tick.toLocaleString()}/>
                    <CartesianGrid strokeDasharray="0 3"/>
                    <Tooltip formatter={(value, name, props) => value.toLocaleString()}/>
                    <Legend/>
                    <Scatter data={this.state.data} line={<BarChart/>}/>

                    <Bar dataKey="steps" fill="#2196f3"/>
                </BarChart>

            </ResponsiveContainer>
        );

        return (
            <Wrapper>
                <h5>{this.props.title}</h5>
                {chart}
            </Wrapper>
        );
    }
}


TeamChart.propTypes = {
    title: React.PropTypes.string,
    userWithSteps: React.PropTypes.array,
    cb: React.PropTypes.func,
};

export default TeamChart;
