/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import WhiteBox from 'components/WhiteBox';
import IconInfo from 'components/IconInfo';
import TeamDashboard from 'containers/TeamDashboard';
import Row from 'components/Row';
import { DashBoard } from './styles';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

export default class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    nbTeams: 0,
    nbManagers: 0,
    nbWorkers: 0,
  }

  componentWillMount() {
    this.props.setPageTitle('Dashboard');
    this.props.AllUsersRequest(currentCompany.companyId);
    this.props.AllTeamsRequest(currentCompany.companyId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.teams) {
      this.setState({ nbTeams: nextProps.teams.length });
    }
    if (nextProps.users) {
      let nbWorkers = 0;
      let nbManagers = 0;

      nextProps.users.forEach((user) => {
        if (user.role === 'worker') nbWorkers += 1;
        else if (user.role === 'manager') nbManagers += 1;
      });

      this.setState({ nbManagers, nbWorkers });
    }
  }

  render() {
    return (
      <DashBoard>
        <Row>
          <WhiteBox
            s={6}
            m={6}
            l={3}
            node={
              <IconInfo
                icon={'group_work'}
                data={this.state.nbTeams}
                label={'Teams'}
                color={'purple'}
              />}
          />
          <WhiteBox
            s={6}
            m={6}
            l={3}
            node={
              <IconInfo
                icon={'supervisor_account'}
                data={this.state.nbManagers}
                label={'Managers'}
                color={'red'}
              />}
          />
          <WhiteBox
            s={6}
            m={6}
            l={3}
            node={
              <IconInfo
                icon={'person'}
                data={this.state.nbWorkers}
                label={'Workers'}
                color={'green'}
              />}
          />
          <WhiteBox
            s={6}
            m={6}
            l={3}
            node={
              <IconInfo
                icon={'flash_on'}
                data={1}
                label={'Actions'}
                color={'orange'}
              />}
          />
        </Row>

        <TeamDashboard />
      </DashBoard>
    );
  }
}

HomePage.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllUsersRequest: React.PropTypes.func,
  AllTeamsRequest: React.PropTypes.func,
};
