import { Row as MRow } from 'react-materialize';
import styled from 'styled-components';

export const DashBoard = styled.div`
  padding: 5px;
`;

export const Row = styled(MRow)`
@media (max-width: 992px) {
  & {
    margin: 0;
    }
  }
`;
