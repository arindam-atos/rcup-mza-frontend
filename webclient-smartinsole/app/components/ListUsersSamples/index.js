import React from "react";
import { Table, Button } from "react-materialize";
import Row from "components/Row";
import moment from "moment/moment";
import WhiteBox from "components/WhiteBox";
import DateInput from "components/DateInput";
import UserBatteryChart from "components/UserBatteryChart";
const currentCompany = JSON.parse(localStorage.getItem("currentCompany"));

class ListUsersSamples extends React.Component {
  //  eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    this.state = {
      selectedUserSamples: [],
      selectedUserMacAdd: "",
      from: moment()
        .subtract(7, "days")
        .format("YYYY-MM-DD"),
      to: moment().format("YYYY-MM-DD")
    };

    this.refreshData = this.refreshData.bind(this);
  }

  componentWillMount() {
    this.props.AllUsersRequest(currentCompany.companyId);
    this.props.setPageTitle("Liste des utilisateurs");
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.users !== nextProps.users) {
      // get last users sample from database
      const addresses = nextProps.users.map(user => user.macAddress);

      this.props.getSamples(
        currentCompany.companyId,
        addresses,
        moment().format("YYYY-MM-DD"),
        moment().format("YYYY-MM-DD")
      );

      // get selected user samples in from to interval from database
      this.props.getSamples(
        currentCompany.companyId,
        [`${nextProps.users[0].macAddress}`],
        this.state.from,
        this.state.to
      );

      this.setState({
        selectedUserMacAdd: nextProps.users[0].macAddress
      });
    }

    // after fetched users last samples, match samples with user to get the table of users
    if (this.props.lastSamples !== nextProps.lastSamples) {
      this.props.mapUsersAndSamples(this.props.users, nextProps.lastSamples);
    }

    // update the selected user samples to update the graph
    if (this.props.userSamples !== nextProps.userSamples) {
      this.setState({
        selectedUserSamples:
          nextProps.userSamples[this.state.selectedUserMacAdd]
      });
    }
  }

  setDate = (key, value) => {
    const date = moment(value).format("YYYY-MM-DD");
    if (key === "from") {
      this.props.getSamples(
        currentCompany.companyId,
        [`${this.state.selectedUserMacAdd}`],
        date,
        this.state.to
      );
      this.setState({ from: date });
    } else if (key === "to") {
      this.props.getSamples(
        currentCompany.companyId,
        [`${this.state.selectedUserMacAdd}`],
        this.state.from,
        date
      );
      this.setState({ to: date });
    }
  };

  refreshData() {
    const addresses = this.props.users.map(user => user.macAddress);

    this.props.getSamples(
      currentCompany.companyId,
      addresses,
      moment().format("YYYY-MM-DD"),
      moment().format("YYYY-MM-DD")
    );
  }

  selectUser(macAdd) {
    this.props.getSamples(
      currentCompany.companyId,
      [`${macAdd}`],
      this.state.from,
      this.state.to
    );
    this.setState({
      selectedUserMacAdd: macAdd
    });
  }

  render() {
    const { usersSamples } = this.props;

    const columns = [
      "id",
      "Email",
      "Adresse Mac",
      "Timestamp",
      "Steps",
      "RawSteps",
      "Batterie",
      "DeviceID",
      "Temperature"
    ];

    return (
      <Row>
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Button
              floating
              className="blue"
              waves="light"
              icon="autorenew"
              onClick={this.refreshData}
              style={{ float: "right" }}
              key="refresh"
            />,
            <Table bordered responsive key="users-list">
              <thead>
                <tr>
                  {columns.map((column, index) => (
                    <th data-field={column} key={`column-${index}`}>
                      {column}
                    </th>
                  ))}
                </tr>
              </thead>

              <tbody>
                {
                  //This is commented because, this functionality should be built from scratch
                  // It requires data to pull from Cloud SQL and then use it in BiqQuery.
                  // it is not in place at the moment and to be done.
                  /* eslint-disable jsx-a11y/no-static-element-interactions */
                  /*usersSamples.map((data, index) => (            
                
                  <tr
                    key={`tr-${index}`}
                    onClick={() =>
                      this.selectUser(usersSamples[index].user.macAddress)
                    }
                    style={{ cursor: "pointer" }}
                  >
                    <td>{usersSamples[index].user.id}</td>
                    <td>{usersSamples[index].user.email}</td>
                    <td>{usersSamples[index].user.macAddress}</td>
                    <td>{usersSamples[index].sample.timestamp}</td>
                    <td>{usersSamples[index].sample.steps}</td>
                    <td>{usersSamples[index].sample.rawSteps}</td>
                    <td>{usersSamples[index].sample.battery}</td>
                    <td>{usersSamples[index].sample.deviceID}</td>
                    <td>{usersSamples[index].sample.temperature}</td>
                  </tr>
                  ))*/
                }
              </tbody>
            </Table>
          ]}
        />
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Row key={"date-filter"}>
              <DateInput
                name={"from"}
                key={"from"}
                value={moment()
                  .subtract(7, "days")
                  .format("YYYY-MM-DD")}
                cb={this.setDate}
              />
              <DateInput
                name={"to"}
                key={"to"}
                value={moment().format("YYYY-MM-DD")}
                cb={this.setDate}
              />
            </Row>,
            <UserBatteryChart
              key={"chart"}
              userWithSteps={this.state.selectedUserSamples}
            />
          ]}
        />
        ,
      </Row>
    );
  }
}

ListUsersSamples.propTypes = {
  setPageTitle: React.PropTypes.func,
  users: React.PropTypes.array,
  AllUsersRequest: React.PropTypes.func,
  lastSamples: React.PropTypes.object,
  usersSamples: React.PropTypes.array,
  userSamples: React.PropTypes.object,
  getSamples: React.PropTypes.func,
  mapUsersAndSamples: React.PropTypes.func
};

export default ListUsersSamples;
