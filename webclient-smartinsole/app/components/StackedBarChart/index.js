import React from 'react';
import styled from 'styled-components';
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const Wrapper = styled.div`
  .recharts-legend-wrapper {
    left: 0 !important;
  }
`;

const StackedBarChart = (props) => {
  const ListBars = [];
  const obj = {};

  props.data.forEach((entry) => {
    Object.keys(entry).forEach((attr) => {
      if ((!Object.prototype.hasOwnProperty.call(obj, attr)) && attr !== 'name') {
        obj[attr] = attr;
        ListBars.push(
          <Bar dataKey={attr} key={`bar-${attr}`} stackId={'a'} fill={props.colorsByType[attr]} />
        );
      }
    });
  });

  return (
    <Wrapper>
      <ResponsiveContainer witdh="100%" height={350}>
        <BarChart
          width={600}
          height={300}
          data={props.data}
          margin={{ top: 20, right: 20, left: -40, bottom: 5 }}
          padding={{ left: 0 }}
          barGap={0}
          barCategoryGap={0}
        >
          <XAxis dataKey="name" />
          <YAxis />
          <CartesianGrid strokeDasharray="1 1" />
          <Tooltip />
          <Legend
            align="center"
            verticalAlign="bottom"
            layout="horizontal"
          />
          {
            ListBars.map((bar) =>
              bar
            )
          }

        </BarChart>
      </ResponsiveContainer>
    </Wrapper>
  );
};

StackedBarChart.propTypes = {
  data: React.PropTypes.array,
};

export default StackedBarChart;
