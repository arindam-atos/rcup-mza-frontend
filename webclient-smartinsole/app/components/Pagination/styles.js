import { Pagination } from 'react-materialize';
import styled from 'styled-components';

export const Wrapper = styled(Pagination)`
  text-align: center;
`;
