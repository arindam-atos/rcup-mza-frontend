import React from 'react';
import { Wrapper } from './styles';

function nbPage(datas) {
  const size = datas / 10;

  if (datas % 10 > 0) {
    return size + 1;
  }
  return size;
}

const Pagination = (props) =>
  <Wrapper
    items={nbPage(props.datas.length)}
    activePage={1}
    maxButtons={5}
    onSelect={props.cb}
  />;

Pagination.propTypes = {
  datas: React.PropTypes.array,
  cb: React.PropTypes.func,
};

export default Pagination;
