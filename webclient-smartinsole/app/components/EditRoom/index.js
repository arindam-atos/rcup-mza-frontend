import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import Row from 'components/Row';
import { Button, Icon, Collection, CollectionItem, Col } from 'react-materialize';
import { Wrapper } from './styles';

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class EditRoom extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    id: '',
    name: '',
    gateways: '',
    listHardnesses: [],
    hardnesses: {},
    hardnessesOptions: [],
  };

  componentWillMount() {
    const { roomId } = this.props.params;

    this.props.GetCompanyRequest(currentCompany.companyId);
    this.props.GetRoomRequest(currentCompany.companyId, roomId);
    this.props.setPageTitle('Edit room');
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.room !== nextProps.room) {
      const hardnesses = {};

      if (nextProps.company) {
        const listHardnesses =
        nextProps.company.hardnesses.split(';');

        listHardnesses.forEach((hardness) => {
          hardnesses[hardness] = 0;
        });
      }

      let gateways = '';

      nextProps.room.gateways.forEach((gtw, index) => {
        gateways = `${gateways} ${gtw.name}`;
        if (index < nextProps.room.gateways.length - 1) {
          gateways = `${gateways}, `;
        }
      });

      // Update hardness value if room has the attribute
      Object.keys(hardnesses).forEach((hardness) => {
        if (Object.prototype.hasOwnProperty.call(nextProps.room.hardnesses, hardness)) {
          hardnesses[hardness] = nextProps.room.hardnesses[hardness];
        }
      });

      this.setState({ gateways, hardnesses });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  setSelect = (key, value) => {
    this.setState((prevState) => ({
      hardnesses: {
        ...prevState.hardnesses,
        [key]: parseInt(value, 10),
      },
    }));
  }

  createInput = () => {
    const hardnessesOptions = [
      <option key={0} value={0}>0</option>,
      <option key={1} value={1}>1</option>,
      <option key={2} value={2}>2</option>,
      <option key={3} value={3}>3</option>,
      <option key={4} value={4}>4</option>,
      <option key={5} value={5}>5</option>,
    ];

    return (
      <Row key="hardnesses-input" id="hardness-input">
        <Collection header="List of hardnesses" key="inputs-collection">
          {
          Object.keys(this.state.hardnesses).map((el, i) =>
            <CollectionItem key={`collection-item-${i}`}>
              <Col s={4} m={4} l={5}>
                <span className="label">{el}</span>
              </Col>
              <Col s={6} m={4} l={4}>
                <Select
                  label="Select hardness value"
                  key={el}
                  name={el}
                  defaultVal={this.state.hardnesses[el]}
                  options={hardnessesOptions}
                  cb={this.setSelect}
                />
              </Col>
            </CollectionItem>
            )
          }
        </Collection>
      </Row>
    );
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { id, name, hardnesses } = this.props.room;

    const room = {};
    room.id = id;
    room.name = this.state.name || name;
    room.hardnesses = this.state.hardnesses || hardnesses;

    this.props.EditRoomRequest(currentCompany.companyId, room);
  }

  render() {
    const { name } = this.props.room;

    return (
      <Wrapper>
        <Form
          className="stayActive"
          node={[
            <Row
              key="form-inputs"
            >
              <Input
                key="name"
                label="Name"
                name="name"
                cb={this.setField}
                value={name || ''}
                s={12}
                m={6}
                l={6}
              />
            </Row>,
            <Row
              key="form-gateways"
            >
              <Input
                label="Gateways"
                name="gateways"
                key="list-gateways"
                disabled
                value={this.state.gateways || ''}
                s={12}
                m={6}
                l={6}
              />
            </Row>,
            this.createInput(),
            <Row
              key="form-submit"
            >
              <Button
                key="submit"
                id="submit"
                waves="light"
                onClick={this.handleSubmit}
                className="blue"
              >
                Modifier
                <Icon left>mode_edit</Icon>
              </Button>
            </Row>,
          ]}
        />
      </Wrapper>
    );
  }
}

EditRoom.propTypes = {
  setPageTitle: React.PropTypes.func,
  params: React.PropTypes.object.isRequired,
  GetRoomRequest: React.PropTypes.func.isRequired,
  EditRoomRequest: React.PropTypes.func.isRequired,
  room: React.PropTypes.object,
  GetCompanyRequest: React.PropTypes.func,
};

export default EditRoom;
