import React from 'react';
import Logo from 'public/logo/logo.png';
import Input from 'components/Input';
import CardPanel from 'containers/CardPanel';
import {browserHistory} from 'react-router';
import {Button, Col, Row, Wrapper} from './styles';

class Login extends React.Component {// eslint-disable-line react/prefer-stateless-function

    state = {
        email: '',
        pwd: '',
    }

    setField = (key, value) => {
        this.setState({[key]: value});
    }

    enterPressed = (e) => {
        const keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            this.props.LoginRequest(this.state.email, this.state.pwd);
        }
    }

    handleSubmit = () => {
        this.props.LoginRequest(this.state.email, this.state.pwd);
    }

    render() {
        return (
            /* eslint-disable jsx-a11y/no-static-element-interactions */
            <Wrapper>
                <div
                    className="form"
                    onKeyPress={this.enterPressed}
                >

                    <div className="logo">
                        <img src={Logo} alt="logo"/>
                    </div>
                    <CardPanel/>

                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                        >
                            <Input
                                label="Identifiant"
                                cb={this.setField}
                                name="email"
                                type="email"
                                className="validate"
                                icon="account_circle"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                        >
                            <Input
                                label="Mot de passe"
                                cb={this.setField}
                                name="pwd"
                                type="password"
                                className={'validate'}
                                icon="vpn_key"
                            />
                        </Col>
                    </Row>

                    <Row
                        style={{
                            marginTop: '30px',
                        }}
                    >
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <Button
                                waves="light"
                                type="submit"
                                onClick={this.handleSubmit}
                                className="submit"
                            >
                                Se connecter
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col
                            s={10}
                            m={10}
                            l={10}
                            offset="s1 m1 l1"
                            className="s1 m1 l1"
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <Button
                                onClick={() => browserHistory.push('/reset-pwd')}
                                href="/reset-pwd"
                                className="forgotten-pwd"
                            >
                                Mot de passe oublié ?
                            </Button>
                        </Col>
                    </Row>
                </div>
            </Wrapper>
            /* eslint-enable jsx-a11y/no-static-element-interactions */
        );
    }
}

Login.propTypes = {
    LoginRequest: React.PropTypes.func,
};

export default Login;
