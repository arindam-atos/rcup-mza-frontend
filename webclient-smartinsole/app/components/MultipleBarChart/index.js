import React from 'react';
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const MultipleBarChart = (props) =>
  <ResponsiveContainer witdh="100%" height={350}>
    <BarChart
      width={600}
      height={300}
      data={props.data}
      margin={{ top: 20, right: 20, left: -30, bottom: 5 }}
      padding={{ left: 0 }}
      barGap={0}
      barCategoryGap={0}
    >
      <XAxis dataKey="name" />
      <YAxis type="number" domain={[0, 100]} />
      <CartesianGrid strokeDasharray="1 1" />
      <Tooltip />
      <Legend
        align="center"
        verticalAlign="bottom"
        layout="horizontal"
      />
      <Bar name="Temps" dataKey="duration" label fill="#0088FE" />
      <Bar name="Pas" dataKey="steps" label fill="#00C49F" />

    </BarChart>
  </ResponsiveContainer>;

MultipleBarChart.propTypes = {
  data: React.PropTypes.array,
};

export default MultipleBarChart;
