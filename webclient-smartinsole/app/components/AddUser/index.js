/*
 *  @Project    WebClient WorkerSole
 *
 *  @File       AddUser.js
 *
 *  @summary    View to add user
 *
 *  @author     PHONG Stéphane
 *
 */

import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import { Button, Icon } from 'react-materialize';
import Row from 'components/Row';
import Col from 'components/Col';
import { Wrapper, Colon } from './styles';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));
const loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

class AddUser extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    email: '',
    password: '',
    macAddress: '',
    role: 'worker',
    firstname: '',
    lastname: '',
    height: '',
    weight: '',
    shoeSize: '',
    age: '',
  };

  componentWillMount = () => {
    this.props.setPageTitle('Add user');
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const user = this.state;
    const { macAdd1, macAdd2, macAdd3, macAdd4, macAdd5, macAdd6 } = this.state;

    user.macAddress = `${macAdd1}:${macAdd2}:${macAdd3}:${macAdd4}:${macAdd5}:${macAdd6}`;
    this.props.AddUserRequest(currentCompany.companyId, user);
  }

  render() {
    const options = [
      <option key="1" value="worker">Worker</option>,
    ];

    if (loggedUser.role === 'director') {
      options.push(
        <option key="2" value="manager">Manager</option>
      );
    }

    if (loggedUser.role === 'admin') {
      options.push(
        <option key="2" value="manager">Manager</option>,
        <option key="3" value="director">Director</option>,
        <option key="4" value="admin">Administrator</option>,
      );
    }

    return (
      <Wrapper>
        <Form
          node={[
            <Row
              key="user-add"
            >
              <Input
                key="email"
                label="Email"
                name="email"
                type="email"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />

              <Col
                s={12}
                m={6}
                l={6}
                className="input-mac-address"
              >
                <label className="active" htmlFor="add">
                  Mac Address
                </label>
                <div id="add">
                  <Input
                    key={'macAdd1'}
                    name={'macAdd1'}
                    cb={this.setField}
                    maxLength={2}
                    s={2}
                    m={2}
                    l={2}
                  />
                </div>
                <Colon>:</Colon>
                <Input
                  key={'macAdd2'}
                  name={'macAdd2'}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd3'}
                  name={'macAdd3'}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd4'}
                  name={'macAdd4'}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd5'}
                  name={'macAdd5'}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
                <Colon>:</Colon>
                <Input
                  key={'macAdd6'}
                  name={'macAdd6'}
                  cb={this.setField}
                  maxLength={2}
                  s={2}
                  m={2}
                  l={2}
                />
              </Col>

              <Input
                key="password"
                label="Password"
                name="password"
                type="password"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Select
                label="Select user's role"
                key="role"
                name="role"
                multiple={false}
                defaultVal={this.state.role}
                options={options}
                cb={this.setField}
              />
              <Input
                key="firstname"
                label="Firstname"
                name="firstname"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="lastname"
                label="Lastname"
                name="lastname"
                cb={this.setField}
                s={12}
                m={6}
                l={6}
              />
              <Input
                key="height"
                label="Height in cm"
                name="height"
                cb={this.setField}
                s={6}
                m={6}
                l={6}
              />
              <Input
                key="weight"
                label="Weight in kg"
                name="weight"
                cb={this.setField}
                s={6}
                m={6}
                l={6}
              />
              <Input
                key="shoeSize"
                label="Shoes size"
                name="shoeSize"
                cb={this.setField}
                s={6}
                m={6}
                l={6}
              />
              <Input
                key="age"
                label="Age"
                name="age"
                cb={this.setField}
                s={6}
                m={6}
                l={6}
              />
            </Row>,
            <Row
              key="form-submit"
            >
              <Button
                key="submit"
                id="submit"
                waves="light"
                onClick={this.handleSubmit}
                className="blue"
              >
                Add
                <Icon left>create</Icon>
              </Button>
            </Row>,
          ]}
        />
      </Wrapper>
    );
  }
}

AddUser.propTypes = {
  setPageTitle: React.PropTypes.func,
  AddUserRequest: React.PropTypes.func.isRequired,
};

export default AddUser;
