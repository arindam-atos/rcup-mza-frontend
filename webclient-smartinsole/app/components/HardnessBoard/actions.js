import moment from "moment/moment";

// This function is called for each index of filtered array of users samples indexed by time. It returns the time spent near by gateway
// format of the result:
// samplesByGtw = {
//  MOBILITY: {duration: -2899, steps: 170}
//  RB30002: {duration: 1795, steps: 0}
//  RB30007: {duration: 1553, steps: 0}
//  RB30008: {duration: 1178, steps: 0}
//  RB30010: {duration: 170, steps: 0}
// }
export function getSamplesByGtw(samples) {
  const samplesByGtw = {};
  let firstSample = samples[0];
  let prevSample = samples[0];
  const mobility = {
    duration: 0,
    steps: 0
  };

  // Calculate the mobility duration for the first sample of the list of samples indexed by time and then proceed to calcul the rest of duration of all samples

  samples.forEach((sample, index) => {
    if (!Object.prototype.hasOwnProperty.call(samplesByGtw, sample.deviceID)) {
      samplesByGtw[sample.deviceID] = {
        duration: 0,
        steps: 0
      };
    }

    if (prevSample.deviceID !== sample.deviceID) {
      const prevDate = moment(prevSample.timestamp).format("HH:mm:ss");
      const firstDate = moment(firstSample.timestamp).format("HH:mm:ss");
      const currDate = moment(sample.timestamp).format("HH:mm:ss");

      const prevDuration = moment.duration(prevDate, "seconds").asSeconds();
      const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
      const curDuration = moment.duration(currDate, "seconds").asSeconds();

      samplesByGtw[prevSample.deviceID].duration +=
        prevDuration - firstDuration;

      samplesByGtw[prevSample.deviceID].steps +=
        prevSample.Steps - firstSample.steps;

      if (prevSample.macAddress === sample.macAddress) {
        mobility.duration += curDuration - prevDuration;

        mobility.steps += sample.steps - prevSample.steps;
      }
      firstSample = sample;
    }

    if (index === samples.length - 1) {
      const firstDate = moment(firstSample.timestamp).format("HH:mm:ss");
      const currDate = moment(sample.timestamp).format("HH:mm:ss");
      const firstDuration = moment.duration(firstDate, "seconds").asSeconds();
      const curDuration = moment.duration(currDate, "seconds").asSeconds();

      samplesByGtw[sample.deviceID].duration += curDuration - firstDuration;
      samplesByGtw[prevSample.deviceID].steps +=
        sample.steps - firstSample.steps;
    }
    prevSample = sample;
  });
  samplesByGtw.MOBILITY = mobility;
  return samplesByGtw;
}

export const getSamplesByZone = (crossingsGtwData, gateways) => {
  const samplesByZone = {};
  samplesByZone.MOBILITY = crossingsGtwData.MOBILITY;

  gateways.forEach(gateway => {
    if (!Object.prototype.hasOwnProperty.call(samplesByZone, gateway.room)) {
      samplesByZone[gateway.room] = {
        duration: 0,
        steps: 0
      };
    }
  });

  Object.keys(crossingsGtwData).forEach(data => {
    gateways.forEach(gateway => {
      if (data === gateway.name) {
        samplesByZone[gateway.room].duration += crossingsGtwData[data].duration;
        samplesByZone[gateway.room].steps += crossingsGtwData[data].steps;
      }
    });
  });
  return samplesByZone;
};
