import styled from 'styled-components';

export const Wrapper = styled.div`
  .container-speedometer {
    text-align: center;
  }

  .speedometer {
    padding-top: 30px;
  }
`;
