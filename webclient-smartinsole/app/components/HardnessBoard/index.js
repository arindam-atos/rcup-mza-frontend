import React from 'react';
import { Row, Collection, CollectionItem } from 'react-materialize';
import Select from 'components/Select';
import WhiteBox from 'components/WhiteBox';
import TeamUsersBox from 'components/TeamUsersBox';
import DateInput from 'components/DateInput';
import ReactSpeedometer from 'react-d3-speedometer';
import moment from 'moment/moment';
import { getSamplesByGtw, getSamplesByZone } from './actions';
import { Wrapper } from './styles';

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class HardnessBoard extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  state = {
    teamUsers: [],
    teams: [],
    teamsOptions: [],
    teamId: 0,
    workHours: 7,
    usersSamples: [],
    roomHardnesses: {},
    userHardnesses: [],
    indexTeamHardness: 0,
    day: moment().format('YYYY-MM-DD'),
    from: '09:00:00',
    to: '16:00:00',
  };

  componentWillMount() {
    this.props.setPageTitle('Hardness Board');
    this.props.AllTeamsRequest(currentCompany.companyId);
    this.props.AllGatewaysRequest(currentCompany.companyId);
    this.props.AllRoomsRequest(currentCompany.companyId);
  }

  componentWillReceiveProps(nextProps) {
    const teamsOptions = [];
    const users = [];

    if (nextProps.teams.length > 0 && this.props.teams !== nextProps.teams) {
      nextProps.teams.forEach((team) => {
        teamsOptions.push(
          <option key={team.id} value={team.id}>{`${team.name}`}</option>
        );
      });
      this.setState({
        teamsOptions,
        teamId: nextProps.teams[0].id,
        teams: nextProps.teams,
      });
    }


    if (this.props.usersTeam !== nextProps.usersTeam) {
      const addresses = [];
      nextProps.usersTeam.forEach((user) => {
        if (user.role === 'worker') {
          users.push({
            email: user.email,
            macAddress: user.macAddress,
          });
          addresses.push(user.macAddress);
        }
      });
      this.props.getSamplesByHours(currentCompany.companyId, addresses, this.state.day, this.state.from, this.state.to);
      this.setState({ teamUsers: users, addresses });
    }

    if (this.props.rooms !== nextProps.rooms) {
      const rooms = {};
      nextProps.rooms.forEach((room) => {
        rooms[room.name] = 0;
        Object.keys(room.hardnesses).forEach((hardness) => {
          rooms[room.name] += parseInt(room.hardnesses[hardness], 10);
        });
      });
      this.setState({ roomHardnesses: rooms });
    }

    if (this.props.teamUsersSamples !== nextProps.teamUsersSamples) {
      const timeSpentByUserByGtw = [];
      // get the time spent by gateway for each user
      nextProps.teamUsersSamples.forEach((sample) => {
        if (sample.length > 0) {
          timeSpentByUserByGtw.push(getSamplesByGtw(sample));
        }
      });

      const timeSpentByZone = [];
      timeSpentByUserByGtw.forEach((sample) => {
        timeSpentByZone.push(getSamplesByZone(sample, nextProps.gateways));
      });
      this.setState({ usersSamples: timeSpentByZone });

      const userHardnesses = [];
      let minIndex = 0;
      let maxIndex = 0;
      let nbHardness = 0;
      timeSpentByZone.forEach((userSample, index) => {
        let res = 0;
        Object.keys(userSample).forEach((hardness) => {
          if (hardness !== 'MOBILITY') {
            if (minIndex > this.state.roomHardnesses[hardness]) {
              minIndex = parseInt(this.state.roomHardnesses[hardness], 10);
            }
            if (maxIndex < this.state.roomHardnesses[hardness]) {
              maxIndex = parseInt(this.state.roomHardnesses[hardness], 10);
            }
            res += parseInt(userSample[hardness].duration, 10) * parseInt(this.state.roomHardnesses[hardness], 10);
          }
          if (index === 0) nbHardness += 1;
        });
        userHardnesses.push(Math.round(res / 3600));
      });

      minIndex *= this.state.workHours;
      maxIndex = nbHardness * 5 * this.state.workHours;

      const indexUsersHardness = [];
      userHardnesses.forEach((user) => {
        const res = 100 - (((user - minIndex) / (maxIndex - minIndex)) * 100);
        indexUsersHardness.push(Math.round(res, 10));
      });

      let indexTeamHardness = 0;
      indexUsersHardness.forEach((index) => {
        indexTeamHardness += index;
      });
      indexTeamHardness /= indexUsersHardness.length;
      this.setState({
        indexTeamHardness,
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.teamId !== nextState.teamId) {
      this.props.getUsersByTeam(currentCompany.companyId, nextState.teamId);
      return false;
    }
    if (this.state.day !== nextState.day) {
      this.props.getSamplesByHours(currentCompany.companyId, this.state.addresses, nextState.day, this.state.from, this.state.to);
      return false;
    }
    return true;
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  render() {
    return (
      <Wrapper>
        <Row>
          <WhiteBox
            s={12}
            m={12}
            l={12}
            node={[
              <h5
                key="title"
              >
                Team dashboard
              </h5>,
              <Row key="dateinput-row">
                <DateInput
                  key="day"
                  label={'Day'}
                  name={'day'}
                  value={moment().format('YYYY-MM-DD')}
                  s={12}
                  m={4}
                  l={4}
                  cb={this.setField}
                />
              </Row>,
              <Select
                style={{ marginTop: '30px', marginBottom: '20px' }}
                label={'Select a team'}
                key={'teamId'}
                name={'teamId'}
                multiple={false}
                defaultVal={this.state.teamId || ''}
                options={this.state.teamsOptions || []}
                cb={this.setField}
                s={6}
                m={6}
                l={6}
              />,
              <TeamUsersBox
                key={'users-team'}
                teamId={this.state.teamId}
                setTeamId={this.setField}
                teamsOptions={this.state.teamsOptions}
                users={this.state.teamUsers}
                columns={['Email', 'Mac Address']}
              />,
            ]}
          />
        </Row>
        <Row>
          <WhiteBox
            s={12}
            m={6}
            l={6}
            node={
              <Collection key="collection" header="Hardness indexes">
                { Object.keys(this.state.roomHardnesses).map((room) =>
                  <CollectionItem key={`collection-${room}`}>
                    { room }: { this.state.roomHardnesses[room] }
                  </CollectionItem>
                )}
              </Collection>
            }
          />
          <WhiteBox
            s={12}
            m={6}
            l={6}
            node={
              <div
                className="container-speedometer"
                key="speedometer-chart"
              >
                <h5 key="hardness-title">Hardness Index</h5>
                <ReactSpeedometer
                  key="speed-chart"
                  height={250}
                  value={this.state.indexTeamHardness}
                  needleColor="steelblue"
                  needleTransitionDuration={4000}
                  needleTransition="easeElastic"
                  minValue={0}
                  maxValue={100}
                  segments={3}
                />
              </div>
            }
          />
        </Row>
      </Wrapper>
    );
  }
}

HardnessBoard.propTypes = {
  setPageTitle: React.PropTypes.func,
  usersTeam: React.PropTypes.array,
  getUsersByTeam: React.PropTypes.func,
  teams: React.PropTypes.array,
  AllTeamsRequest: React.PropTypes.func,
  AllGatewaysRequest: React.PropTypes.func,
  teamUsersSamples: React.PropTypes.array,
  getSamplesByHours: React.PropTypes.func,
  AllRoomsRequest: React.PropTypes.func,
  rooms: React.PropTypes.array,
};

export default HardnessBoard;
