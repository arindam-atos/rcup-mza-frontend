import React from 'react';
import HeatmapLayer from 'react-leaflet-heatmap-layer';

export default class GenfencingHeatmap extends React.Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    addressPoints: [],
    minIntensity: 600,
    incrementIntensity: 30,
    maxIntensity: 3000,
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.gateways !== nextProps.gateways) {
      const addressPoints = [];
      let point = [];
      nextProps.gateways.forEach((gateway) => {
        point.push(gateway.longitude);
        point.push(gateway.latitude);
        point.push(this.state.minIntensity);
        point.push(gateway.name);
        addressPoints.push(point);
        point = [];
      });
      this.setState({ addressPoints });
    }

    if (this.props.samples !== nextProps.samples) {
      const addressPoints = [];
      let point = [];
      this.props.gateways.forEach((gateway) => {
        point.push(gateway.longitude);
        point.push(gateway.latitude);
        point.push(this.state.minIntensity);
        point.push(gateway.name);
        addressPoints.push(point);
        point = [];
      });
      this.setState({ addressPoints });
    }

    if (this.props.secondInterval !== nextProps.secondInterval) {
      this.setState({ incrementIntensity: 30 }, () => {
        const intensifyCoef = this.state.maxIntensity / nextProps.secondInterval;
        const incrementIntensity = this.state.incrementIntensity * intensifyCoef;
        this.setState({ incrementIntensity });
      });
    }

    if (nextProps.gtw && this.props.gtw !== nextProps.gtw) {
      this.state.addressPoints.forEach((point, index) => {
        if (point[3] === nextProps.gtw.name) {
          this.state.addressPoints[index][2] += this.state.incrementIntensity;
        }
      });
    }
  }

  render() {
    return (
      <HeatmapLayer
        fitBoundsOnLoad
        fitBoundsOnUpdate
        points={this.state.addressPoints}
        longitudeExtractor={(m) => m[0]}
        latitudeExtractor={(m) => m[1]}
        intensityExtractor={(m) => parseFloat(m[2])}
        blur={20}
        radius={25}
        max={this.state.maxIntensity}
        style={{ display: 'none', visibility: 'hidden' }}
      />
    );
  }
}

GenfencingHeatmap.propTypes = {
  gtw: React.PropTypes.object,
  gateways: React.PropTypes.array,
  secondInterval: React.PropTypes.number,
  samples: React.PropTypes.array,
};
