import React from 'react';
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import moment from 'moment/moment';

class UserBatteryChart extends React.PureComponent {//  eslint-disable-line react/prefer-stateless-function
  state = {
    data: [],
  }

  componentWillReceiveProps(nextProps) {
    const data = [];

    if (this.props.userWithSteps !== nextProps.userWithSteps) {
      nextProps.userWithSteps.forEach((sample) => {
        data.push({
          timestamp: moment(sample.Timestamp).format('DD/MM/YYYY'),
          battery: sample.Battery,
        });
      });
      this.setState({ data });
    }
  }

  render() {
    const chart = (
      <ResponsiveContainer witdh="100%" height={300}>
        <LineChart
          width={600}
          height={300}
          data={this.state.data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis dataKey="timestamp" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="battery" stroke="#8884d8" activeDot={{ r: 8 }} />

        </LineChart>
      </ResponsiveContainer>
    );

    return (
      <div>
        {chart}
      </div>
    );
  }
}


UserBatteryChart.propTypes = {
  userWithSteps: React.PropTypes.array,
};

export default UserBatteryChart;
