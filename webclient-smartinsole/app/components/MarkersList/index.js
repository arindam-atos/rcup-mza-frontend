import React from 'react';
import { Marker, Tooltip } from 'react-leaflet';

export default class MarkersList extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    markers: [],
  };

  componentWillMount() {
    if (this.props.markers) {
      this.setState({ markers: this.props.markers });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.markers !== nextProps.markers) {
      this.setState({ markers: nextProps.markers });
    }
  }

  render() {
    const markersList = this.state.markers.map((marker) =>
      <Marker
        key={marker.name}
        position={[marker.latitude, marker.longitude]}
        opacity={1.0}
      >
        <Tooltip>
          <div>
            <div>{marker.name}</div>
            <div>{marker.room}</div>
          </div>
        </Tooltip>
      </Marker>
    );

    return (
      <div>
        { markersList }
      </div>
    );
  }
}

MarkersList.propTypes = {
  markers: React.PropTypes.array,
};
