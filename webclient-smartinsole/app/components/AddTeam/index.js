import React from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import Select from 'components/Select';
import { Button, Icon } from 'react-materialize';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class AddTeam extends React.Component {// eslint-disable-line react/prefer-stateless-function

  state = {
    name: '',
    managersId: [],
    workersId: [],
    managersOptions: [],
    workersOptions: [],
  };

  componentWillMount() {
    this.props.setPageTitle('Add team');
    this.props.AllUsersRequest(currentCompany.companyId);
  }

  componentWillReceiveProps(nextProps) {
    const managersOptions = [
      <option key={0} disabled value="">{'Select'}</option>,
    ];
    const workersOptions = [
      <option key={0} disabled value="">{'Select'}</option>,
    ];
    const { users } = nextProps;
    let i = 1;

    if (users) {
      users.forEach((user) => {
        if (user.role === 'manager') {
          managersOptions.push(<option key={i} value={user.id}>{`${user.firstname} ${user.lastname}`}</option>);
        } else if (user.role === 'worker') {
          workersOptions.push(<option key={i} value={user.id}>{`${user.firstname} ${user.lastname}`}</option>);
        }
        i += 1;
      });

      this.setState({
        managersOptions,
        workersOptions,
      });
    }
  }

  setField = (key, value) => {
    this.setState({ [key]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const team = {
      name: this.state.name,
      managersId: this.state.managersId,
      workersId: this.state.workersId,
    };
    this.props.AddTeamRequest(currentCompany.companyId, team);
  }

  render() {
    return (
      <div>
        <Form
          node={[
            <Input
              key="name"
              label="Name"
              name="name"
              cb={this.setField}
            />,
            <Select
              label="Select managers"
              key="managersId"
              name="managersId"
              multiple
              defaultValue=""
              options={this.state.managersOptions}
              cb={this.setField}
            />,
            <Select
              label="Select workers"
              key="workersId"
              name="workersId"
              multiple
              defaultValue=""
              options={this.state.workersOptions}
              cb={this.setField}
            />,
            <Button
              key="submit"
              id="submit"
              waves="light"
              onClick={this.handleSubmit}
              className="blue"
            >
              Add
              <Icon left>create</Icon>
            </Button>,
          ]}
        />
      </div>
    );
  }
}

AddTeam.propTypes = {
  setPageTitle: React.PropTypes.func,
  users: React.PropTypes.array.isRequired,
  AllUsersRequest: React.PropTypes.func.isRequired,
  AddTeamRequest: React.PropTypes.func.isRequired,
};

export default AddTeam;
