import React from 'react';
import TeamUsersBox from 'components/TeamUsersBox';
import Row from 'components/Row';
import WhiteBox from 'components/WhiteBox';
import TeamChart from 'components/TeamChart';
import Select from 'components/Select';
import TeamRoomView from 'containers/TeamRoomView';
import TeamZonesChart from 'containers/TeamZonesChart';
import TeamActivity from 'components/TeamActivity';
import InfoBox from 'components/InfoBox';

import moment from 'moment/moment';

const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class TeamDashboard extends React.Component {//  eslint-disable-line react/prefer-stateless-function

  state = {
    teamUsers: [],
    teams: [],
    teamsOptions: [],
    teamId: 0,
    teamAverageSteps: 0,
    maxAverageDay: {
      steps: 0,
      rawSteps: 0,
      timestamp: moment().format('DD/MM/YYYY'),
    },
    from: moment().startOf('week').add(1, 'day').format('YYYY-MM-DD'), // return monday of this week
    to: moment().format('YYYY-MM-DD'),

    // from: moment('2018-05-25').format('YYYY-MM-DD'), // return monday of this week
    // to: moment().format('YYYY-MM-DD'), // return monday of

  };

  componentWillMount() {
    this.props.AllTeamsRequest(currentCompany.companyId);
  }

  componentDidMount() {
    // setInterval(() => {
    //   this.props.getUsersAndSteps(currentCompany.companyId, this.state.teamId, this.state.from, this.state.to);
    // }, 60 * 1000);
  }

  componentWillReceiveProps(nextProps) {
    const teamsOptions = [];
    const users = [];

    if (nextProps.teams.length > 0 && this.props.teams !== nextProps.teams) {
      nextProps.teams.forEach((team) => {
        teamsOptions.push(
          <option key={team.id} value={team.id}>{`${team.name}`}</option>
        );
      });
      this.setState({
        teamsOptions,
        teamId: nextProps.teams[0].id || '',
        teams: nextProps.teams,
      });
    }


    if (this.props.usersSteps !== nextProps.usersSteps) {
      nextProps.usersSteps.forEach((user) => {
        if (user.user.role === 'worker') {
          users.push({
            email: user.user.email,
            macAddress: user.user.macAddress,
          });
        }
      });
      this.setState({ teamUsers: users });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.teamId !== nextState.teamId) {
      this.props.getUsersAndSteps(currentCompany.companyId, nextState.teamId, this.state.from, this.state.to);
      return false;
    }
    return true;
  }


  setField = (key, value) => {
    this.setState({ teamId: value });
  }

  setDate = (from, to) => {
    const fromDate = moment(from).format('YYYY-MM-DD');
    const toDate = moment(to).format('YYYY-MM-DD');

    this.props.getUsersAndSteps(currentCompany.companyId, this.state.teamId, fromDate, toDate);

    this.setState({
      from: fromDate,
      to: toDate,
    });
  }

  setAverageSteps = (averageSteps) => {
    let teamAverageSteps = 0;
    const maxAverageDay = {
      steps: 0,
      timestamp: moment().format('DD/MM/YY'),
    };

    averageSteps.forEach((stepsByDay) => {
      if (maxAverageDay.steps < stepsByDay.steps) {
        maxAverageDay.steps = stepsByDay.steps;
        maxAverageDay.timestamp = stepsByDay.timestamp;
      }
      teamAverageSteps += stepsByDay.steps;
    });
    teamAverageSteps /= averageSteps.length;
    if (isNaN(teamAverageSteps)) teamAverageSteps = 0;
    if (isNaN(maxAverageDay.steps)) maxAverageDay.steps = 0;

    teamAverageSteps = Math.round(teamAverageSteps);
    maxAverageDay.steps = Math.round(maxAverageDay.steps);

    this.setState({ teamAverageSteps });
    this.setState({ maxAverageDay });
  }

  render() {
    return (
      <Row>
        <Row>
          <WhiteBox
            s={12}
            m={12}
            l={6}
            node={[
              <h5
                key="title"
              >
                Team dashboard
              </h5>,
              <Select
                style={{ marginTop: '30px', marginBottom: '20px' }}
                label={'Select a team'}
                key={'team'}
                name={'team'}
                multiple={false}
                defaultVal={this.state.teamId || ''}
                options={this.state.teamsOptions || []}
                cb={this.setField}
                s={6}
                m={6}
                l={6}
              />,
              <TeamUsersBox
                key={'users-team'}
                teamId={this.state.teamId}
                setTeamId={this.setField}
                teamsOptions={this.state.teamsOptions}
                users={this.state.teamUsers}
                columns={['email', 'Adresse Mac']}
              />,
            ]}
          />

          <WhiteBox
            s={12}
            m={12}
            l={6}
            node={
              <InfoBox
                key={'activity'}
                label={'Team activity'}
                date={` ${this.state.to}`}
                node={<TeamActivity
                  key="team-activity-box"
                  usersSteps={this.props.usersSteps}
                />}
              />
            }
          />

          <WhiteBox
            s={12}
            m={12}
            l={12}
            node={[
              <h5
                key="title"
              >
                Workers by zone
              </h5>,
              <TeamRoomView
                key="team-room"
                teamUsers={this.state.teamUsers}
              />,
            ]}
          />
          <WhiteBox
            s={12}
            m={12}
            l={12}
            node={[
              <h5
                key="chart-title"
              >
                Activity by zone
              </h5>,
              <TeamZonesChart
                key="3d-chart"
                teamUsers={this.state.teamUsers}
              />,
            ]}
          />
        </Row>

        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={
            <InfoBox
              key={'activity'}
              label={'Week Steps'}
              date={` ${this.state.to}`}
              node={<TeamChart
                key={'chart'}
                userWithSteps={this.props.usersSteps}
                cb={this.setAverageSteps}
              />}
            />
          }
        />

      </Row>
    );
  }
}

TeamDashboard.propTypes = {
  getUsersAndSteps: React.PropTypes.func,
  usersSteps: React.PropTypes.array,
  teams: React.PropTypes.array,
  AllTeamsRequest: React.PropTypes.func,
};

export default TeamDashboard;
