import React from "react";
import {Button, Icon} from "react-materialize";
import WhiteBox from "components/WhiteBox";
import FileSaver from 'file-saver';
import moment from 'moment/moment';

export default class GeofencingSamplesDL extends React.Component {
    // eslint-disable-line react/prefer-stateless-function

    state = {
        data: []
    };

    componentWillReceiveProps = nextProps => {

        const data = [];

        if (this.props.samples !== nextProps.samples) {
            nextProps.samples.forEach((sample) => {
                data.push({
                    Date: sample.Timestamp,
                    Semelle: sample.MacAddress,
                    Gateway: sample.DeviceID,
                    Pas: sample.Steps,
                    RSSI: sample.RSSI,
                    RawSteps: sample.RawSteps,
                });
            });
            this.setState({data});
        }
    };

    onDownload() {

        const urlArray = process.env.NODE_ENV.split("#");
        const historyUrl = "http://" + urlArray[1];

        const fromTime = moment(this.props.day + ' ' + this.props.from).utc().format('HH:mm');
        const toTime = moment(this.props.day + ' ' + this.props.to).utc().format('HH:mm');

        let url = historyUrl + "/historyByDayExport?day=" + this.props.day + "&from=" + fromTime + "&to=" + toTime + "&companyId=1&addresses=" + this.props.macaddr;

        FileSaver.saveAs(url, "rcup.xls");
    }


    render() {
        let content = "";

        if (this.state.data.length > 0) {
            content = (
                <div>
                    <p style={{margin: 0}}>
                        Download data between {this.props.from} and {this.props.to} <br/>{" "}
                        of {this.props.day}
                    </p>
                    <div
                        style={{
                            display: "inline-block",
                            marginTop: "15px"
                        }}
                    >
                        <Button onClick={() => this.onDownload()}>
                            Download
                            <Icon right small>
                                file_download
                            </Icon>
                        </Button>
                    </div>
                </div>
            );
        } else {
            content = <div style={{paddingTop: "40px"}}>No data available. </div>;
        }

        return (
            <WhiteBox
                s={12}
                m={12}
                l={4}
                node={[
                    <div
                        key={"sampleDL"}
                        style={{height: "114px", textAlign: "center"}}
                    >
                        {content}
                    </div>
                ]}
            />
        );
    }
}

GeofencingSamplesDL.propTypes = {
    samples: React.PropTypes.array,
    from: React.PropTypes.string,
    to: React.PropTypes.string,
    day: React.PropTypes.string
};
