import { Col as MCol, Icon as MIcon } from 'react-materialize';
import styled from 'styled-components';

const colors = {
  brown: '#795548',
  blue: '#2196f3',
  green: '#4caf50',
  red: '#f44336',
  orange: '#ff9800',
  purple: '#9c27b0',
};

export const Wrapper = styled.div`
  .datetime {
    color: #777;
    border-top: 1px solid #777;

    padding-top: 20px;
    i {
      float: left;
    }

    span {
      float: right;
    }
  }

`;

export const Col = styled(MCol)`
  div {
    margin: 10px 0;
  }

  p {
    text-align: right;
    line-height: 1.5;
    margin-top: 0;
    margin-bottom: 0;
  }

  .info-label {
    font-size: 18px;
  }

  span {
    font-size: 15px;
  }

  i {
    font-weight: 700;
    line-height: 85px;
    font-size: 3.4rem;
  }
`;

export const Icon = styled(MIcon)`
  color: ${(props) => colors[props.color]};
`;
