import React from 'react';
import Row from 'components/Row';
import { Icon, Wrapper } from './styles';

export default class InfoBox extends React.PureComponent {// eslint-disable-line react/prefer-stateless-function

  state = {
    date: '',
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.date !== nextProps.date) {
      this.setState({ date: nextProps.date });
    }
  }

  render() {
    return (
      <Wrapper>
        <Row>
          <h5 className="info-label">
            {this.props.label}
          </h5>
        </Row>
        <Row>
          { this.props.node }
        </Row>
        { this.props.date ?
          <Row className="datetime">
            <div>
              <Icon>event</Icon>
              <span>{this.props.date}</span>
            </div>
          </Row> : ''
        }
      </Wrapper>);
  }
}

InfoBox.propTypes = {
  label: React.PropTypes.string,
  node: React.PropTypes.node,
  date: React.PropTypes.string,
};
