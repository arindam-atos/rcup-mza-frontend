import React from 'react';
import Grid from 'components/Grid';
import { Row } from 'react-materialize';
import WhiteBox from 'components/WhiteBox';
const currentCompany = JSON.parse(localStorage.getItem('currentCompany'));

class ListRooms extends React.Component {//  eslint-disable-line react/prefer-stateless-function

  state = {
    rooms: [],
  };

  componentWillMount() {
    this.props.AllRoomsRequest(currentCompany.companyId);
    this.props.setPageTitle('Rooms list');
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.rooms !== nextProps.rooms) {
      const listRooms = [];

      nextProps.rooms.forEach((room) => {
        let gateways = '';
        room.gateways.forEach((gtw, index) => {
          gateways = `${gateways} ${gtw.name}`;
          if (index < room.gateways.length - 1) {
            gateways = `${gateways}, `;
          }
        });

        listRooms.push({
          id: room.id,
          name: room.name,
          gateways,
          createdAt: room.createdAt,
          updatedAt: room.updatedAt,
        });
        this.setState({ rooms: listRooms });
      });
    }
  }

  render() {
    const columns = ['id', 'Name', 'Gateways', 'Created', 'Last updated'];

    return (
      <Row>
        <WhiteBox
          s={12}
          m={12}
          l={12}
          node={[
            <Grid
              key={'list-rooms'}
              columns={columns}
              datas={this.state.rooms}
              addUrl="/add-room"
              editUrl="/edit-room"
              deleteRequest={this.props.DeleteRoomRequest}
              actions
            >
            </Grid>,
          ]}
        />
      </Row>
    );
  }
}

ListRooms.propTypes = {
  setPageTitle: React.PropTypes.func,
  AllRoomsRequest: React.PropTypes.func,
  DeleteRoomRequest: React.PropTypes.func,
  rooms: React.PropTypes.array,
};

export default ListRooms;
