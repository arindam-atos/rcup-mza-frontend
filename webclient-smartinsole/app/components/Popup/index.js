import React from 'react';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';

const Popup = (props) =>
  <SweetAlert
    show={props.show}
    title="Are you sure?"
    text="Warning: This data will be definitely lost."
    type="warning"
    showCancelButton
    confirmButtonText="Delete"
    confirmButtonColor="#E53935"
    cancelButtonText="Cancel"
    onConfirm={props.onConfirm}
    onCancel={props.onCancel}
  />;

Popup.propTypes = {
  show: React.PropTypes.bool,
  onConfirm: React.PropTypes.func,
  onCancel: React.PropTypes.func,
};

export default Popup;
