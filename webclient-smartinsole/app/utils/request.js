//
// Module to centralize the use of an ajax API (here superagent)
//

import superagent from "superagent";
import { browserHistory } from "react-router";
//import manifest from "../manifest.json";

//const baseUrl = manifest.url_server_api;

const urlArray = process.env.NODE_ENV.split("#");

//console.log(" array split : " + urlArray);

const baseUrl = "http://" + urlArray[0];

const historyUrl = "http://" + urlArray[1];

const envType = urlArray[2];

const loggedUser = JSON.parse(localStorage.getItem("loggedUser"));

//console.log("baseUrl " + baseUrl);

//console.log("envtype " + envType);

function get(url, overrideUrl) {
  if (url.startsWith("/history")) {
    //console.log("historyUrl " + historyUrl);
    return superagent.get(historyUrl + url);
  }

  if (overrideUrl) {
    return superagent.get(url).withCredentials();
  }
  return superagent.get(baseUrl + url);
  // .withCredentials()
  // .set('Authorization', `Bearer  ${loggedUser.accessToken}`);
}

function post(url, overrideUrl) {
  console.log("url", url);
  console.log("baseurl", baseUrl);
  if (overrideUrl) {
    return superagent.post(url).withCredentials();
  }
  return superagent.post(baseUrl + url);
  // .withCredentials()
  // .set('Authorization', `Bearer  ${loggedUser.accessToken}`);
}

function put(url) {
  return superagent.put(baseUrl + url);
  // .withCredentials()
  // .set('Authorization', `Bearer  ${loggedUser.accessToken}`);
}

function del(url) {
  return superagent.del(baseUrl + url);
  // .withCredentials()
  // .set('Authorization', `Bearer  ${loggedUser.accessToken}`);
}

function head(url) {
  return superagent.head(baseUrl + url);
  // .withCredentials()
  // .set('Authorization', `Bearer  ${loggedUser.accessToken}`);
}

function redirectHandling(errorCode) {
  if (errorCode === 404) {
    browserHistory.push("/notfound");
  } else if (errorCode === 403) {
    browserHistory.push("/denied");
  } else if (errorCode === 401) {
    localStorage.clear();
    window.location.replace("/login");
    // browserHistory.push('/login');
  }
}

export default {
  get,
  post,
  put,
  del,
  head,
  redirectHandling
};
