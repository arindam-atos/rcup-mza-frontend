import { connect } from 'react-redux';
import HomePage from 'components/HomePage';
import { AllUsersRequest } from 'containers/ListUsers/actions';
import { AllTeamsRequest } from 'containers/ListTeams/actions';
import { setPageTitle } from 'containers/NavBar/actions';

function mapStateToProps(state) {
  return {
    users: state.get('users').users,
    teams: state.get('teams').teams,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
    AllTeamsRequest: (companyId) => dispatch(AllTeamsRequest(companyId)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
