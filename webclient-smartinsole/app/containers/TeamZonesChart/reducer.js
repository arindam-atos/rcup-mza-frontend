import {
  TEAM_USERS_SAMPLES,
} from './constants';

const initialState = {
  teamUsersSamples: [],
};

function TeamZonesChartReducer(state = initialState, action) {
  switch (action.type) {
    case TEAM_USERS_SAMPLES:
      return { ...state, teamUsersSamples: action.teamUsersSamples };
    default:
      return state;
  }
}

export default TeamZonesChartReducer;
