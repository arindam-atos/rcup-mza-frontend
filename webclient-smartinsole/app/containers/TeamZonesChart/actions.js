import request from 'utils/request';
import moment from 'moment/moment';
import {TEAM_USERS_SAMPLES} from './constants';

function setTeamUsersSamples(teamUsersSamples) {
    return {
        type: TEAM_USERS_SAMPLES,
        teamUsersSamples,
    };
}

export function getSamplesByHours(companyId, addresses, day, from, to) {
    const fromTime = moment(day + ' ' + from).utc().format('HH:mm:ss');
    const toTime = moment(day + ' ' + to).utc().format('HH:mm:ss');

    return (dispatch) => request
        .get('/historyByDay')
        .query({
            companyId,
            addresses,
            day,
            from: fromTime,
            to: toTime,
        })
        .end((err, res) => {
            if (!err) {
                dispatch(setTeamUsersSamples(res.body));
            }
        });
}
