import { connect } from 'react-redux';
import TeamZonesChart from 'components/TeamZonesChart';
import { AllGatewaysRequest } from 'containers/ListGateways/actions';
import { AllRoomsRequest } from 'containers/ListRooms/actions';
import { getSamplesByHours } from './actions';

function mapStateToProps(state) {
  return {
    gateways: state.get('gateways').gateways,
    teamUsersSamples: state.get('teamUsersSamples').teamUsersSamples,
    rooms: state.get('rooms').rooms,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllGatewaysRequest: (companyId) => dispatch(AllGatewaysRequest(companyId)),
    getSamplesByHours: (companyId, addresses, day, from, to) => dispatch(getSamplesByHours(companyId, addresses, day, from, to)),
    AllRoomsRequest: (companyId) => dispatch(AllRoomsRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamZonesChart);
