import { ALL_TEAMS } from './constants';

const initialState = {
  teams: [],
};

function TeamsReducer(state = initialState, action) {
  switch (action.type) {
    case ALL_TEAMS:
      return { ...state, teams: action.teams };
    default:
      return state;
  }
}

export default TeamsReducer;
