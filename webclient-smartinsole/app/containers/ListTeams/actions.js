import request from 'utils/request';
// import { browserHistory } from 'react-router';
import moment from 'moment/moment';
import { ALL_TEAMS } from './constants';

export function setAllTeams(teams) {
  const teamsArr = [];
  let teamObj = {};
  teams.forEach((team) => {
    teamObj = team;
    teamObj.createdAt = moment(team.createdAt).format('DD-MM-YYYY');
    teamObj.updatedAt = moment(team.updatedAt).format('DD-MM-YYYY');
    teamsArr.push(teamObj);
  });

  return {
    type: ALL_TEAMS,
    teams: teamsArr,
  };
}

export function AllTeamsRequest(companyId) {
  return function setAction(dispatch) {
    return request
    .get(`/companies/${companyId}/teams`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setAllTeams(res.body));
      }
    });
  };
}

export function DeleteTeamRequest(companyId, teamId) {
  return function setAction(dispatch) {
    return request
    .del(`/companies/${companyId}/teams/${teamId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(AllTeamsRequest(companyId));
      }
    });
  };
}
