
import { connect } from 'react-redux';
import { ListTeams } from 'components/ListTeams';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllTeamsRequest, DeleteTeamRequest } from './actions';

function mapStateToProps(state) {
  return {
    teams: state.get('teams').teams,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AllTeamsRequest: (companyId) => dispatch(AllTeamsRequest(companyId)),
    DeleteTeamRequest: (companyId, teamId) => dispatch(DeleteTeamRequest(companyId, teamId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTeams);
