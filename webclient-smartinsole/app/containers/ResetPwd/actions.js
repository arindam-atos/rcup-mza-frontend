import request from 'utils/request';
// import { browserHistory } from 'react-router';
import { setCard } from 'containers/CardPanel/actions';
import manifest from '../../manifest.json';

export function ResetPwdRequest(username) {
  return function setAction(dispatch) {
    return request
      .post(manifest.request_pwd_url, true)
      .type('json')
      .send({
        username,
      })
      .end((err, res) => {
        request.redirectHandling(res.statusCode);
        if (!err) {
          dispatch(setCard('Un email vous a été envoyé, veuillez suivre les instructions pour changer de mot de passe'));
        } else {
          dispatch(setCard("L'email n'existe pas.", 'error'));
        }
      });
  };
}
