import { connect } from 'react-redux';
import ResetPwd from 'components/ResetPwd';
import { ResetPwdRequest } from './actions';

function mapDispatchToProps(dispatch) {
  return {
    ResetPwdRequest: (username) => dispatch(ResetPwdRequest(username)),
  };
}

export default connect(null, mapDispatchToProps)(ResetPwd);
