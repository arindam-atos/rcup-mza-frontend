import request from 'utils/request';
// import { browserHistory } from 'react-router';
import moment from 'moment/moment';
import { ALL_COMPANIES } from './constants';

export function setAllCompanies(companies) {
  let compObj = {};
  const compArr = [];

  companies.forEach((company) => {
    compObj = company;
    compObj.createdAt = moment(company.createdAt).format('DD-MM-YYYY');
    compObj.updatedAt = moment(company.updatedAt).format('DD-MM-YYYY');
    compArr.push(compObj);
  });

  return {
    type: ALL_COMPANIES,
    companies: compArr,
  };
}

export function AllCompaniesRequest() {
  return function setAction(dispatch) {
    return request
    .get('/companies')
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setAllCompanies(res.body));
      }
    });
  };
}

export function DeleteCompanyRequest(curCompany = null, companyId) {
  return function setAction(dispatch) {
    return request
    .del(`/companies/${companyId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(AllCompaniesRequest(companyId));
      }
    });
  };
}
