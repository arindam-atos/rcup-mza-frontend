import { connect } from 'react-redux';
import ListCompanies from 'components/ListCompanies';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllCompaniesRequest, DeleteCompanyRequest } from './actions';

function mapStateToProps(state) {
  return {
    companies: state.get('companies').companies,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AllCompaniesRequest: () => dispatch(AllCompaniesRequest()),
    DeleteCompanyRequest: (curCmp, companyId) => dispatch(DeleteCompanyRequest(curCmp, companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListCompanies);
