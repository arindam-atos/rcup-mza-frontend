import { ALL_COMPANIES } from './constants';

const initialState = {
  companies: [],
};

function CompaniesReducer(state = initialState, action) {
  switch (action.type) {
    case ALL_COMPANIES:
      return { ...state, companies: action.companies };
    default:
      return state;
  }
}

export default CompaniesReducer;
