import request from 'utils/request';
import { browserHistory } from 'react-router';
import { FETCHED_COMPANY } from './constants';

export function setCompany(company) {
  const companyObj = {
    name: company.name,
    map: company.map,
    address: company.address.address,
    city: company.address.city,
    zipCode: company.address.zipCode,
    country: company.address.country,
    latitude: company.address.latitude,
    longitude: company.address.longitude,
    hardnesses: company.hardnesses,
    file: company.file,
    logo: company.logo,
  };

  return {
    type: FETCHED_COMPANY,
    company: companyObj,
  };
}

export function GetCompanyRequest(company) {
  return function setAction(dispatch) {
    return request
    .get(`/companies/${company}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setCompany(res.body));
      }
    });
  };
}

export function EditCompanyRequest(companyId, company) {
  const { name, map, hardnesses, file, address, city, zipCode, country, latitude, longitude } = company;

  return function setAction() {
    return request
    .put(`/companies/${companyId}`)
    .field({ name })
    .field({ map })
    .field({ hardnesses })
    .field({ address })
    .field({ city })
    .field({ zipCode })
    .field({ country })
    .field({ latitude })
    .field({ longitude })
    .attach('file', file)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/companies');
      }
    });
  };
}
