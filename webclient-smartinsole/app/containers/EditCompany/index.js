import { connect } from 'react-redux';
import EditCompanyComponent from 'components/EditCompany';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllGatewaysRequest } from 'containers/ListGateways/actions';
import { GetCompanyRequest, EditCompanyRequest } from './actions';

function mapStateToProps(state) {
  return {
    company: state.get('fetchedCompany').company,
    gateways: state.get('gateways').gateways,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    GetCompanyRequest: (company) => dispatch(GetCompanyRequest(company)),
    EditCompanyRequest: (companyId, company) =>
      dispatch(EditCompanyRequest(companyId, company)),
    AllGatewaysRequest: (companyId) => dispatch(AllGatewaysRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCompanyComponent);
