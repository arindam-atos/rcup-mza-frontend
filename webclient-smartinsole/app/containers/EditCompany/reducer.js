import { FETCHED_COMPANY } from './constants';

const initialState = {
  company: {},
};

function EditCompanyReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHED_COMPANY:
      return { ...state, company: action.company };
    default:
      return state;
  }
}

export default EditCompanyReducer;
