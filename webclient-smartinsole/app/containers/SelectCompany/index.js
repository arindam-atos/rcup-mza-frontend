import { connect } from 'react-redux';
import SelectCompany from 'components/SelectCompany';
import { AllCompaniesRequest } from 'containers/ListCompanies/actions';

function mapStateToProps(state) {
  return {
    companies: state.get('companies').companies,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllCompaniesRequest: () => dispatch(AllCompaniesRequest()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectCompany);
