import { connect } from 'react-redux';
import ListUsersSamples from 'components/ListUsersSamples';
import { AllUsersRequest } from 'containers/ListUsers/actions';
import { setPageTitle } from 'containers/NavBar/actions';
import { mapUsersAndSamples, getSamples } from './actions';

function mapStateToProps(state) {
  return {
    users: state.get('users').users,
    usersSteps: state.get('teamUsers').steps,
    lastSamples: state.get('usersSamples').lastSamples,
    userSamples: state.get('usersSamples').userSamples,
    usersSamples: state.get('usersSamples').usersSamples,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
    mapUsersAndSamples: (users, samples) => dispatch(mapUsersAndSamples(users, samples)),
    getSamples: (companyId, addresses, from, to) => dispatch(getSamples(companyId, addresses, from, to)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListUsersSamples);
