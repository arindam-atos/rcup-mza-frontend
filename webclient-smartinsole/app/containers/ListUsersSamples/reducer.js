import {
  LAST_SAMPLES,
  USERS_SAMPLES,
  USER_SAMPLES,
} from './constants';

const initialState = {
  lastSamples: {},
  usersSamples: [],
  userSamples: {},
};

function ListUsersSamplesReducer(state = initialState, action) {
  switch (action.type) {
    case LAST_SAMPLES:
      return { ...state, lastSamples: action.lastSamples };
    case USERS_SAMPLES:
      return { ...state, usersSamples: action.usersSamples };
    case USER_SAMPLES:
      return { ...state, userSamples: action.userSamples };
    default:
      return state;
  }
}

export default ListUsersSamplesReducer;
