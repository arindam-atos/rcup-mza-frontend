import request from 'utils/request';
import { LAST_SAMPLES, USERS_SAMPLES, USER_SAMPLES } from './constants';

export function setUsersSamples(usersSamples) {
  return {
    type: USERS_SAMPLES,
    usersSamples,
  };
}

export function setLastSamples(lastSamples) {
  return {
    type: LAST_SAMPLES,
    lastSamples,
  };
}

export function setUserSamples(userSamples) {
  return {
    type: USER_SAMPLES,
    userSamples,
  };
}


export function mapUsersAndSamples(users, samples) {
  const usersLastSample = [];
  return (dispatch) => {
    users.forEach((user) => {
      Object.keys(samples).forEach((sample) => {
        if (user.macAddress === sample) {
          const lastSample = samples[sample].length - 1;
          const samplesByUser = samples[sample][lastSample];
          usersLastSample.push({
            user,
            sample: samplesByUser,
          });
        }
      });
    });
    dispatch(setUsersSamples(usersLastSample));
  };
}

export function getSamples(companyId, addresses, from, to) {

if( addresses === "" || addresses === null || addresses.length === 0 )
 addresses = "00:00:00:00:00:00";

  return (dispatch) =>
    new Promise((resolve, reject) => {
      request
      .get('/history')
      .query({
        companyId,
        addresses,
        from,
        to,
      })
      .then((res) => {
        request.redirectHandling(res.statusCode);
        if (addresses.length > 1) {
          resolve(dispatch(setLastSamples(res.body)));
        } else {
          resolve(dispatch(setUserSamples(res.body)));
        }
      }).catch((err) => {
        reject(err);
      });
    });
}
