import { PAGE_TITLE } from './constants';

const initialState = {
  pageTitle: '',
};

function NavBarReducer(state = initialState, action) {
  switch (action.type) {
    case PAGE_TITLE:
      return { ...state, pageTitle: action.pageTitle };
    default:
      return state;
  }
}

export default NavBarReducer;
