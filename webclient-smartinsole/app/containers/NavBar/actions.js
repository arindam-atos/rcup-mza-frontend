import { PAGE_TITLE } from './constants';

export function setPageTitle(pageTitle) {
  return {
    type: PAGE_TITLE,
    pageTitle,
  };
}
