import { connect } from 'react-redux';
import NavBar from 'components/NavBar';

function mapStateToProps(state) {
  return {
    pageTitle: state.get('navBar').pageTitle,
  };
}

function mapDispatchToProps() {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
