import { connect } from 'react-redux';
import EditUserComponent from 'components/EditUser';
import { setPageTitle } from 'containers/NavBar/actions';
import { GetUserRequest, EditUserRequest } from './actions';

function mapStateToProps(state) {
  return {
    user: state.get('fetchedUser').user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    GetUserRequest: (companyId, userId) => dispatch(GetUserRequest(companyId, userId)),
    EditUserRequest: (companyId, user) =>
      dispatch(EditUserRequest(companyId, user)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUserComponent);
