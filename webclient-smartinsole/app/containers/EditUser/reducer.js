import { FETCHED_USER } from './constants';

const initialState = {
  user: {},
};

function EditUserReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHED_USER:
      return { ...state, user: action.user };
    default:
      return state;
  }
}

export default EditUserReducer;
