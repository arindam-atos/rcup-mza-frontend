import request from 'utils/request';
import { browserHistory } from 'react-router';
import { FETCHED_USER } from './constants';

export function setUser(user) {
  const userObj = {
    id: user.id,
    email: user.email,
    macAddress: user.macAddress,
    macAdd1: user.macAddress.substring(0, 2),
    macAdd2: user.macAddress.substring(3, 5),
    macAdd3: user.macAddress.substring(6, 8),
    macAdd4: user.macAddress.substring(9, 11),
    macAdd5: user.macAddress.substring(12, 14),
    macAdd6: user.macAddress.substring(15, 17),
    role: user.role,
    firstname: user.property.firstname,
    lastname: user.property.lastname,
    height: user.property.height,
    weight: user.property.weight,
    shoeSize: user.property.shoeSize,
    age: user.property.age,
  };

  return {
    type: FETCHED_USER,
    user: userObj,
  };
}

export function GetUserRequest(companyId, userId) {
  return function setAction(dispatch) {
    return request
    .get(`/companies/${companyId}/users/${userId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setUser(res.body));
      }
    });
  };
}

export function EditUserRequest(companyId, user) {
  return function setAction() {
    return request
    .put(`/companies/${companyId}/users/${user.id}`)
    .type('json')
    .send({ user })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/users');
      }
    });
  };
}
