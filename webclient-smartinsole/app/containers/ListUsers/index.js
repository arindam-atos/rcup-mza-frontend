import { connect } from 'react-redux';
import ListUsers from 'components/ListUsers';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllUsersRequest, DeleteUserRequest } from './actions';

function mapStateToProps(state) {
  return {
    users: state.get('users').users,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
    DeleteUserRequest: (companyId, userId) => dispatch(DeleteUserRequest(companyId, userId)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListUsers);
