import { ALL_USERS } from './constants';

const initialState = {
  users: [],
};

function UsersReducer(state = initialState, action) {
  switch (action.type) {
    case ALL_USERS:
      return { ...state, users: action.users };
    default:
      return state;
  }
}

export default UsersReducer;
