import request from 'utils/request';
// import { browserHistory } from 'react-router';
import moment from 'moment/moment';
import { ALL_USERS } from './constants';

export function setAllUsers(users) {
  let userObj = {};
  const userArr = [];

  users.forEach((user) => {
    userObj.id = user.id;
    userObj.email = user.email;
    userObj.macAddress = user.macAddress;
    userObj.role = user.role;
    userObj.firstname = user.property.firstname;
    userObj.lastname = user.property.lastname;
    userObj.createdAt = moment(user.createdAt).format('DD-MM-YYYY');
    userObj.updatedAt = moment(user.updatedAt).format('DD-MM-YYYY');
    userArr.push(userObj);
    userObj = {};
  });


  return {
    type: ALL_USERS,
    users: userArr,
  };
}

export function AllUsersRequest(companyId) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      request
      .get(`/companies/${companyId}/users`)
      .then((res) => {
        request.redirectHandling(res.statusCode);
        dispatch(setAllUsers(res.body));
        resolve();
      }).catch((err) => {
        request.redirectHandling(err.response.statusCode);
        reject(err);
      });
    });
}

export function DeleteUserRequest(companyId, userId) {
  return function setAction(dispatch) {
    return request
    .del(`/companies/${companyId}/users/${userId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(AllUsersRequest(companyId));
      }
    });
  };
}
