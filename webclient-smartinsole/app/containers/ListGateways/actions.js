import request from 'utils/request';
// import { browserHistory } from 'react-router';
import moment from 'moment/moment';
import { ALL_GATEWAYS } from './constants';

export function setAllGateways(gateways) {
  let gtwObj = {};
  const gtwArr = [];

  gateways.forEach((gtw) => {
    gtwObj.id = gtw.id;
    gtwObj.name = gtw.name;
    gtwObj.room = gtw.room.name;
    gtwObj.latitude = gtw.latitude;
    gtwObj.longitude = gtw.longitude;
    gtwObj.createdAt = moment(gtw.createdAt).format('DD-MM-YYYY');
    gtwObj.updatedAt = moment(gtw.updatedAt).format('DD-MM-YYYY');
    gtwArr.push(gtwObj);
    gtwObj = {};
  });


  return {
    type: ALL_GATEWAYS,
    gateways: gtwArr,
  };
}

export function AllGatewaysRequest(companyId) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      request
      .get(`/companies/${companyId}/gateways`)
      .end((err, res) => {
        request.redirectHandling(res.statusCode);
        if (err) reject(err);
        resolve(dispatch(setAllGateways(res.body)));
      });
    });
}

export function DeleteGatewayRequest(companyId, gtwId) {
  return function setAction(dispatch) {
    return request
    .del(`/companies/${companyId}/gateways/${gtwId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(AllGatewaysRequest(companyId));
      }
    });
  };
}
