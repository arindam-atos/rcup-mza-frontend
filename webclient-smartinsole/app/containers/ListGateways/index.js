import { connect } from 'react-redux';
import ListGateways from 'components/ListGateways';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllGatewaysRequest, DeleteGatewayRequest } from './actions';

function mapStateToProps(state) {
  return {
    gateways: state.get('gateways').gateways,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AllGatewaysRequest: (companyId) => dispatch(AllGatewaysRequest(companyId)),
    DeleteGatewayRequest: (companyId, userId) => dispatch(DeleteGatewayRequest(companyId, userId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListGateways);
