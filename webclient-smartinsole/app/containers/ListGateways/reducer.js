import { ALL_GATEWAYS } from './constants';

const initialState = {
  gateways: [],
};

function GatewaysReducer(state = initialState, action) {
  switch (action.type) {
    case ALL_GATEWAYS:
      return { ...state, gateways: action.gateways };
    default:
      return state;
  }
}

export default GatewaysReducer;
