import { FETCHED_GATEWAY } from './constants';

const initialState = {
  gateway: {},
};

function EditGatewayReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHED_GATEWAY:
      return { ...state, gateway: action.gateway };
    default:
      return state;
  }
}

export default EditGatewayReducer;
