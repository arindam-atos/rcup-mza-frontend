import { connect } from 'react-redux';
import EditGateway from 'components/EditGateway';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllRoomsRequest } from 'containers/ListRooms/actions';
import { GetGatewayRequest, EditGatewayRequest } from './actions';

function mapStateToProps(state) {
  return {
    rooms: state.get('rooms').rooms,
    gateway: state.get('fetchedGateway').gateway,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AllRoomsRequest: (companyId) => dispatch(AllRoomsRequest(companyId)),
    GetGatewayRequest: (companyId, gtwId) => dispatch(GetGatewayRequest(companyId, gtwId)),
    EditGatewayRequest: (companyId, gtw) =>
      dispatch(EditGatewayRequest(companyId, gtw)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditGateway);
