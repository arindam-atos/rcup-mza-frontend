import request from 'utils/request';
import { browserHistory } from 'react-router';
import { FETCHED_GATEWAY } from './constants';

export function setGateway(gtw) {
  const gtwObj = {
    id: gtw.id,
    name: gtw.name,
    roomId: gtw.roomId,
    latitude: gtw.latitude,
    longitude: gtw.longitude,
    createdAt: gtw.createdAt,
    updatedAt: gtw.updatedAt,
  };

  return {
    type: FETCHED_GATEWAY,
    gateway: gtwObj,
  };
}

export function GetGatewayRequest(companyId, gtwId) {
  return function setAction(dispatch) {
    return request
    .get(`/companies/${companyId}/gateways/${gtwId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setGateway(res.body));
      }
    });
  };
}

export function EditGatewayRequest(companyId, gtw) {
  return function setAction() {
    return request
    .put(`/companies/${companyId}/gateways/${gtw.id}`)
    .type('json')
    .send({ gtw })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/gateways');
      }
    });
  };
}
