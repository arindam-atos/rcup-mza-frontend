import request from 'utils/request';
// import { browserHistory } from 'react-router';
import moment from 'moment/moment';
import { ALL_ROOMS } from './constants';

export function setAllRooms(rooms) {
  let roomObj = {};
  const roomArr = [];

  rooms.forEach((room) => {
    roomObj.id = room.id;
    roomObj.name = room.name;
    roomObj.gateways = room.gateways;
    roomObj.hardnesses = JSON.parse(room.hardnesses) || {};
    roomObj.createdAt = moment(room.createdAt).format('DD-MM-YYYY');
    roomObj.updatedAt = moment(room.updatedAt).format('DD-MM-YYYY');
    roomArr.push(roomObj);
    roomObj = {};
  });


  return {
    type: ALL_ROOMS,
    rooms: roomArr,
  };
}

export function AllRoomsRequest(companyId) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      request
      .get(`/companies/${companyId}/rooms`)
      .end((err, res) => {
        request.redirectHandling(res.statusCode);
        if (err) reject(err);
        resolve(dispatch(setAllRooms(res.body)));
      });
    });
}

export function DeleteRoomRequest(companyId, roomId) {
  return function setAction(dispatch) {
    return request
    .del(`/companies/${companyId}/rooms/${roomId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(AllRoomsRequest(companyId));
      }
    });
  };
}
