import { connect } from 'react-redux';
import ListRooms from 'components/ListRooms';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllRoomsRequest, DeleteRoomRequest } from './actions';

function mapStateToProps(state) {
  return {
    rooms: state.get('rooms').rooms,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AllRoomsRequest: (companyId) => dispatch(AllRoomsRequest(companyId)),
    DeleteRoomRequest: (companyId, userId) => dispatch(DeleteRoomRequest(companyId, userId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListRooms);
