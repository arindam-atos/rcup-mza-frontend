import { ALL_ROOMS } from './constants';

const initialState = {
  rooms: [],
};

function RoomsReducer(state = initialState, action) {
  switch (action.type) {
    case ALL_ROOMS:
      return { ...state, rooms: action.rooms };
    default:
      return state;
  }
}

export default RoomsReducer;
