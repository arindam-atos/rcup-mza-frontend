import { connect } from 'react-redux';
import EditTeamComponent from 'components/EditTeam';
import { AllUsersRequest } from 'containers/ListUsers/actions';
import { setPageTitle } from 'containers/NavBar/actions';
import { GetTeamRequest, EditTeamRequest } from './actions';

function mapStateToProps(state) {
  return {
    team: state.get('fetchedTeam').team,
    users: state.get('users').users,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    GetTeamRequest: (companyId, teamId) => dispatch(GetTeamRequest(companyId, teamId)),
    EditTeamRequest: (companyId, teamId) =>
      dispatch(EditTeamRequest(companyId, teamId)),
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTeamComponent);
