import { FETCHED_TEAM } from './constants';

const initialState = {
  team: {},
};

function EditTeamReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHED_TEAM:
      return { ...state, team: action.team };
    default:
      return state;
  }
}

export default EditTeamReducer;
