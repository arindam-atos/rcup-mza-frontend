import request from 'utils/request';
import { browserHistory } from 'react-router';
import { FETCHED_TEAM } from './constants';

export function setTeam(team) {
  return {
    type: FETCHED_TEAM,
    team,
  };
}

export function GetTeamRequest(companyId, teamId) {
  return function setAction(dispatch) {
    return request
    .get(`/companies/${companyId}/teams/${teamId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setTeam(res.body));
      }
    });
  };
}

export function EditTeamRequest(companyId, team) {
  return function setAction() {
    return request
    .put(`/companies/${companyId}/teams/${team.id}`)
    .type('json')
    .send({ team })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/teams');
      }
    });
  };
}
