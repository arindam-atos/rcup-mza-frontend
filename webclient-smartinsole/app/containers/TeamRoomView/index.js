import { connect } from 'react-redux';
import TeamRoomView from 'components/TeamRoomView';
import { AllRoomsRequest } from 'containers/ListRooms/actions';
import { getSamples } from 'containers/ListUsersSamples/actions';

function mapStateToProps(state) {
  return {
    rooms: state.get('rooms').rooms,
    lastSamples: state.get('usersSamples').lastSamples,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllRoomsRequest: (companyId) => dispatch(AllRoomsRequest(companyId)),
    getSamples: (companyId, addresses, from, to) => dispatch(getSamples(companyId, addresses, from, to)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamRoomView);
