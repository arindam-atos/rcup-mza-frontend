import { connect } from 'react-redux';
import AddRoom from 'components/AddRoom';
import { setPageTitle } from 'containers/NavBar/actions';
import { GetCompanyRequest } from 'containers/EditCompany/actions';
import { AddRoomRequest } from './actions';

function mapStateToProps(state) {
  return {
    company: state.get('fetchedCompany').company,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AddRoomRequest: (companyId, gateway) => dispatch(AddRoomRequest(companyId, gateway)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    GetCompanyRequest: (companyId) => dispatch(GetCompanyRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRoom);
