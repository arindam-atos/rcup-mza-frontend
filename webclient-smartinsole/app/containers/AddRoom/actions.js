import request from 'utils/request';
import { browserHistory } from 'react-router';

export function AddRoomRequest(companyId, room) {
  const obj = room;

  delete obj.hardnessesOptions;
  delete obj.listHardnesses;
  return function setAction() {
    return request
    .post(`/companies/${companyId}/rooms`)
    .type('json')
    .send({
      room: obj,
    })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/rooms');
      }
    });
  };
}
