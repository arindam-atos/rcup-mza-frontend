import { connect } from 'react-redux';
import AddGateway from 'components/AddGateway';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllRoomsRequest } from 'containers/ListRooms/actions';
import { AddGatewayRequest } from './actions';

function mapStateToProps(state) {
  return {
    rooms: state.get('rooms').rooms,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllRoomsRequest: (companyId) => dispatch(AllRoomsRequest(companyId)),
    AddGatewayRequest: (companyId, gateway) => dispatch(AddGatewayRequest(companyId, gateway)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddGateway);
