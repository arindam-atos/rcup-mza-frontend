import request from 'utils/request';
import { browserHistory } from 'react-router';

export function AddGatewayRequest(companyId, gateway) {
  return function setAction() {
    return request
    .post(`/companies/${companyId}/gateways`)
    .type('json')
    .send({
      gateway,
    })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/gateways');
      }
    });
  };
}
