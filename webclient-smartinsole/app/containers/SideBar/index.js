import { connect } from 'react-redux';
import SideBar from 'components/SideBar';
import { GetCompanyRequest } from 'containers/EditCompany/actions';

function mapStateToProps(state) {
  return {
    company: state.get('fetchedCompany').company,
    pageTitle: state.get('navBar').pageTitle,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    GetCompanyRequest: (company) => dispatch(GetCompanyRequest(company)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
