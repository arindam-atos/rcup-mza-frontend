import request from 'utils/request';
import { browserHistory } from 'react-router';

export function AddCompanyRequest(company) {
  const { name, map, hardnesses, file, address, city, zipCode, country, latitude, longitude } = company;
  return function setAction() {
    return request
    .post('/companies')
    .field({ name })
    .field({ map })
    .field({ hardnesses })
    .field({ address })
    .field({ city })
    .field({ zipCode })
    .field({ country })
    .field({ latitude })
    .field({ longitude })
    .attach('file', file)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/companies');
      }
    });
  };
}
