import { connect } from 'react-redux';
import AddCompanyComponent from 'components/AddCompany';
import { setPageTitle } from 'containers/NavBar/actions';
import { AddCompanyRequest } from './actions';

function mapDispatchToProps(dispatch) {
  return {
    AddCompanyRequest: (company) => dispatch(AddCompanyRequest(company)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(null, mapDispatchToProps)(AddCompanyComponent);
