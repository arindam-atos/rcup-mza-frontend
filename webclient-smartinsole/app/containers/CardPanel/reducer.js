import { CARD_PANEL } from './constants';

const initialState = {
  card: {},
};

function CardPanelReducer(state = initialState, action) {
  switch (action.type) {
    case CARD_PANEL:
      return { ...state, card: action.card };
    default:
      return state;
  }
}

export default CardPanelReducer;
