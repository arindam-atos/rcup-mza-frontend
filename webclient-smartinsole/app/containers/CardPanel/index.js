import { connect } from 'react-redux';
import CardPanel from 'components/CardPanel';
import { resetCard } from './actions';

function mapStateToProps(state) {
  return {
    card: state.get('card').card,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    resetCard: () => dispatch(resetCard()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CardPanel);
