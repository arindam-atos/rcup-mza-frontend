import { CARD_PANEL } from './constants';

export function setCard(msg, type) {
  return {
    type: CARD_PANEL,
    card: {
      message: msg,
      type,
    },
  };
}

export function resetCard() {
  return {
    type: CARD_PANEL,
    card: {},
  };
}
