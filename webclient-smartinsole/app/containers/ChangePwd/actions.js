import request from 'utils/request';
import { browserHistory } from 'react-router';
import { setCard } from 'containers/CardPanel/actions';

export function ChangePwdRequest(username, password) {
  return function setAction(dispatch) {
    return request
      .post('/new_pwd')
      .type('json')
      .send({
        username,
        password,
      })
      .end((err, res) => {
        localStorage.clear();
        if (!err) {
          dispatch(setCard("Votre mot de passe a été modifié. Vous allez être rediriger vers la page d'authentification dans quelques secondes.", 'info'));
        } else {
          dispatch(setCard("Une erreur s'est produite. Vous allez être rediriger vers la page d'authentification dans quelques secondes.", 'error'));
        }
        setTimeout(() => {
          if (!err) {
            browserHistory.push('/login');
          } else {
            request.redirectHandling(res.statusCode);
          }
        }, 3000);
      });
  };
}
