import { connect } from 'react-redux';
import ChangePwd from 'components/ChangePwd';
import { setCard } from 'containers/CardPanel/actions';
import { ChangePwdRequest } from './actions';

function mapDispatchToProps(dispatch) {
  return {
    setCard: (msg, type) => dispatch(setCard(msg, type)),
    ChangePwdRequest: (username, password) => dispatch(ChangePwdRequest(username, password)),
  };
}

export default connect(null, mapDispatchToProps)(ChangePwd);
