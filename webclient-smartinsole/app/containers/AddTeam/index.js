import { connect } from 'react-redux';
import AddTeamComponent from 'components/AddTeam';
import { AllUsersRequest } from 'containers/ListUsers/actions';
import { setPageTitle } from 'containers/NavBar/actions';
import { AddTeamRequest } from './actions';

function mapStateToProps(state) {
  return {
    users: state.get('users').users,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AddTeamRequest: (companyId, team) => dispatch(AddTeamRequest(companyId, team)),
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTeamComponent);
