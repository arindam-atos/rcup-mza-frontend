import request from 'utils/request';
import { browserHistory } from 'react-router';

export function AddTeamRequest(companyId, team) {
  return function setAction() {
    return request
    .post(`/companies/${companyId}/teams`)
    .type('json')
    .send({
      team,
    })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/teams');
      }
    });
  };
}
