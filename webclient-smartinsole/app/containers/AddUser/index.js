import { connect } from 'react-redux';
import AddUserComponent from 'components/AddUser';
import { setPageTitle } from 'containers/NavBar/actions';
import { AddUserRequest } from './actions';

function mapDispatchToProps(dispatch) {
  return {
    AddUserRequest: (companyId, user) => dispatch(AddUserRequest(companyId, user)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(null, mapDispatchToProps)(AddUserComponent);
