import request from 'utils/request';
import { browserHistory } from 'react-router';

export function AddUserRequest(companyId, user) {
  return function setAction() {
    return request
    .post(`/companies/${companyId}/users`)
    .type('json')
    .send({
      user,
    })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/users');
      }
    });
  };
}
