import { connect } from 'react-redux';
import HardnessBoard from 'components/HardnessBoard';
import { setPageTitle } from 'containers/NavBar/actions';
import { AllTeamsRequest } from 'containers/ListTeams/actions';
import { getUsersByTeam } from 'containers/TeamDashboard/actions';
import { getSamplesByHours } from 'containers/TeamZonesChart/actions';
import { AllGatewaysRequest } from 'containers/ListGateways/actions';
import { AllRoomsRequest } from 'containers/ListRooms/actions';

function mapStateToProps(state) {
  return {
    usersTeam: state.get('teamUsers').usersTeam,
    teams: state.get('teams').teams,
    teamUsersSamples: state.get('teamUsersSamples').teamUsersSamples,
    gateways: state.get('gateways').gateways,
    rooms: state.get('rooms').rooms,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    getUsersByTeam: (companyId, teamId) => dispatch(getUsersByTeam(companyId, teamId)),
    AllTeamsRequest: (companyId) => dispatch(AllTeamsRequest(companyId)),
    getSamplesByHours: (companyId, addresses, day, from, to) => dispatch(getSamplesByHours(companyId, addresses, day, from, to)),
    AllGatewaysRequest: (companyId) => dispatch(AllGatewaysRequest(companyId)),
    AllRoomsRequest: (companyId) => dispatch(AllRoomsRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HardnessBoard);
