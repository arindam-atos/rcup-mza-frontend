/**
 *
 * App.react.js
 *
 */

import { connect } from 'react-redux';
import App from 'components/App';

function mapStateToProps() {
  return {
  };
}

function mapDispatchToProps() {
  return {
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
