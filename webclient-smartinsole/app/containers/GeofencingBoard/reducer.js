import { USER_SAMPLES } from './constants';

const initialState = {
  userSamples: [],
};

function GeofencingBoardReducer(state = initialState, action) {
  switch (action.type) {
    case USER_SAMPLES:
      return { ...state, userSamples: action.userSamples };
    default:
      return state;
  }
}

export default GeofencingBoardReducer;
