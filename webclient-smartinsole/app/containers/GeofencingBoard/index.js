import { connect } from 'react-redux';
import GeofencingBoard from 'components/GeofencingBoard';
import { AllGatewaysRequest } from 'containers/ListGateways/actions';
import { AllUsersRequest } from 'containers/ListUsers/actions';
import { GetCompanyRequest } from 'containers/EditCompany/actions';
import { setPageTitle } from 'containers/NavBar/actions';
import { getSamplesByHours } from './actions';

function mapStateToProps(state) {
  return {
    userSamples: state.get('geofencing').userSamples,
    gateways: state.get('gateways').gateways,
    users: state.get('users').users,
    company: state.get('fetchedCompany').company,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
    AllGatewaysRequest: (companyId) => dispatch(AllGatewaysRequest(companyId)),
    getSamplesByHours: (companyId, add, day, from, to) =>
    dispatch(getSamplesByHours(companyId, add, day, from, to)),
    GetCompanyRequest: (company) => dispatch(GetCompanyRequest(company)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GeofencingBoard);
