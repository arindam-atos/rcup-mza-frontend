import request from 'utils/request';
import { browserHistory } from 'react-router';

export function LogoutRequest() {
  return request
    .get('/logout', true)
    .end(() => {
      localStorage.clear();
      browserHistory.push('/login');
      window.location.reload();
    });
}
