import request from 'utils/request';
import { browserHistory } from 'react-router';
import { setCard } from 'containers/CardPanel/actions';
import manifest from '../../manifest.json';

export function LoginRequest(username, password) {

  return function setAction(dispatch) {
    return request
      .post("/authenticate")
      .type('json')
      .send({
        username,
        password,
      })
      .end((err, res) => {
        // Only users with minimum rights of manager can access to the website
        if (err) {
          dispatch(setCard('Vos identifiants sont incorrects.', 'error'));

        } else if (res.body.role === 'worker') {
          dispatch(setCard("Vous n'�tes pas autoris� � vous connecter.", 'error'));
        } else {
          // global variables needed for app
          localStorage.setItem('loggedUser', JSON.stringify({
            // accessToken: res.body.accessToken,
            email: res.body.email,
            id: res.body.id,
            firstname: res.body.firstname,
            lastname: res.body.lastname,
            role: res.body.role,
          }));
          localStorage.setItem('currentCompany', JSON.stringify({
            companyId: res.body.companyId,
            companyName: res.body.companyName,
          }));

          window.location.reload();
          browserHistory.push('/');
        }
      });
  };
}
