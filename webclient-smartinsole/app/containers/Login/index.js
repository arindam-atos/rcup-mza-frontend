import { connect } from 'react-redux';
import Login from 'components/Login';
import { LoginRequest } from './actions';

function mapDispatchToProps(dispatch) {
  return {
    LoginRequest: (username, pwd) => dispatch(LoginRequest(username, pwd)),
  };
}

export default connect(null, mapDispatchToProps)(Login);
