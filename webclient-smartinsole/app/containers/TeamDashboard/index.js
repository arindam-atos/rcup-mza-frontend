import { connect } from 'react-redux';
import TeamDashboard from 'components/TeamDashboard';
import { AllTeamsRequest } from 'containers/ListTeams/actions';
import { getUsersAndSteps } from './actions';

function mapStateToProps(state) {
  return {
    usersSteps: state.get('teamUsers').usersSteps,
    teams: state.get('teams').teams,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getUsersAndSteps: (companyId, teamId, from, to) => dispatch(getUsersAndSteps(companyId, teamId, from, to)),
    AllTeamsRequest: (companyId) => dispatch(AllTeamsRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamDashboard);
