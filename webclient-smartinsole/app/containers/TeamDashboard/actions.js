import request from "utils/request";
import { FETCHED_STEPS, USERS_STEPS, FETCHED_USERS_TEAM } from "./constants";

export function setUsersSteps(steps) {
  return {
    type: FETCHED_STEPS,
    steps
  };
}

export function setUsersBySteps(usersSteps) {
  return {
    type: USERS_STEPS,
    usersSteps
  };
}

export function setUsersTeam(usersTeam) {
  return {
    type: FETCHED_USERS_TEAM,
    usersTeam
  };
}

export function getUsersByTeam(companyId, teamId) {
  return dispatch =>
    new Promise((resolve, reject) => {
      request
        .get(`/companies/${companyId}/teams/${teamId}/users`)
        .then(res => {
          request.redirectHandling(res.statusCode);
          resolve(dispatch(setUsersTeam(res.body)));
        })
        .catch(err => {
          reject(err);
        });
    });
}

export function UsersStepsRequest(companyId, addresses, from, to) {
  return dispatch =>
    new Promise((resolve, reject) => {
      request
        .get("/history")
        .query({
          companyId,
          addresses,
          from,
          to
        })
        .then(res => {
          request.redirectHandling(res.statusCode);
          resolve(dispatch(setUsersSteps(res.body)));
        })
        .catch(err => {
          reject(err);
        });
    });
}

function mapUsersAndSteps(users, steps) {
  return dispatch => {
    const usersBySteps = [];

    users.usersTeam.forEach(user => {
      Object.keys(steps.steps).forEach(userSteps => {
        if (userSteps === user.macAddress) {
          const listStepsByMacAdd = steps.steps[userSteps];
          const arrayStepsByUser = [];
          listStepsByMacAdd.forEach(val => {
            arrayStepsByUser.push({
              step: val.steps,
              rawStep: val.rawSteps,
              timestamp: val.timestamp
            });
          });
          usersBySteps.push({
            user,
            arrayStepsByUser
          });
        }
      });
    });
    dispatch(setUsersBySteps(usersBySteps));
  };
}

export function getUsersAndSteps(companyId, teamId, from, to) {
  return dispatch =>
    new Promise(resolve => {
      resolve(dispatch(getUsersByTeam(companyId, teamId)));
    }).then(users => {
      const addresses = users.usersTeam.map(user => user.macAddress);
      return new Promise(resolve => {
        resolve(dispatch(UsersStepsRequest(companyId, addresses, from, to)));
      }).then(
        steps =>
          new Promise(resolve => {
            resolve(dispatch(mapUsersAndSteps(users, steps)));
          })
      );
    });
}
