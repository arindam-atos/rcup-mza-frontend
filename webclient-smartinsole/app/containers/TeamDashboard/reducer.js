import {
  FETCHED_STEPS,
  USERS_STEPS,
  FETCHED_USERS_TEAM,
} from './constants';

const initialState = {
  steps: {},
  usersSteps: [],
  usersTeam: [],
};

function TeamDashboardReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHED_STEPS:
      return { ...state, steps: action.steps };
    case USERS_STEPS:
      return { ...state, usersSteps: action.usersSteps };
    case FETCHED_USERS_TEAM:
      return { ...state, usersTeam: action.usersTeam };
    default:
      return state;
  }
}

export default TeamDashboardReducer;
