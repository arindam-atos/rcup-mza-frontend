import { connect } from 'react-redux';
import EditRoom from 'components/EditRoom';
import { setPageTitle } from 'containers/NavBar/actions';
import { GetCompanyRequest } from 'containers/EditCompany/actions';
import { GetRoomRequest, EditRoomRequest } from './actions';

function mapStateToProps(state) {
  return {
    room: state.get('fetchedRoom').room,
    company: state.get('fetchedCompany').company,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
    GetRoomRequest: (companyId, gtwId) => dispatch(GetRoomRequest(companyId, gtwId)),
    EditRoomRequest: (companyId, gtw) =>
    dispatch(EditRoomRequest(companyId, gtw)),
    GetCompanyRequest: (companyId) => dispatch(GetCompanyRequest(companyId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditRoom);
