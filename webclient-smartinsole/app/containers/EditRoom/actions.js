import request from 'utils/request';
import { browserHistory } from 'react-router';
import { FETCHED_ROOM } from './constants';

export function setRoom(room) {
  const roomObj = {
    id: room.id,
    name: room.name,
    gateways: room.gateways,
    hardnesses: JSON.parse(room.hardnesses) || {},
    createdAt: room.createdAt,
    updatedAt: room.updatedAt,
  };

  return {
    type: FETCHED_ROOM,
    room: roomObj,
  };
}

export function GetRoomRequest(companyId, roomId) {
  return function setAction(dispatch) {
    return request
    .get(`/companies/${companyId}/rooms/${roomId}`)
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        dispatch(setRoom(res.body));
      }
    });
  };
}

export function EditRoomRequest(companyId, room) {
  return function setAction() {
    return request
    .put(`/companies/${companyId}/rooms/${room.id}`)
    .type('json')
    .send({ room })
    .end((err, res) => {
      request.redirectHandling(res.statusCode);
      if (!err) {
        browserHistory.push('/rooms');
      }
    });
  };
}
