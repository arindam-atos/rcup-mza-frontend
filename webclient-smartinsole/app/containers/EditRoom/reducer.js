import { FETCHED_ROOM } from './constants';

const initialState = {
  room: {},
};

function EditRoomReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHED_ROOM:
      return { ...state, room: action.room };
    default:
      return state;
  }
}

export default EditRoomReducer;
