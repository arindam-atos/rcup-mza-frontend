import request from 'utils/request';
import {USER_SAMPLES} from './constants';
import moment from 'moment/moment';

function setUserSamples(userSamples) {
    return {
        type: USER_SAMPLES,
        userSamples: userSamples[0],
    };
}

export function getSamplesByHours(companyId, add, day, from, to) {
    const fromTime = moment(day + ' ' + from).utc().format('HH:mm:ss');
    const toTime = moment(day + ' ' + to).utc().format('HH:mm:ss');

    const addresses = [];
    addresses.push(add);

    return (dispatch) => request
        .get('/historyByDay')
        .query({
            companyId,
            addresses,
            day,
            from: fromTime,
            to: toTime,
        })
        .end((err, res) => {
            if (!err) {
                dispatch(setUserSamples(res.body));
            }
        });
}
