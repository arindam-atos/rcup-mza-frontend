import { connect } from 'react-redux';
import LocalizationBoard from 'components/LocalizationBoard';
import { AllUsersRequest } from 'containers/ListUsers/actions';
import { setPageTitle } from 'containers/NavBar/actions';
import { getSamplesByHours } from './actions';

function mapStateToProps(state) {
  return {
    userSamples: state.get('localization').userSamples,
    users: state.get('users').users,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    AllUsersRequest: (companyId) => dispatch(AllUsersRequest(companyId)),
    getSamplesByHours: (companyId, add, day, from, to) =>
    dispatch(getSamplesByHours(companyId, add, day, from, to)),
    setPageTitle: (pageTitle) => dispatch(setPageTitle(pageTitle)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LocalizationBoard);
