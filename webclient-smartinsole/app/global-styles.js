import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: Roboto,Helvetica Neue,Arial,sans-serif;
        font-weight: 400;

    min-height: 100vh;
    position: relative;
    background-color: #fff;
  }

  #app {
    // background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  // p,
  // label {
  //   font-family: Georgia, Times, 'Times New Roman', serif;
  //   line-height: 1.5em;
  // }

  .h1, .h2, .h3, .h4, .h5, .h6, .navbar, .td-name, a, button.close, h1, h2, h3, h4, h5, h6, p, span, td, input, option, label {
      -moz-osx-font-smoothing: grayscale;
      -webkit-font-smoothing: antialiased;
      font-family: Roboto,Helvetica Neue,Arial,sans-serif;
      font-weight: 400;

  @keyframes fade {
    from { opacity: 0.5; }
  }

  :root {
    --toto: #2196f3;
  }
`;
