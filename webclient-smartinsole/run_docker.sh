#!/usr/bin/env sh


echo "Node run with IP of backend "+$1
echo "Big Query IP "+$2
echo "Env for which it runs "+$3

cross-env NODE_ENV=$1#$2#$3 node server
